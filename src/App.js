import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Login from "./components/login/login";
import BillingDashboard from "./components/pages/billingDashboard";
import Storage from "./components/storage/storage";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import PatientPage from "./components/pages/patientPage";
import BillingPage from "./components/pages/billingPage";
import NewTestTemplate from "./components/pages/newTestTemplatePage";
import TemplatesDashboard from "./components/pages/viewTestTemplatesPage";
import ReportsPage from "./components/pages/reportsPage";
import ViewTestReportTemplate from "./components/pages/viewTestTemplatePage";
import EditTestTemplate from "./components/pages/editTestTemplatePage";
import LabTestReportPage from "./components/pages/labTestReport";
import UserRegisteration from "./components/pages/userRegistrationPage";
import PasswordResetPage from "./components/pages/passwordResetPage";
import ProfilePage from "./components/pages/profilePage";
import ConfigPage from "./components/pages/configPage";
import AppointmentPage from "./components/pages/outpatient/appointmentPage";
import AppointmentDashboardPage from "./components/pages/outpatient/appointmentPageDashboard";
import DoctorsPage from "./components/pages/doctorsPage";
import PatientAppointmentPage from "./components/pages/patientAppointmentPage";
import PackagesDashboard from "./components/pages/viewPackagesPage";
import EditPackageTemplate from "./components/pages/editPackageTemplatePage";
import DashboardPage from "./components/pages/dashboardPage";

class App extends Component {
  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
    this.restoreSession = this.restoreSession.bind(this);
    this.state = {
      isLoggedIn: false,
      restoringSession: false,
      userId: null,
      accessToken: null,
      userName: null,
    };
  }

  login(value, userId, name, accessToken) {
    this.setState({
      isLoggedIn: value,
      userId: userId,
      userName: name,
      accessToken: accessToken,
    });
    Storage.set("loggedIn", value);
    Storage.set("userId", userId);
    Storage.set("userName", name);
    Storage.set("accessToken", accessToken);
  }

  restoreSession() {
    this.login(
      Storage.get("loggedIn"),
      Storage.get("userId"),
      Storage.get("userName"),
      Storage.get("accessToken"),
    );
  }

  componentWillMount() {
    this.restoreSession();
  }

  render() {
    const theme = createTheme({
      palette: {
        // Define your theme colors here
        primary: {
          main: "#1976d2",
        },
        secondary: {
          main: "#dc004e",
        },
      },
    });

    const isLoggedIn =
      Storage.get("loggedIn") === "true" &&
      Storage.get("accessToken") !== "null" &&
      Date.now() < Storage.get("expires") + Storage.get("validFrom");
    const isAdmin = Storage.get("isAdmin") === "true";
    return (
      <ThemeProvider theme={theme}>
        {" "}
        {/* Wrap the Router with ThemeProvider */}
        <Router>
          <React.Fragment>
            <Switch>
              <Route exact path="/login" component={Login} />
              {/* <Route exact path='/login' render={props => <Login login={this.login} />} /> */}
              {isLoggedIn && this.state.accessToken !== "null" ? (
                <div>
                  <Route exact path="/billing" component={BillingDashboard} />
                  <Route exact path="/" component={BillingDashboard} />
                  <Route
                    exact
                    path="/lab/template"
                    component={NewTestTemplate}
                  />
                  <Route
                    exact
                    path="/lab/template/:id"
                    component={ViewTestReportTemplate}
                  />
                  <Route exact path="/dashboard" component={DashboardPage} />
                  <Route
                    exact
                    path="/daycare/templates"
                    component={PackagesDashboard}
                  />
                  <Route
                    exact
                    path="/lab/template/:id/edit"
                    component={EditTestTemplate}
                  />
                  <Route
                    exact
                    path="/daycare/template/:id/edit"
                    component={EditPackageTemplate}
                  />
                  <Route
                    exact
                    path="/daycare/template"
                    component={EditPackageTemplate}
                  />
                  <Route
                    exact
                    path="/lab/templates"
                    component={TemplatesDashboard}
                  />
                  <Route exact path="/patient" component={PatientPage} />
                  <Route exact path="/patient/:id" component={PatientPage} />
                  <Route
                    exact
                    path="/patient/:id/edit"
                    component={PatientPage}
                  />
                  <Route exact path="/new/billing" component={BillingPage} />
                  <Route exact path="/billing/:id" component={BillingPage} />
                  <Route
                    exact
                    path="/billing/:id/edit"
                    component={BillingPage}
                  />
                  <Route exact path="/tests" component={ReportsPage} />
                  <Route
                    exact
                    path="/tests/bill/:bid"
                    component={ReportsPage}
                  />
                  <Route
                    exact
                    path="/appointments"
                    component={AppointmentPage}
                  />
                  <Route exact path="/doctors" component={DoctorsPage} />
                  <Route
                    exact
                    path="/appointments/dashboard"
                    component={AppointmentDashboardPage}
                  />
                  <Route
                    exact
                    path="/appointments/bill/:id"
                    component={PatientAppointmentPage}
                  />
                  <Route exact path="/profile" component={ProfilePage} />
                  <Route exact path="/test/:id" component={LabTestReportPage} />
                  <Route exact path="/config" component={ConfigPage} />
                  {isAdmin ? (
                    <Route
                      exact
                      path="/admin/userregistration"
                      component={UserRegisteration}
                    />
                  ) : null}
                  {isAdmin ? (
                    <Route
                      exact
                      path="/admin/passwordreset"
                      component={PasswordResetPage}
                    />
                  ) : null}
                </div>
              ) : (
                <Redirect to="/login" />
              )}
              ;
            </Switch>
          </React.Fragment>
        </Router>
      </ThemeProvider>
    );
  }
}

export default App;
