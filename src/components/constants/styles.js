import { makeStyles } from "@mui/styles";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
  tableElements: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  table: {
    minWidth: 750,
    borderCollapse: "collapse",
    borderSpacing: "0 15px",
    backgroundColor: "#f8f9fa",
    alignContent: "center",
    justifyContent: "center",
  },
  tableHeader: {
    backgroundColor: theme.palette.grey[200],
  },
  tableCell: {
    padding: "10px 15px",
    fontSize: "1rem",
    color: theme.palette.text.primary,
    borderBottom: "none",
  },
  buttonWrapper: {
    display: "flex",
    justifyContent: "flex-end",
    marginBottom: "20px",
  },
  newBillingButton: {
    marginLeft: theme.spacing(3),
    backgroundColor: theme.palette.primary.main,
    color: "#fff",
    textTransform: "none",
    padding: "10px 20px",
    borderRadius: "8px",
    "&:hover": {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  clickableRow: {
    cursor: "pointer",
    "&:hover": {
      backgroundColor: theme.palette.action.hover,
    },
  },
  pendingRow: {
    backgroundColor: "#fff4e5",
  },
  root: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  container: {
    maxHeight: 440,
  },
  paper: {
    alignContent: "center",
    padding: theme.spacing(3),
    overflow: "hidden",
    width: "100%",
    boxShadow: "0 3px 8px rgba(0, 0, 0, 0.15)",
    backgroundColor: "#ffffff",
  },
  title: {
    fontWeight: 700,
    marginBottom: theme.spacing(2),
    color: "#2c3e50",
    textAlign: "center",
  },
  formGroup: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: theme.spacing(2),
  },
  formAddUser: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    marginBottom: theme.spacing(2),
  },
  textField: {
    width: "50%",
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(3),
  },
  addIcon: {
    cursor: "pointer",
    color: "#007bff",
    "&:hover": {
      color: "#0056b3",
    },
  },
  removeIcon: {
    cursor: "pointer",
    color: "#ff4d4d",
    "&:hover": {
      color: "#e60000",
    },
  },
  dialogTitle: {
    color: "#007bff",
    fontWeight: 600,
  },
  totalAmount: {
    fontSize: "1.25rem",
    fontWeight: "bold",
    color: "#2c3e50",
  },
  txnAmount: {
    fontSize: "1rem",
    color: "#495057",
  },
  buttonPrimary: {
    backgroundColor: "#007bff",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#0056b3",
    },
  },
  buttonSecondary: {
    backgroundColor: "#6c757d",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#5a6268",
    },
  },
  tableContainer: {
    borderRadius: theme.shape.borderRadius,
    boxShadow: "0 4px 12px rgba(0, 0, 0, 0.1)",
  },
  tableHeadCell: {
    backgroundColor: "#e9ecef",
    fontWeight: "bold",
    color: "#2c3e50",
  },
  actionButtons: {
    display: "flex",
    justifyContent: "flex-end",
    gap: theme.spacing(2),
    marginTop: theme.spacing(2),
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  mintable: {
    width: "100%",
    border: 1,
  },
  buttonSpacing: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(3), // Adjusted spacing
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
    backgroundColor: "#fafafa",
  },
  toolbar: {
    paddingRight: 24,
    spacing: 10,
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  menuButtonHidden: {
    display: "none",
  },
  drawerPaper: {
    position: "relative",
    whiteSpace: "nowrap",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: "hidden",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9),
    },
  },
  override: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
  },
  sectionTitle: {
    fontWeight: 600,
    marginBottom: theme.spacing(1),
    color: theme.palette.primary.main,
  },
  infoRow: {
    marginBottom: theme.spacing(2),
  },
  label: {
    fontWeight: 500,
    color: "#495057",
  },
  value: {
    fontWeight: 400,
    color: "#2c3e50",
  },
  divider: {
    margin: `${theme.spacing(2)}px 0`,
  },
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
    backgroundColor: "#fafafa",
  },
}));

export default useStyles;
