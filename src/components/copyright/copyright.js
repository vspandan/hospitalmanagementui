import Link from "@mui/material/Link";
import React from "react";
import Typography from "@mui/material/Typography";
import * as Constants from "../constants/constants";
function Copyright() {
  return (
    <Typography variant="inherit" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit">{Constants.COMPANY_NAME}</Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

export default Copyright;
