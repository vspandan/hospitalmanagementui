import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";

import DatePicker from "@mui/lab/DatePicker";
import {
  Button,
  TableContainer,
  Typography,
  TextField,
  MenuItem,
  Grid,
} from "@mui/material";
import { Redirect } from "react-router-dom";
import AppointmentService from "../services/Appointment"; // Updated service
import clsx from "clsx";
import useStyles from "../constants/styles";
import { HashLoader } from "react-spinners";

import Identity from "../services/Identity";
import Autocomplete from "@mui/material/Autocomplete";

class AppointmentList extends Component {
  state = {
    appointmentData: [],
    patientAutoSearchData: [],
    doctorAutoSearchData: [],
    referrerAutoSearchData: [],
    selectedAppointmentId: null,
    viewAppointmentRedirect: false,
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    pageSize: 20,
    loading: true,
    filters: {
      patientId: "",
      doctorId: "",
      referrerId: "",
      appointmentDate: null,
      status: "Scheduled",
    },
    statusOptions: ["Scheduled", "Completed", "Canceled"],
  };

  handleFilterChange = (name, value) => {
    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        [name]: value,
      },
    }));
  };

  handleDateChange = (date) => {
    this.setState((prevFilters) => ({
      ...prevFilters,
      appointmentDate: date,
    }));
  };

  handleSearch = () => {
    this.setState({ page: 0 });
    this.fetchAppointmentData();
    /*this.setState({
      filters: {
        patientId: "",
        doctorId: "",
        referrerId: "",
        appointmentDate: null,
        status: "Scheduled",
      },
    });*/
  };

  setViewAppointmentRedirect = (id) => {
    this.setState({
      selectedAppointmentId: id,
      viewAppointmentRedirect: true,
    });
  };

  renderViewAppointmentRedirect = () => {
    if (this.state.viewAppointmentRedirect) {
      let redirectUrl = `/appointments/bill/${this.state.selectedAppointmentId}`;
      return <Redirect to={redirectUrl} />;
    }
  };

  componentDidMount() {
    this.fetchAppointmentData();
  }

  fetchAppointmentData(page = 0, size = this.state.pageSize) {
    this.setState({ loading: true });
    const params = {
      page,
      size,
      ...Object.keys(this.state.filters).reduce((acc, key) => {
        if (
          this.state.filters[key] !== "" &&
          this.state.filters[key] !== null &&
          this.state.filters[key] !== undefined
        ) {
          acc[key] = this.state.filters[key];
        }
        return acc;
      }, {}),
    };
    const appointmentService = new AppointmentService();
    appointmentService
      .getAppointmentList(params)
      .then((res) => {
        this.setState({
          appointmentData: res.data.content,
          totalElements: res.data.totalElements,
          totalPages: res.data.totalPages,
          currentPage: res.data.number,
          loading: false,
        });
      })
      .catch((error) => {
        console.error("Error fetching appointment data:", error);
        this.setState({ loading: false });
      });
  }

  patientAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .patientAutoSearch(event.target.value)
      .then((res) =>
        this.setState({ patientAutoSearchData: res.data, loading: false }),
      )
      .catch((error) => {
        console.error("Error searching patient: ", error);
        this.setState({ loading: false });
      });
  };

  referrerAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .referrerAutoSearch(event.target.value)
      .then((res) =>
        this.setState({ referrerAutoSearchData: res.data, loading: false }),
      )
      .catch((error) => {
        console.error("Error searching referrer: ", error);
        this.setState({ loading: false });
      });
  };

  doctorAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .doctorAutoSearch(event.target.value)
      .then((res) =>
        this.setState({ doctorAutoSearchData: res.data, loading: false }),
      )
      .catch((error) => {
        console.error("Error searching doctor: ", error);
        this.setState({ loading: false });
      });
  };

  handlePageChange(page) {
    this.fetchAppointmentData(page);
  }

  renderPaginationControls() {
    const { totalPages, currentPage } = this.state;
    return (
      <div style={{ marginTop: "20px", textAlign: "center" }}>
        <Button
          disabled={currentPage === 0}
          onClick={() => this.handlePageChange(currentPage - 1)}
        >
          Previous
        </Button>
        <span style={{ margin: "0 10px" }}>
          Page {totalPages === 0 ? 0 : currentPage + 1} of {totalPages}
        </span>
        <Button
          disabled={currentPage + 1 === totalPages}
          onClick={() => this.handlePageChange(currentPage + 1)}
        >
          Next
        </Button>
      </div>
    );
  }

  render() {
    const { classes } = this.props;

    return this.state.loading ? (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <HashLoader color={"#00ffff"} loading={true} size={50} />
      </div>
    ) : (
      <div>
        <Typography
          variant="h4"
          style={{
            fontWeight: "bold",
            padding: "8px 16px",
            borderBottom: "2px solid #f0f0f0",
            color: "#3f51b5",
            textAlign: "center",
          }}
        >
          Appointment Dashboard
        </Typography>

        <Grid
          container
          spacing={2}
          style={{
            margin: "10px",
            alignItems: "center",
            gap: "10px",
            flexDirection: "row",
          }}
        >
          {/* Filter Section */}
          <Autocomplete
            name="patientId"
            options={this.state.patientAutoSearchData}
            getOptionLabel={(option) =>
              `${option.code} - ${option.firstName} - ${option.lastName} - ${option.phone}`
            }
            style={{ width: 300 }}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Select Patient"
                variant="outlined"
                margin="dense"
                fullWidth
                onChange={this.patientAutoSearch}
              />
            )}
            onChange={(_event, option) => {
              if (option) {
                // If option is not null, set the selected patient and filter value
                this.handleFilterChange("patientId", option.id);
              } else {
                // If option is null (cleared), reset selected patient and filter value
                this.handleFilterChange("patientId", null);
              }
            }}
            onClear={() => {
              this.handleFilterChange("patientId", null); // Reset filter value when cleared
            }}
          />
          <Autocomplete
            options={this.state.doctorAutoSearchData}
            getOptionLabel={(option) =>
              `${option.firstName} ${option.lastName} - ${option.specialization}`
            }
            name="doctorId"
            style={{ width: 300 }}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Select Doctor"
                variant="outlined"
                fullWidth
                margin="dense"
                onChange={this.doctorAutoSearch}
              />
            )}
            onChange={(_event, option) => {
              if (option) {
                // If option is not null, set the selected patient and filter value
                this.handleFilterChange("doctorId", option.id);
              } else {
                // If option is null (cleared), reset selected patient and filter value
                this.handleFilterChange("doctorId", null);
              }
            }}
            onClear={() => {
              this.handleFilterChange("doctorId", null); // Reset filter value when cleared
            }}
          />
          <Autocomplete
            options={this.state.referrerAutoSearchData}
            getOptionLabel={(option) =>
              `${option.firstName} ${option.lastName}`
            }
            name="doctorId"
            style={{ width: 300 }}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Select Referrer"
                variant="outlined"
                fullWidth
                margin="dense"
                onChange={this.referrerAutoSearch}
              />
            )}
            onChange={(_event, option) => {
              if (option) {
                // If option is not null, set the selected patient and filter value
                this.handleFilterChange("referrerId", option.id);
              } else {
                // If option is null (cleared), reset selected patient and filter value
                this.handleFilterChange("referrerId", null);
              }
            }}
            onClear={() => {
              this.handleFilterChange("referrerId", null); // Reset filter value when cleared
            }}
          />
          <DatePicker
            label="Appointment Date"
            value={this.state.filters.appointmentDate}
            onChange={this.handleDateChange}
            renderInput={(params) => (
              <TextField {...params} size="small" variant="outlined" />
            )}
          />
          <TextField
            label="Status"
            name="status"
            fullWidth
            style={{ width: 300 }}
            value={this.state.filters.status}
            onChange={(e) => this.handleFilterChange("status", e.target.value)}
            select
            variant="outlined"
            margin="dense"
          >
            {this.state.statusOptions.map((status) => (
              <MenuItem key={status} value={status}>
                {status}
              </MenuItem>
            ))}
          </TextField>
          <Button variant="contained" onClick={this.handleSearch}>
            Search
          </Button>
        </Grid>

        {/* Appointment Table */}
        <TableContainer
          style={{ display: "flex", justifyContent: "center", width: "100%" }}
        >
          <Table className={classes.table}>
            <TableHead>
              <TableRow className={classes.tableHeader}>
              <TableCell className={classes.tableCell}>
                  <strong>Appointment Id</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Patient Name</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Doctor Name</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Specialization</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Appointment Date</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Appointment Type</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Status</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Referred By</strong>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.renderViewAppointmentRedirect()}
              {this.state.appointmentData.map((appointment) => (
                <TableRow
                  hover
                  key={appointment.appointmentDetails.billReferenceCode}
                  className={clsx(classes.clickableRow)}
                  onClick={() =>
                    this.setViewAppointmentRedirect(
                      appointment.appointmentDetails.billReferenceCode,
                    )
                  }
                >
                  <TableCell className={classes.tableCell}>
                    {`${appointment.appointmentDetails.appointmentId.toUpperCase()}`}
                  </TableCell><TableCell className={classes.tableCell}>
                    {`${appointment.patientDetails.firstName} ${appointment.patientDetails.lastName}`}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {`${appointment.doctorDetailsDto.firstName} ${appointment.doctorDetailsDto.lastName}`}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {`${appointment.doctorDetailsDto.specialization}`}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {new Date(
                      appointment.appointmentDetails.appointmentDate,
                    ).toLocaleDateString()}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {appointment.appointmentDetails.appointmentType}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {appointment.appointmentDetails.status}
                  </TableCell>
                  <TableCell className={classes.tableCell}>
                    {appointment.referrerDetails &&
                    (appointment.referrerDetails.firstName ||
                      appointment.referrerDetails.lastName)
                      ? [
                          appointment.referrerDetails.firstName,
                          appointment.referrerDetails.lastName,
                        ]
                          .filter(Boolean) // Removes null/undefined/empty values
                          .join(" ") // Joins non-null values with a space
                      : "N/A"}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        {/* Pagination Controls */}
        {this.renderPaginationControls()}
      </div>
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(AppointmentList);
