import React, { Component } from "react";
import Link from "@mui/material/Link";
import { withStyles } from "@mui/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Identity from "../services/Identity";
import {
  Autocomplete,
  Button,
  Grid,
  MenuItem,
  TableContainer,
  TextField,
  Typography,
} from "@mui/material";
import { Redirect } from "react-router-dom";
import Billing from "../services/Billing";
import clsx from "clsx";
import useStyles from "../constants/styles";
import { HashLoader } from "react-spinners";
import { DatePicker } from "@mui/lab";

class BillingList extends Component {
  state = {
    billingData: [],
    patientAutoSearchData: [],
    doctorAutoSearchData: [],
    referrerAutoSearchData: [],
    selectedPatient: null,
    selectedReferrer: null,
    selectedDoctor: null,
    selectedBillId: null,
    newBillingRedirect: false,
    viewBillingRedirect: false,
    totalElements: 0,
    totalPages: 0,
    currentPage: 0,
    pageSize: 20,
    loading: true,
    filters: {
      patientId: "",
      doctorId: "",
      referrerId: "",
      appointmentDate: null,
      status: "",
    },
    statusOptions: ["PAID", "CANCELLED", "DRAFT"],
  };

  handleSearch = () => {
    this.setState({ page: 0 });
    this.fetchBillingData();
    /*this.setState({
      filters: {
        patientId: "",
        doctorId: "",
        referrerId: "",
        appointmentDate: null,
        status: "",
      },
    });*/
  };

  patientAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .patientAutoSearch(event.target.value)
      .then((res) =>
        this.setState({ patientAutoSearchData: res.data, loading: false }),
      )
      .catch((error) => {
        console.error("Error searching patient: ", error);
        this.setState({ loading: false });
      });
  };

  doctorAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .doctorAutoSearch(event.target.value)
      .then((res) =>
        this.setState({ doctorAutoSearchData: res.data, loading: false }),
      )
      .catch((error) => {
        console.error("Error searching patient: ", error);
        this.setState({ loading: false });
      });
  };

  referrerAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .referrerAutoSearch(event.target.value)
      .then((res) =>
        this.setState({ referrerAutoSearchData: res.data, loading: false }),
      )
      .catch((error) => {
        console.error("Error searching referrer: ", error);
        this.setState({ loading: false });
      });
  };

  handleFilterChange = (name, value) => {
    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        [name]: value,
      },
    }));
  };

  setNewBillingRedirect = () => {
    this.setState({
      newBillingRedirect: true,
    });
  };

  renderNewBillingRedirect = () => {
    if (this.state.newBillingRedirect) {
      return <Redirect to="/new/billing" />;
    }
  };

  setViewBillingRedirect = (id) => {
    this.setState({
      selectedBillId: id,
      viewBillingRedirect: true,
    });
  };

  renderViewBillingRedirect = () => {
    if (this.state.viewBillingRedirect) {
      let redirectUrl = "/billing/" + this.state.selectedBillId;
      return <Redirect to={redirectUrl} />;
    }
  };

  componentDidMount() {
    this.fetchBillingData();
  }

  fetchBillingData(page = 0, size = this.state.pageSize) {
    this.setState({ loading: true });
    let params = {
      page: page, // Current page number
      size: size,
      ...Object.keys(this.state.filters).reduce((acc, key) => {
        if (
          this.state.filters[key] !== "" &&
          this.state.filters[key] !== null &&
          this.state.filters[key] !== undefined
        ) {
          acc[key] = this.state.filters[key];
        }
        return acc;
      }, {}),
    };
    let billingService = new Billing();
    billingService
      .getBillingList(params) // Pass the page and size parameters
      .then((res) => {
        this.setState({
          billingData: res.data.content, // Store the billing items
          totalElements: res.data.totalElements, // Store total number of elements
          totalPages: res.data.totalPages, // Store total number of pages
          currentPage: res.data.number,
          loading: false, // Store current page number
        });
      })
      .catch((error) => {
        console.error("Error fetching billing data:", error);
        this.setState({ loading: false });
      });
  }

  handlePageChange(page) {
    this.fetchBillingData(page); // Fetch data for the new page
  }

  renderPaginationControls() {
    const { totalPages, currentPage } = this.state;

    return (
      <div style={{ marginTop: "20px", textAlign: "center" }}>
        <Button
          disabled={currentPage === 0}
          onClick={() => this.handlePageChange(currentPage - 1)}
        >
          Previous
        </Button>
        <span style={{ margin: "0 10px" }}>
          Page {totalPages === 0 ? 0 : currentPage + 1} of {totalPages}
        </span>
        <Button
          disabled={currentPage + 1 === totalPages}
          onClick={() => this.handlePageChange(currentPage + 1)}
        >
          Next
        </Button>
      </div>
    );
  }

  render() {
    const { classes } = this.props;

    return this.state.loading ? (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <HashLoader
          color={"#00ffff"}
          loading={true}
          size={50}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
      </div>
    ) : (
      <div>
        <Typography
          variant="h4"
          style={{
            fontWeight: "bold",
            padding: "8px 16px",
            borderBottom: "2px solid #f0f0f0",
            color: "#3f51b5",
            textAlign: "center",
          }}
        >
          Billing Dashboard
        </Typography>

        <Grid
          container
          spacing={2}
          style={{
            margin: "20px",
            alignItems: "center",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          {/* Filter Section */}
          <Grid item>
            <Grid
              container
              spacing={2}
              style={{
                alignItems: "center",
                gap: "10px",
                flexDirection: "row",
              }}
            >
              <Autocomplete
                name="patientId"
                options={this.state.patientAutoSearchData}
                getOptionLabel={(option) =>
                  `${option.code} - ${option.firstName} - ${option.lastName} - ${option.phone}`
                }
                style={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Patient"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={this.patientAutoSearch}
                  />
                )}
                onChange={(_event, option) => {
                  if (option) {
                    // If option is not null, set the selected patient and filter value
                    this.setState({ selectedPatient: option });
                    this.handleFilterChange("patientId", option.id);
                  } else {
                    // If option is null (cleared), reset selected patient and filter value
                    this.setState({ selectedPatient: null });
                    this.handleFilterChange("patientId", null);
                  }
                }}
                onClear={() => {
                  this.setState({ selectedPatient: null });
                  this.handleFilterChange("patientId", null); // Reset filter value when cleared
                }}
                clearOnEscape
              />
              <Autocomplete
                options={this.state.doctorAutoSearchData}
                getOptionLabel={(option) =>
                  `${option.firstName} ${option.lastName} - ${option.specialization}`
                }
                name="doctorId"
                style={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Doctor"
                    variant="outlined"
                    fullWidth
                    margin="dense"
                    onChange={this.doctorAutoSearch}
                  />
                )}
                onChange={(_event, option) => {
                  if (option) {
                    // If option is not null, set the selected patient and filter value
                    this.setState({ selectedDoctor: option });
                    this.handleFilterChange("doctorId", option.id);
                  } else {
                    // If option is null (cleared), reset selected patient and filter value
                    this.setState({ selectedDoctor: null });
                    this.handleFilterChange("doctorId", null);
                  }
                }}
                onClear={() => {
                  this.setState({ selectedDoctor: null });
                  this.handleFilterChange("doctorId", null); // Reset filter value when cleared
                }}
              />
              <Autocomplete
                options={this.state.referrerAutoSearchData}
                getOptionLabel={(option) =>
                  `${option.firstName} ${option.lastName}`
                }
                name="referrerId"
                style={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Referrer"
                    variant="outlined"
                    fullWidth
                    margin="dense"
                    onChange={this.referrerAutoSearch}
                  />
                )}
                onChange={(_event, option) => {
                  if (option) {
                    // If option is not null, set the selected patient and filter value
                    this.setState({ selectedReferrer: option });
                    this.handleFilterChange("referrerId", option.id);
                  } else {
                    // If option is null (cleared), reset selected patient and filter value
                    this.setState({ selectedReferrer: null });
                    this.handleFilterChange("referrerId", null);
                  }
                }}
                onClear={() => {
                  this.setState({ selectedReferrer: null });
                  this.handleFilterChange("referrerId", null); // Reset filter value when cleared
                }}
              />
              <DatePicker
                label="Appointment Date"
                value={this.state.filters.appointmentDate}
                onChange={this.handleDateChange}
                renderInput={(params) => (
                  <TextField {...params} size="small" variant="outlined" />
                )}
              />
              <TextField
                label="Status"
                name="status"
                fullWidth
                style={{ width: 300 }}
                value={this.state.filters.status}
                onChange={(e) =>
                  this.handleFilterChange("status", e.target.value)
                }
                select
                variant="outlined"
                margin="dense"
              >
                {this.state.statusOptions.map((status) => (
                  <MenuItem key={status} value={status}>
                    {status}
                  </MenuItem>
                ))}
              </TextField>
              <Button variant="contained" onClick={this.handleSearch}>
                Search
              </Button>
            </Grid>
          </Grid>
          {/* New Billing Button */}
          <Grid item>
            <Grid
              container
              spacing={2}
              style={{
                marginRight: "40px",
                alignItems: "left",
                gap: "10px",
                flexDirection: "row",
              }}
            >
              {this.renderNewBillingRedirect()}
              <Button
                variant="contained"
                onClick={this.setNewBillingRedirect}
                sx={{
                  backgroundColor: "#0056b3",
                  color: "white",
                  "&:hover": { backgroundColor: "#A056b3" },
                }}
              >
                New Billing
              </Button>
            </Grid>
          </Grid>
        </Grid>
        {/* Billing Table */}
        <TableContainer
          style={{ display: "flex", justifyContent: "center", width: "100%" }}
        >
          <Table className={classes.table}>
            <TableHead>
              <TableRow className={classes.tableHeader}>
                <TableCell className={classes.tableCell}>
                  <strong>Bill ID</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Patient ID</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Patient Name</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Doctor</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Referred By</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Billing Amount</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Billing Status</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Billed By</strong>
                </TableCell>
                <TableCell className={classes.tableCell}>
                  <strong>Created At</strong>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.renderViewBillingRedirect()}
              {this.state.billingData === undefined
                ? ""
                : this.state.billingData.map((row) => (
                    <TableRow
                      hover
                      role="checkbox"
                      key={row.id}
                      tabIndex={row.id}
                      className={clsx(
                        classes.clickableRow,
                        row.status === "Pending" && classes.pendingRow,
                      )}
                      onClick={() => this.setViewBillingRedirect(row.id)}
                    >
                      <TableCell className={classes.tableCell}>
                        {row.code}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {row.patientDetails.id}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {row.patientDetails.firstName}{" "}
                        {row.patientDetails.lastName ??
                          row.patientDetails.lastName}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {row.doctorDetails &&
                        (row.doctorDetails.firstName ||
                          row.doctorDetails.lastName)
                          ? [
                              row.doctorDetails.firstName,
                              row.doctorDetails.lastName,
                            ]
                              .filter(Boolean) // Removes null/undefined/empty values
                              .join(" ") // Joins non-null values with a space
                          : "Self"}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {row.referrerDetails &&
                        (row.referrerDetails.firstName ||
                          row.referrerDetails.lastName)
                          ? [
                              row.referrerDetails.firstName,
                              row.referrerDetails.lastName,
                            ]
                              .filter(Boolean) // Removes null/undefined/empty values
                              .join(" ") // Joins non-null values with a space
                          : "N/A"}{" "}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {row.amountPaid}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {row.status}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {row.createdBy}
                      </TableCell>
                      <TableCell className={classes.tableCell}>
                        {row.createdAt}
                      </TableCell>
                    </TableRow>
                  ))}
            </TableBody>
          </Table>
        </TableContainer>
        {/* Pagination Controls */}
        {this.renderPaginationControls()}
      </div>
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(BillingList);
