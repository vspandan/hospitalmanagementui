import React, { Component } from "react";
import Paper from "@mui/material/Paper";
import { withStyles } from "@mui/styles";
import {
  TextField,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Button,
  TableBody,
  Grid,
  Box,
  Select,
  FormControl,
  InputLabel,
  MenuItem,
} from "@mui/material";
import { Autocomplete } from "@mui/lab";
import Typography from "@mui/material/Typography";
import RemoveCircleOutlineOutlinedIcon from "@mui/icons-material/RemoveCircleOutlineOutlined";
import { Redirect } from "react-router-dom";
import Billing from "../services/Billing";
import Identity from "../services/Identity";
import Storage from "../storage/storage";
import useStyles from "../constants/styles";
import { HashLoader } from "react-spinners";
import { PdfDialog } from "./pdfViewer";

const paymentModes = [
  "CASH",
  "UPI",
  "MOBILE_WALLET",
  "CREDIT_CARD",
  "DEBIT_CARD",
  "NET_BANKING",
  "IMPS",
  "NEFT",
  "RTGS",
  "CHEQUE",
  "DEMAND_DRAFT",
  "AEPS",
  "BBPS",
  "PAYMENT_GATEWAY",
];

class BillingTemplate extends Component {
  state = {
    dialogConfirmation: false,
    dialogOpen: false,
    id: null,
    code: null,
    billItems: [],
    redirect: false,
    discount: 0,
    total: 0,
    version: null,
    comments: null,
    docterReferenceId: null,
    referrerId: null,
    referrerObj: null,
    doctorName: null,
    patientId: null,
    patientObj: null,
    status: "DRAFT",
    editMode: true,
    autoSearchData: [],
    patientAutoSearchData: [],
    doctorAutoSearchData: [],
    referrerAutoSearchData: [],
    transactionDetails: [],
    amountPaid: 0,
    txnAmount: 0,
    paymentType: "",
    paymentRefId: "",
    open: false,
    loading: false,
    createdAt: "",
    createdBy: "",
    dateEditMode: false,
  };

  handleEditToggle = () => {
    this.setState({ dateEditMode: !this.state.dateEditMode });
  };

  handleValueChange = (event) => {
    this.setState({
      createdAt: event.target.value,
      dateEditMode: !this.state.dateEditMode,
    });
    let d = {
      billId: this.state.id,
      createdAt: new Date(event.target.value).getTime(),
      paymentRefId: this.state.paymentRefId,
      paymentType: this.state.paymentType,
    };
    let billingService = new Billing();
    billingService
      .putDateEntity(d)
      .then((res) => {
        if (res.data !== undefined) {
          this.setState({
            loading: false,
            editMode:
              res.data.status !== "PAID" && res.data.status !== "CANCELLED",
            id: res.data.id,
            patientId: res.data.patientDetails.id,
            docterReferenceId: res.data.doctorDetails
              ? res.data.doctorDetails.id
              : null,
            doctorName: res.data.doctorDetails
              ? res.data.doctorDetails.firstName
              : "",
            referrerId: res.data.referrerDetails
              ? res.data.referrerDetails.id
              : "",
            comments: res.data.comments,
            status: res.data.status,
            discount: res.data.discount,
            version: res.data.version,
            billItems: res.data.billItems,
            amountPaid: res.data.amountPaid,
            code: res.data.code,
            txnAmount: 0,
            paymentType: res.data.paymentType,
            paymentRefId: res.data.paymentRefId,
            createdAt: res.data.createdAt,
            createdBy: res.data.createdBy,
            patientObj: res.data.patientDetails,
            transactionDetails: res.data.transactionDetails,
          });
          if (
            this.state.patientId !== null &&
            this.state.patientId !== undefined
          ) {
          }
          this.setTotal();
        }
      })
      .catch((error) => {
        console.error(`Error updating bill: `, error);
        this.setState({ loading: false });
      });
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/" />;
    }
  };

  setTotal = () => {
    let totalAmnt = 0;
    this.state.billItems.map((_field, index) => {
      totalAmnt = totalAmnt + this.setItemTotal(index);
      return;
    });
    totalAmnt = totalAmnt - parseFloat(this.state.discount);
    return totalAmnt;
  };

  setItemTotal = (index) => {
    let total = 0;
    if (this.state.billItems[index].discount === undefined)
      total = parseFloat(this.state.billItems[index].price);
    else
      total =
        parseFloat(this.state.billItems[index].price) -
        parseFloat(this.state.billItems[index].discount);
    return total;
  };

  handleBillItemsChange = (index, attr) => (event) => {
    switch (attr) {
      case "refId":
        this.state.billItems[index].itemReferenceId = event.target.value;
        this.setState({ billItems: this.state.billItems });
        break;
      case "type":
        this.state.billItems[index].billItemType = event.target.value;
        this.setState({ billItems: this.state.billItems });
        break;
      case "doctorId":
        this.setState({ docterReferenceId: event.target.value });
        break;
      case "comments":
        this.setState({ comments: event.target.value });
        break;
      case "price":
        this.state.billItems[index].price = event.target.value;
        this.setState({ billItems: this.state.billItems });
        break;
      case "discount":
        var regex = /^\d*\.?\d*$/;
        var value = event.target.value;

        if (value === "" || regex.test(value)) {
          if (!value || value.trim() === "") {
            value = 0;
          } else if (value > 100) {
            value = 100;
          } else if (value < 0) {
            value = 0;
          }

          this.state.billItems[index].discount =
            (this.state.billItems[index].price * value) / 100;
          this.setState({ billItems: this.state.billItems });
        }
        break;
      case "totalDiscount":
        this.setState({ discount: event.target.value });
        break;
      case "txnAmount":
        var regex = /^\d*\.?\d*$/;
        var value = event.target.value;
        if (value === "" || regex.test(value)) {
          if (!value || value.trim() === "") {
            value = 0;
          }
        }
        if (!regex.test(value)) {
          value = 0;
        }
        if (this.setTotal() - this.state.amountPaid < value) {
          value = 0;
        }
        this.setState({ txnAmount: parseInt(value) });
        break;
      case "paymentType":
        var value = event.target.value;
        this.setState({ paymentType: value });
        break;
      case "paymentRefId":
        var value = event.target.value;
        this.setState({ paymentRefId: value });
      default:
        break;
    }
  };

  submitForm = () => {
    let data = {
      patientId: this.state.patientId,
      referrerId: this.state.referrerId,
      docterReferenceId: this.state.docterReferenceId,
      comments: this.state.comments,
      status: this.state.status,
      discount: this.state.discount,
      version: this.state.version,
      id: this.state.id,
      billItems: this.state.billItems,
      code: this.state.code,
    };
    this.setState({ loading: true });
    let billingService = new Billing();
    if (this.state.id === undefined || this.state.id === null) {
      billingService
        .postEntity(data)
        .then((res) => {
          if (res.data !== undefined) {
            window.location.href = "/billing/" + res.data.id;
          }
        })
        .catch((error) => {
          console.error(`Error generating bill: `, error);
          this.setState({ loading: false, editMode: true });
        });
    } else {
      if (
        this.state.txnAmount <= 0 ||
        this.state.paymentRefId == null ||
        this.state.paymentRefId.trim() === "" ||
        this.state.paymentType == null ||
        this.state.paymentType.trim() === ""
      ) {
        alert("Please fill in the transaction details to save");
        this.setState({ loading: false });
        return;
      }
      let d = {
        billId: this.state.id,
        txnAmount: this.state.txnAmount,
        paymentRefId: this.state.paymentRefId,
        paymentType: this.state.paymentType,
      };
      //alert(JSON.stringify(d));
      billingService
        .putEntity(d)
        .then((res) => {
          if (res.data !== undefined) {
            this.setState({
              loading: false,
              editMode:
                res.data.status !== "PAID" && res.data.status !== "CANCELLED",
              id: res.data.id,
              patientId: res.data.patientDetails.id,
              docterReferenceId: res.data.doctorDetails
                ? res.data.doctorDetails.id
                : null,
              doctorName: res.data.doctorDetails
                ? res.data.doctorDetails.firstName
                : "",
              comments: res.data.comments,
              status: res.data.status,
              discount: res.data.discount,
              version: res.data.version,
              billItems: res.data.billItems,
              amountPaid: res.data.amountPaid,
              code: res.data.code,
              txnAmount: 0,
              patientObj: res.data.patientDetails,
              paymentType: res.data.paymentType,
              paymentRefId: res.data.paymentRefId,
              transactionDetails: res.data.transactionDetails,
            });

            this.setTotal();
          }
        })
        .catch((error) => {
          console.error(`Error updating bill: `, error);
          this.setState({ loading: false });
        });
    }
    this.setState({ editMode: false });
  };

  deleteBill = () => {
    let billingService = new Billing();
    if (this.state.id !== undefined) {
      this.setState({ loading: true });
      billingService
        .deleteEntity(this.state.id)
        .then((res) => {
          this.setState({ loading: false });
          alert("Successfully Deleted Bill");
          window.location.href = "/billing";
        })
        .catch((error) => {
          console.error(`Error deleting bill`, error);
          this.setState({ loading: false });
        });
    } else {
      return <Redirect to="/lab/templates" />;
    }
  };

  handleChange(e, index) {
    this.state.billItems[index] = e.target.value;
    this.setState({ billItems: this.state.billItems });
  }

  removeField(index, mode) {
    if (mode) {
      this.state.billItems.splice(index, 1);
      this.setState({ billItems: this.state.billItems });
    }
  }

  componentDidMount() {
    if (this.props && this.props.id) {
      const id = this.props.id;
      this.setState({ loading: true });
      let billingService = new Billing();
      billingService
        .getEntity(id)
        .then((res) => {
          if (res.data !== undefined) {
            if (res.data.status === "PAID" || res.data.status === "CANCELLED") {
              this.setState({ editMode: false, loading: false });
            }
            this.setState({
              loading: false,
              id: res.data.id,
              patientId: res.data.patientDetails.id,
              docterReferenceId: res.data.doctorDetails
                ? res.data.doctorDetails.id
                : null,
              doctorName: res.data.doctorDetails
                ? res.data.doctorDetails.firstName
                : "",
              comments: res.data.comments,
              status: res.data.status,
              discount: res.data.discount,
              version: res.data.version,
              billItems: res.data.billItems,
              amountPaid: res.data.amountPaid,
              paymentRefId: res.data.paymentRefId,
              paymentType: res.data.paymentType,
              code: res.data.code,
              createdAt: res.data.createdAt,
              createdBy: res.data.createdBy,
              patientObj: res.data.patientDetails,
              transactionDetails: res.data.transactionDetails,
            });
            this.setTotal();
          }
        })
        .catch((error) => {
          console.error(`Error fetching bill details: `, error);
          this.setState({ loading: false });
        });
    }
  }

  isAdmin = () => {
    return Storage.get("isAdmin") === "true";
  };
  patientAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .patientAutoSearch(event.target.value)
      .then((res) => this.setState({ patientAutoSearchData: res.data }));
  };

  referrerAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .referrerAutoSearch(event.target.value)
      .then((res) => this.setState({ referrerAutoSearchData: res.data }));
  };

  doctorAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .doctorAutoSearch(event.target.value)
      .then((res) => this.setState({ doctorAutoSearchData: res.data }));
  };

  autoSearch = (event) => {
    let billingService = new Billing();
    billingService
      .autoSearch(event.target.value)
      .then((res) => this.setState({ autoSearchData: res.data }));
  };

  unSetDoctor() {
    this.setState({ docterReferenceId: null });
  }

  render() {
    const { classes } = this.props;
    return this.state.loading ? (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <HashLoader
          color={"#00ffff"}
          loading={true}
          size={50}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
      </div>
    ) : (
      <main className={classes.content}>
        <PdfDialog
          open={this.state.open}
          onClose={this.handleClose}
          header={true}
          type="bill"
          id={this.state.id}
        />
        <Typography
          variant="h5"
          style={{
            fontWeight: "bold",
            padding: "8px 16px",
            borderBottom: "2px solid #f0f0f0",
            color: "#3f51b5",
            textAlign: "center",
          }}
        >
          {this.state.id}{" "}
          {this.state.id !== null
            ? this.state.editMode
              ? " - Edit Bill"
              : ""
            : "Generate Bill"}{" "}
          (Status - {this.state.status})
        </Typography>
        <Paper className={classes.paper}>
          {!this.state.id ? (
            <Box
              display="grid"
              gridTemplateColumns="1fr 2fr 1.2fr 1fr 1.2fr 1.4fr 1.2fr 1fr 2fr 0.8fr 1.5fr 2fr"
              gap={2}
              padding={2}
              style={{ border: "1px solid #ddd", borderRadius: "8px" }}
            >
              <Autocomplete
                options={this.state.patientAutoSearchData}
                getOptionLabel={(option) =>
                  `${option.code} - ${option.firstName} - ${option.lastName} - ${option.phone}`
                }
                style={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Patient"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={this.patientAutoSearch}
                  />
                )}
                onChange={(_event, option) => {
                  if (option)
                    this.setState({ patientObj: option, patientId: option.id });
                }}
              />
              <Autocomplete
                options={this.state.doctorAutoSearchData}
                getOptionLabel={(option) =>
                  `${option.firstName} ${option.lastName} - ${option.specialization}`
                }
                style={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Doctor"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={this.doctorAutoSearch}
                  />
                )}
                onChange={(_event, option) => {
                  if (option)
                    this.setState({
                      docterReferenceId: option.id,
                      doctorName: option.firstName,
                    });
                }}
              />
              <Autocomplete
                options={this.state.referrerAutoSearchData}
                getOptionLabel={(option) =>
                  `${option.code} - ${option.firstName} - ${option.lastName} - ${option.phone}`
                }
                style={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Referrer"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={this.referrerAutoSearch}
                  />
                )}
                onChange={(_event, option) => {
                  if (option)
                    this.setState({
                      referrerObj: option,
                      referrerId: option.id,
                    });
                }}
              />
            </Box>
          ) : null}
          <Box
            display="grid"
            gridTemplateColumns="1fr 2fr 1.2fr 1fr 1.2fr 1.4fr 1.2fr 1fr 2fr 0.8fr 1.5fr 2fr"
            gap={2}
            padding={2}
            style={{ border: "1px solid #ddd", borderRadius: "8px" }}
          >
            {/* Header Row */}
            {[
              "Patient ID",
              "Name",
              "Date of Birth",
              "Gender",
              "Phone",
              "Aadhaar",
              "Marital Status",
              "Blood Group",
              "Doctor Name",
              "Status",
              "Billed By",
              "Created At",
            ].map((header, index) => (
              <Typography
                key={index}
                style={{
                  fontWeight: "bold",
                  fontSize: "12px",
                  textAlign: "center",
                }}
              >
                {header}
              </Typography>
            ))}

            {/* Data Row */}
            <Typography align="center">
              {this.state.patientObj?.code}
            </Typography>
            <Typography align="center">
              {this.state.patientObj?.firstName}{" "}
              {this.state.patientObj?.surname || ""}
            </Typography>
            <Typography align="center">{this.state.patientObj?.dob}</Typography>
            <Typography align="center">
              {this.state.patientObj?.gender}
            </Typography>
            <Typography align="center">
              {this.state.patientObj?.phone}
            </Typography>
            <Typography align="center">
              {this.state.patientObj?.maskedAadhaar || ""}
            </Typography>
            <Typography align="center">
              {this.state.patientObj?.maritialStatus}
            </Typography>
            <Typography align="center">
              {this.state.patientObj?.bloodGroup
                .replace(" POSITIVE", "+")
                .replace(" NEGATIVE", "-")}
            </Typography>
            <Typography align="center">
              {this.state.doctorName || "Self"}
            </Typography>
            <Typography align="center">{this.state.status}</Typography>
            <Typography align="center">{this.state.createdBy}</Typography>
            {this.state.dateEditMode ? (
              <TextField
                type="datetime-local"
                inputProps={{
                  min: "2020-01-01T00:00", // Replace this with your desired minimum date
                  max: new Date().toISOString().slice(0, 16), // Current date and time
                }}
                onKeyDown={(e) => e.preventDefault()}
                value={this.state.createdAt}
                onChange={this.handleValueChange}
                onBlur={this.handleEditToggle} // Exits edit mode on blur
                fullWidth
              />
            ) : (
              <Typography
                align="center"
                onClick={this.handleEditToggle}
                style={{ cursor: "pointer" }}
              >
                {this.state.createdAt}
              </Typography>
            )}
          </Box>
        </Paper>
        <br />
        <Paper className={classes.paper}>
          <Typography
            variant="h5"
            style={{
              fontWeight: "bold",
              padding: "8px 16px",
              borderBottom: "2px solid #f0f0f0",
              color: "#3f51b5",
              textAlign: "center",
            }}
          >
            <b>Bill Items</b>
          </Typography>
          <br />
          <div align="left">
            {this.state.editMode && !this.state.id && (
              <Autocomplete
                options={this.state.autoSearchData}
                getOptionLabel={(option) =>
                  `${option.itemReferenceName} - ${option.billItemType}`
                }
                style={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Item"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={this.autoSearch}
                  />
                )}
                onChange={(_event, option) => {
                  if (option) {
                    this.setState({
                      billItems: [
                        ...this.state.billItems,
                        {
                          itemReferenceId: option.itemReferenceId,
                          itemReferenceCode: option.itemReferenceCode,
                          itemName: option.itemReferenceName,
                          discount: 0,
                          price: option.price,
                          status: "PENDING",
                          billItemType: option.billItemType,
                          addedBy: Storage.get("userId"),
                          addedByName: Storage.get("userName"),
                        },
                      ],
                    });
                  }
                }}
              />
            )}
          </div>

          <TableContainer>
            <Table
              stickyHeader
              className={classes.table}
              size="small"
              style={{ tableLayout: "fixed" }}
            >
              <TableHead>
                <TableRow>
                  <TableCell
                    align="center"
                    style={{
                      fontWeight: "bold",
                      fontSize: "12px",
                      width: "80px",
                    }}
                  >
                    #
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      fontWeight: "bold",
                      fontSize: "12px",
                      width: "120px",
                    }}
                  >
                    Item Id
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      fontWeight: "bold",
                      fontSize: "12px",
                      width: "150px",
                    }}
                  >
                    Item Name
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      fontWeight: "bold",
                      fontSize: "12px",
                      width: "100px",
                    }}
                  >
                    Item Type
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      fontWeight: "bold",
                      fontSize: "12px",
                      width: "120px",
                    }}
                  >
                    Added By
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      fontWeight: "bold",
                      fontSize: "12px",
                      width: "100px",
                    }}
                  >
                    Amount
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      fontWeight: "bold",
                      fontSize: "12px",
                      width: "80px",
                    }}
                  >
                    Discount (In Rs)
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      fontWeight: "bold",
                      fontSize: "12px",
                      width: "120px",
                    }}
                  >
                    Total Amount
                  </TableCell>
                  <TableCell
                    align="center"
                    style={{
                      fontWeight: "bold",
                      fontSize: "12px",
                      width: "40px",
                    }}
                  ></TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.billItems.map((item, index) => (
                  <TableRow key={index} hover>
                    <TableCell align="center" style={{ width: "80px" }}>
                      {index + 1}
                    </TableCell>
                    <TableCell align="center" style={{ width: "120px" }}>
                      {item.itemReferenceId}
                    </TableCell>
                    <TableCell align="center" style={{ width: "150px" }}>
                      {item.itemName}
                    </TableCell>
                    <TableCell align="center" style={{ width: "100px" }}>
                      {item.billItemType}
                    </TableCell>
                    <TableCell align="center" style={{ width: "120px" }}>
                      {item.addedByName}
                    </TableCell>
                    <TableCell align="center" style={{ width: "100px" }}>
                      Rs {item.price}
                    </TableCell>
                    <TableCell align="center" style={{ width: "80px" }}>
                      {this.state.editMode && !this.state.id ? (
                        <TextField
                          margin="dense"
                          variant="outlined"
                          value={item.discount}
                          onChange={this.handleBillItemsChange(
                            index,
                            "discount",
                          )}
                          style={{ width: "60px" }}
                        />
                      ) : (
                        item.discount
                      )}
                    </TableCell>
                    <TableCell align="center" style={{ width: "120px" }}>
                      Rs {this.setItemTotal(index)}
                    </TableCell>
                    <TableCell align="center" style={{ width: "40px" }}>
                      {this.state.editMode && (
                        <RemoveCircleOutlineOutlinedIcon
                          onClick={() =>
                            this.removeField(index, this.state.editMode)
                          }
                        />
                      )}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <br />
          {this.state.id != null && this.state.transactionDetails != null ? (
            <Paper className={classes.paper}>
              <Typography
                variant="h6"
                style={{
                  fontWeight: "bold",
                  padding: "8px 16px",
                  borderBottom: "2px solid #f0f0f0",
                  color: "#3f51b5",
                  textAlign: "center",
                }}
              >
                <b>Transaction Details</b>
              </Typography>
              <br />
              <TableContainer>
                <Table stickyHeader className={classes.table} size="small">
                  <TableHead>
                    <TableRow>
                      <TableCell
                        align="center"
                        style={{
                          fontWeight: "bold",
                          fontSize: "12px",
                          width: "40px",
                        }}
                      >
                        Payment Method
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{
                          fontWeight: "bold",
                          fontSize: "12px",
                          width: "40px",
                        }}
                      >
                        Payment Reference ID
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{
                          fontWeight: "bold",
                          fontSize: "12px",
                          width: "40px",
                        }}
                      >
                        Txn Amount
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{
                          fontWeight: "bold",
                          fontSize: "12px",
                          width: "40px",
                        }}
                      >
                        Status
                      </TableCell>
                      <TableCell
                        align="center"
                        style={{
                          fontWeight: "bold",
                          fontSize: "12px",
                          width: "40px",
                        }}
                      >
                        Payment Date
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {this.state.transactionDetails.map((txn, index) => (
                      <TableRow key={index} hover>
                        <TableCell align="center" style={{ width: "40px" }}>
                          {txn.paymentMethod}
                        </TableCell>
                        <TableCell align="center" style={{ width: "40px" }}>
                          {txn.paymentRefId}
                        </TableCell>
                        <TableCell align="center" style={{ width: "40px" }}>
                          Rs {txn.txnAmount.toFixed(2)}
                        </TableCell>
                        <TableCell align="center" style={{ width: "40px" }}>
                          {txn.status}
                        </TableCell>
                        <TableCell align="center" style={{ width: "40px" }}>
                          {new Date(txn.paymentDate).toLocaleString()}
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>

              <br />

              <div align="right">
                <Typography>Total Amount: Rs. {this.setTotal()}</Typography>
                <br />
                <Typography>
                  Total Amount Paid: Rs. {this.state.amountPaid}
                </Typography>
              </div>
              {this.state.editMode && this.state.id ? (
                <Grid
                  container
                  spacing={2}
                  style={{
                    justifyContent: "flex-end",
                    align: "right",
                    gap: "10px",
                    alignItems: "right",
                    paddingTop: "20px",
                    paddingBottom: "10px",
                    flexDirection: "row",
                  }}
                >
                  <Grid item>
                    <FormControl
                      variant="outlined"
                      margin="dense"
                      style={{ minWidth: 200 }}
                    >
                      <InputLabel>Payment Method</InputLabel>
                      <Select
                        value={this.state.paymentType}
                        onChange={this.handleBillItemsChange(-1, "paymentType")}
                        label="Payment Method"
                      >
                        {paymentModes.map((mode) => (
                          <MenuItem key={mode} value={mode}>
                            {mode.replace(/_/g, " ")}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item>
                    <TextField
                      label="Payment Reference"
                      margin="dense"
                      variant="outlined"
                      value={this.state.paymentRefId}
                      onChange={this.handleBillItemsChange(-1, "paymentRefId")}
                    />
                  </Grid>
                  <Grid item>
                    <TextField
                      label="Transaction Amount"
                      margin="dense"
                      variant="outlined"
                      value={this.state.txnAmount}
                      onChange={this.handleBillItemsChange(-1, "txnAmount")}
                    />
                  </Grid>
                </Grid>
              ) : null}
            </Paper>
          ) : null}
        </Paper>
        <br />
        <Grid container spacing={2} justifyContent="flex-end">
          {this.renderRedirect()}
          {this.state.editMode &&
          this.state.patientId &&
          this.state.billItems &&
          this.state.billItems.length > 0 &&
          this.setTotal() >= 0 ? (
            <Grid container spacing={2} justifyContent="flex-end">
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  component="span"
                  className={classes.buttonSpacing}
                  onClick={this.submitForm}
                >
                  Save
                </Button>
              </Grid>
              {this.state.id && this.isAdmin() ? (
                <Grid item>
                  <Button
                    variant="contained"
                    color="secondary"
                    component="span"
                    className={classes.buttonSpacing}
                    onClick={this.deleteBill}
                  >
                    Delete
                  </Button>
                </Grid>
              ) : null}
              <Grid item>
                <Button
                  variant="outlined"
                  color="primary"
                  component="span"
                  onClick={this.setRedirect}
                  sx={{ backgroundColor: "black", color: "white" }}
                  className={classes.buttonSpacing}
                >
                  Close
                </Button>
              </Grid>
            </Grid>
          ) : !this.state.editMode && this.state.status !== "CANCELLED" ? (
            <Grid container spacing={2} justifyContent="flex-end">
              {this.state.billItems.find(
                (item) => item.billItemType === "LAB",
              ) ? (
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    component="span"
                    className={classes.buttonSpacing}
                    onClick={() => {
                      window.location.href = `/tests/bill/${this.state.id}`;
                    }}
                  >
                    Reports
                  </Button>
                </Grid>
              ) : (
                ""
              )}
              {this.state.billItems.find(
                (item) => item.billItemType === "CONSULTATION_FEE",
              ) ? (
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    component="span"
                    className={classes.buttonSpacing}
                    onClick={() => {
                      window.location.href = `/appointments/bill/${this.state.id}`;
                    }}
                  >
                    Consultation
                  </Button>
                </Grid>
              ) : (
                ""
              )}
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  component="span"
                  className={classes.buttonSpacing}
                  onClick={this.handleOpen}
                >
                  Receipt
                </Button>
              </Grid>
              <Grid item>
                <Button
                  variant="outlined"
                  color="primary"
                  component="span"
                  onClick={this.setRedirect}
                  sx={{ backgroundColor: "black", color: "white" }}
                  className={classes.buttonSpacing}
                >
                  Close
                </Button>
              </Grid>
            </Grid>
          ) : (
            <Button
              variant="outlined"
              color="primary"
              component="span"
              onClick={this.setRedirect}
              sx={{ backgroundColor: "black", color: "white" }}
              className={classes.buttonSpacing}
            >
              Close
            </Button>
          )}
        </Grid>
      </main>
    );
  }
}
export default withStyles(useStyles, { withTheme: true })(BillingTemplate);
