import React, { useEffect, useState } from "react";
import Config from "../services/Config";
import axios from "axios";
import {
  Container,
  Typography,
  TextField,
  Button,
  Grid,
  Paper,
  Snackbar,
  Alert,
  Box,
} from "@mui/material";
import Storage from "../storage/storage";
import LoginButton from "./loginButton";

const ConfigDisplay = () => {
  const [appName, setAppName] = useState("");
  const [logo, setLogo] = useState(null);
  const [currentSettings, setCurrentSettings] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [successMessage, setSuccessMessage] = useState("");
  const [logoExists, setLogoExists] = useState(false);
  const [addressLine1, setAddressLine1] = React.useState("");
  const [addressLine2, setAddressLine2] = React.useState("");
  const [city, setCity] = React.useState("");
  const [state, setState] = React.useState("");
  const [postalCode, setPostalCode] = React.useState("");
  const [primaryContact, setPrimaryContact] = React.useState("");
  const [secondaryContact, setSecondaryContact] = React.useState("");
  const [emergencyContact, setEmergencyContact] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [tagLine, setTagLine] = React.useState("");
  const [appointmentBooking, setAppointmentBooking] = React.useState("");
  const [receptionDesk, setReceptionDesk] = React.useState("");
  const [loc1, setLoc1] = React.useState("");
  const [loc2, setLoc2] = React.useState("");
  const [loc3, setLoc3] = React.useState("");
  const [loc4, setLoc4] = React.useState("");
  const [editMode, setEditMode] = React.useState(false);

  useEffect(() => {
    const fetchSettings = async () => {
      try {
        let configService = new Config();
        const settingsData = await configService.getConfigSettings();
        setCurrentSettings(settingsData.data);
        Storage.set("logo", currentSettings.logoPath);
        Storage.set("appName", currentSettings.appName);
        setAppName(currentSettings.appName);
        setTagLine(currentSettings.tagLine);
        setAddressLine1(currentSettings.addressLine1);
        setAddressLine2(currentSettings.addressLine2);
        setCity(currentSettings.city);
        setState(currentSettings.state);
        setPostalCode(currentSettings.postalCode);
        setPrimaryContact(currentSettings.primaryContact);
        setSecondaryContact(currentSettings.secondaryContact);
        setEmergencyContact(currentSettings.emergencyContact);
        setAppointmentBooking(currentSettings.appointmentBooking);
        setEmail(currentSettings.email);
        await checkLogoExists(settingsData.logoPath);
      } catch (err) {
      } finally {
        setLoading(false);
      }
    };

    fetchSettings();
  }, []);

  const checkLogoExists = async (logoPath) => {
    try {
      const response = await fetch(logoPath, { method: "HEAD" });
      setLogoExists(response.ok); // Set to true if the logo exists
    } catch (err) {
      setLogoExists(false); // Set to false if an error occurs
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const formData = new FormData();
    if (appName && appName.trim().length != 0) formData.append("name", appName);
    if (logo) formData.append("logo", logo);
    if (addressLine1 && addressLine1.trim().length != 0)
      formData.append("addressLine1", addressLine1);
    if (addressLine2 && addressLine2.trim().length != 0)
      formData.append("addressLine2", addressLine2);
    if (city && city.trim().length != 0) formData.append("city", city);
    if (state && state.trim().length != 0) formData.append("state", state);
    if (postalCode && postalCode.trim().length != 0)
      formData.append("postalCode", postalCode);
    if (primaryContact && primaryContact.trim().length != 0)
      formData.append("primaryContact", primaryContact);
    if (secondaryContact && secondaryContact.trim().length != 0)
      formData.append("secondaryContact", secondaryContact);
    if (emergencyContact && emergencyContact.trim().length != 0)
      formData.append("emergencyContact", emergencyContact);
    if (email && email.trim().length != 0) formData.append("email", email);
    if (tagLine && tagLine.trim().length != 0)
      formData.append("tagLine", tagLine);
    if (appointmentBooking && appointmentBooking.trim().length != 0)
      formData.append("appointmentBooking", appointmentBooking);
    if (receptionDesk && receptionDesk.trim().length != 0)
      formData.append("receptionDesk", receptionDesk);
    var locs = [];
    if (loc1 && loc1.trim().length != 0) locs.push(loc1.replace(/\\/g, "/"));
    if (loc2 && loc2.trim().length != 0) locs.push(loc2.replace(/\\/g, "/"));
    if (loc3 && loc3.trim().length != 0) locs.push(loc3.replace(/\\/g, "/"));
    if (loc4 && loc4.trim().length != 0) locs.push(loc4.replace(/\\/g, "/"));
    if (locs.length > 0) {
      const userConfirmed = window.confirm(
        "Make sure the backup locations are valid, otherwise backups will fail. Do you want to continue?",
      );
      if (!userConfirmed) {
        // Cancel the backup operation
        return;
      }
      formData.append("backupLocations", locs);
    }
    try {
      let configService = new Config();
      await configService.saveConfigProperties(formData).then((res) => {
        if (res && res.data) {
          window.location.href = "/config";
        }
      });
    } catch (err) {
      setError("Failed to update settings");
    }
    await checkLogoExists(currentSettings.logoPath);
  };

  const handleTokenReceived = async (token) => {
    try {
      let configService = new Config();
      const configData = await configService.postToken(token).then((res) => {
        if (res && res.data === "true") alert("Enabled Google Backup");
      });
    } catch (error) {
      console.error("Error sending token:", error);
    }
  };

  const handleEdit = () => {
    setEditMode(true);
  };

  const handleLogoChange = (event) => {
    setLogo(event.target.files[0]);
  };

  const handleCloseSnackbar = () => {
    setError(null);
    setSuccessMessage("");
  };

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <Container maxWidth="false" sx={{ marginTop: 3 }}>
      <Grid container spacing={4}>
        <Grid item md={12}>
          <Paper elevation={3} sx={{ padding: 3 }}>
            <Typography variant="h4" gutterBottom>
              <b>{editMode ? "Update Settings" : "Current Settings"}</b>
            </Typography>
            <form onSubmit={handleSubmit}>
              <TextField
                label="Application Name"
                variant="outlined"
                disabled={!editMode}
                fullWidth
                margin="normal"
                value={editMode ? appName : currentSettings.appName}
                onChange={(e) => setAppName(e.target.value)}
              />
              <TextField
                label="Tag Line"
                variant="outlined"
                fullWidth
                disabled={!editMode}
                margin="normal"
                value={editMode ? tagLine : currentSettings.tagLine}
                onChange={(e) => setTagLine(e.target.value)}
              />

              <Typography variant="h5" gutterBottom sx={{ marginTop: 3 }}>
                <b>Address Information</b>
              </Typography>
              <TextField
                label="Address Line 1"
                variant="outlined"
                fullWidth
                disabled={!editMode}
                margin="normal"
                value={editMode ? addressLine1 : currentSettings.addressLine1}
                onChange={(e) => setAddressLine1(e.target.value)}
              />
              <TextField
                label="Address Line 2"
                variant="outlined"
                fullWidth
                disabled={!editMode}
                margin="normal"
                value={editMode ? addressLine2 : currentSettings.addressLine2}
                onChange={(e) => setAddressLine2(e.target.value)}
              />
              <TextField
                label="City"
                variant="outlined"
                fullWidth
                disabled={!editMode}
                margin="normal"
                value={editMode ? city : currentSettings.city}
                onChange={(e) => setCity(e.target.value)}
              />
              <TextField
                label="State"
                variant="outlined"
                fullWidth
                disabled={!editMode}
                margin="normal"
                value={editMode ? state : currentSettings.state}
                onChange={(e) => setState(e.target.value)}
              />
              <TextField
                label="Postal Code"
                variant="outlined"
                fullWidth
                disabled={!editMode}
                margin="normal"
                value={editMode ? postalCode : currentSettings.postalCode}
                onChange={(e) => setPostalCode(e.target.value)}
              />

              <Typography variant="h5" gutterBottom sx={{ marginTop: 3 }}>
                <b>Contact Information</b>
              </Typography>
              <TextField
                label="Primary Contact Number"
                variant="outlined"
                fullWidth
                disabled={!editMode}
                margin="normal"
                value={
                  editMode ? primaryContact : currentSettings.primaryContact
                }
                onChange={(e) => setPrimaryContact(e.target.value)}
              />
              <TextField
                label="Secondary Contact Number"
                variant="outlined"
                fullWidth
                disabled={!editMode}
                margin="normal"
                value={
                  editMode ? secondaryContact : currentSettings.secondaryContact
                }
                onChange={(e) => setSecondaryContact(e.target.value)}
              />
              <TextField
                label="Emergency Contact Number"
                variant="outlined"
                fullWidth
                disabled={!editMode}
                margin="normal"
                value={
                  editMode ? emergencyContact : currentSettings.emergencyContact
                }
                onChange={(e) => setEmergencyContact(e.target.value)}
              />
              <TextField
                label="Appointment Booking Number"
                variant="outlined"
                fullWidth
                disabled={!editMode}
                margin="normal"
                value={
                  editMode
                    ? appointmentBooking
                    : currentSettings.appointmentBooking
                }
                onChange={(e) => setAppointmentBooking(e.target.value)}
              />
              <TextField
                label="Email"
                variant="outlined"
                fullWidth
                disabled={!editMode}
                margin="normal"
                value={editMode ? email : currentSettings.email}
                onChange={(e) => setEmail(e.target.value)}
              />
              <Typography
                variant="h5"
                gutterBottom
                sx={{ marginTop: 3, mb: 2 }}
              >
                <b>Back Up Locations</b>
              </Typography>
              {!editMode ? (
                !currentSettings.backupLocations ||
                currentSettings.backupLocations.length === 0 ? (
                  <Box sx={{ display: "flex", alignItems: "center", mb: 4 }}>
                    <Typography sx={{ mr: 2 }}>
                      <strong>No backup locations available</strong>
                    </Typography>
                  </Box>
                ) : (
                  <div>
                    {currentSettings.backupLocations.map((location, index) => (
                      <Box
                        sx={{ display: "flex", alignItems: "center", mb: 4 }}
                        key={index}
                      >
                        <Typography sx={{ ml: 1 }}>
                          <strong>Back Up Location {index + 1}: </strong>{" "}
                          {location.replace("/", "\\") || "N/A"}
                        </Typography>
                      </Box>
                    ))}
                    <Box sx={{ display: "flex", alignItems: "center", mb: 1 }}>
                      <Typography sx={{ mr: 2 }}>
                        <strong>Google Backup Enabled: </strong>{" "}
                        {currentSettings.googleDriveBackUpEnabled
                          ? "Yes"
                          : "No"}
                      </Typography>
                    </Box>
                  </div>
                )
              ) : (
                <div>
                  <TextField
                    label="Location 1"
                    variant="outlined"
                    fullWidth
                    margin="normal"
                    value={loc1}
                    onChange={(e) => setLoc1(e.target.value)}
                  />
                  <TextField
                    label="Location 2"
                    variant="outlined"
                    fullWidth
                    margin="normal"
                    value={loc2}
                    onChange={(e) => setLoc2(e.target.value)}
                  />
                  <TextField
                    label="Location 3"
                    variant="outlined"
                    fullWidth
                    margin="normal"
                    value={loc3}
                    onChange={(e) => setLoc3(e.target.value)}
                  />
                  <TextField
                    label="Location 4"
                    variant="outlined"
                    fullWidth
                    margin="normal"
                    value={loc4}
                    onChange={(e) => setLoc4(e.target.value)}
                  />
                  {/*
                  <Box sx={{ display: "flex", alignItems: "center", mt: 2 }}>
                    <LoginButton onTokenReceived={handleTokenReceived} />
                  </Box>
                  */}
                </div>
              )}
              <Typography
                variant="h5"
                gutterBottom
                sx={{ marginTop: 3, mb: 4 }}
              >
                <b>Logo</b>
              </Typography>
              {editMode ? (
                <Grid container spacing={2}>
                  <Grid item xs={12} md={6}>
                    <Button
                      variant="contained"
                      component="label"
                      fullWidth
                      sx={{ marginRight: 1 }} // Adjust margin if needed
                    >
                      Upload Logo
                      <input
                        type="file"
                        hidden
                        accept="image/*"
                        onChange={handleLogoChange}
                        required
                      />
                    </Button>
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <Typography
                      sx={{
                        display: "flex",
                        alignItems: "center",
                        height: "100%",
                      }}
                    >
                      {logo ? `${logo.name}` : "No file selected"}
                    </Typography>
                  </Grid>
                </Grid>
              ) : currentSettings.logoPath ? (
                <img
                  src={currentSettings.logoPath}
                  alt="Logo"
                  style={{ maxWidth: "200px", marginLeft: "10px" }}
                />
              ) : (
                ""
              )}
              {editMode ? (
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: "20px",
                  }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    disabled={
                      !(
                        appName ||
                        logo ||
                        addressLine1 ||
                        addressLine2 ||
                        city ||
                        state ||
                        postalCode ||
                        primaryContact ||
                        secondaryContact ||
                        emergencyContact ||
                        email ||
                        tagLine ||
                        appointmentBooking ||
                        receptionDesk ||
                        loc1 ||
                        loc2 ||
                        loc3 ||
                        loc4
                      )
                    }
                    onClick={handleSubmit}
                  >
                    Save Settings
                  </Button>
                  <Button
                    variant="outlined"
                    color="secondary"
                    onClick={() => {
                      setAppName(currentSettings.appName);
                      setTagLine(currentSettings.tagLine);
                      setAddressLine1(currentSettings.addressLine1);
                      setAddressLine2(currentSettings.addressLine2);
                      setCity(currentSettings.city);
                      setState(currentSettings.state);
                      setPostalCode(currentSettings.postalCode);
                      setPrimaryContact(currentSettings.primaryContact);
                      setSecondaryContact(currentSettings.secondaryContact);
                      setEmergencyContact(currentSettings.emergencyContact);
                      setAppointmentBooking(currentSettings.appointmentBooking);
                      setEmail(currentSettings.email);
                      setLoc1("");
                      setLoc2("");
                      setLoc3("");
                      setLoc4("");
                      setEditMode(false);
                    }}
                  >
                    Close
                  </Button>
                </Box>
              ) : (
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginTop: "20px",
                  }}
                >
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={handleEdit}
                  >
                    Edit Settings
                  </Button>
                </Box>
              )}
            </form>
          </Paper>
        </Grid>
      </Grid>
      <Snackbar
        open={!!error}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <Alert onClose={handleCloseSnackbar} severity="error">
          {error}
        </Alert>
      </Snackbar>

      <Snackbar
        open={!!successMessage}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
      >
        <Alert onClose={handleCloseSnackbar} severity="success">
          {successMessage}
        </Alert>
      </Snackbar>
    </Container>
  );
};

export default ConfigDisplay;
