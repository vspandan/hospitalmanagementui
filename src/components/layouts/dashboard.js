import React, { useState, useEffect } from "react";
import {
  PieChart,
  Pie,
  Cell,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TableContainer,
  Paper,
  Button,
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
} from "@mui/material";
import DashboardService from "../services/Dashboard";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import { makeStyles } from "@mui/styles";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

const useStyles = makeStyles((theme) => ({
  container: {
    padding: theme.spacing(3),
    backgroundColor: "#f4f6f8",
    borderRadius: "8px",
    boxShadow: "0 4px 6px rgba(0, 0, 0, 0.1)",
  },
  title: {
    fontSize: "2rem",
    fontWeight: "600",
    marginBottom: theme.spacing(2),
    color: "#2c3e50",
  },
  subTitle: {
    fontSize: "1.5rem",
    fontWeight: "500",
    marginBottom: theme.spacing(2),
    color: "#007bff",
  },
  buttonGroup: {
    display: "flex",
    justifyContent: "space-around",
    marginBottom: theme.spacing(3),
  },
  chartContainer: {
    marginBottom: theme.spacing(3),
    borderRadius: "8px",
    boxShadow: "0 4px 6px rgba(0, 0, 0, 0.1)",
  },
  tableContainer: {
    marginTop: theme.spacing(3),
    borderRadius: "8px",
    boxShadow: "0 4px 6px rgba(0, 0, 0, 0.1)",
  },
  calendarContainer: {
    marginTop: theme.spacing(3),
    backgroundColor: "#ffffff",
    borderRadius: "8px",
    boxShadow: "0 4px 6px rgba(0, 0, 0, 0.1)",
    padding: theme.spacing(2),
  },
  tableHeader: {
    backgroundColor: "#f1f1f1",
  },
  valueCell: {
    fontWeight: "600",
    color: "#2c3e50",
  },
  chartTitle: {
    marginBottom: theme.spacing(2),
    fontWeight: "600",
  },
}));

function Dashboard() {
  const classes = useStyles();
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [dashboardData, setDashboardData] = useState(null);
  const [selectedPeriod, setSelectedPeriod] = useState("daily");
  const metricLabels = {
    totalTests: "Count",
    completedTests: "Completed Tests",
    totalAppointments: "Total Appointments",
    completedAppointments: "Scheduled Appointments",
    totalRevenue: "Total Revenue",
    totalBills: "Bill Count (Paid and Unpaid)",
  };
  // Create a map of user IDs to first + last name
  const testsMap = dashboardData?.testNames || {};

  const userMap = {
    ...(dashboardData?.userDetails?.reduce((acc, user) => {
      acc[user.id] = `${user.firstName} ${user.lastName}`;
      return acc;
    }, {}) || {}),
    ...testsMap,
  };

  // Function to render expandable sections
  const renderExpandableSection = (title, data) => (
    <Accordion>
      <AccordionSummary expandIcon={<ExpandMoreIcon />}>
        <Typography>{title}</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <TableContainer component={Paper}>
          <Table>
            <TableHead>
              <TableRow className={classes.tableHeader}>
                <TableCell>Name</TableCell>
                <TableCell align="center">Metric</TableCell>
                <TableCell align="center">Count</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {Object.entries(data).map(([metric, values]) =>
                Object.entries(values).map(([userId, value]) => (
                  <TableRow key={`${metric}-${userId}`}>
                    <TableCell>{userMap[userId] || "N/A"}</TableCell>
                    <TableCell align="left">
                      {metricLabels[metric] || metric}
                    </TableCell>
                    <TableCell align="left">{value}</TableCell>
                  </TableRow>
                )),
              )}
            </TableBody>
          </Table>
        </TableContainer>
      </AccordionDetails>
    </Accordion>
  );
  // Fetch data for the selected period
  const fetchData = (period) => {
    setSelectedPeriod(period);
    let dashboardService = new DashboardService();
    dashboardService
      .getData(period)
      .then((response) => {
        setDashboardData(response.data);
      })
      .catch((error) => console.error("Error fetching dashboard data", error));
  };

  const fetchDataForDate = (date) => {
    const formattedDate = date.toISOString().split("T")[0]; // Format the date as 'YYYY-MM-DD'
    const dashboardService = new DashboardService();
    dashboardService
      .getDataByDate(formattedDate) // Adjust service method as needed
      .then((response) => {
        setDashboardData(response.data);
      })
      .catch((error) => console.error("Error fetching dashboard data", error));
  };

  // Fetch data whenever the selected date changes
  useEffect(() => {
    if (selectedPeriod === "daily") {
      fetchDataForDate(selectedDate);
    }
  }, [selectedDate, selectedPeriod]);

  const dataForPieChart = [
    {
      name: "Scheduled Appointments",
      value: dashboardData ? dashboardData.completedAppointments : 0,
    },
  ];

  const dataForBarChart = [
    {
      name: "Appointments",
      value: dashboardData ? dashboardData.totalAppointments : 0,
    },
    {
      name: "Scheduled Appointments",
      value: dashboardData ? dashboardData.completedAppointments : 0,
    },
    { name: "Tests", value: dashboardData ? dashboardData.totalTests : 0 },
    {
      name: "Completed Tests",
      value: dashboardData ? dashboardData.completedTests : 0,
    },
  ];

  const normalizeToUTC = (date) => {
    const normalizedDate = new Date(
      Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()),
    );
    return normalizedDate;
  };

  const today = normalizeToUTC(new Date()); // Normalize the date to UTC
  const endOfDay = new Date(today);
  endOfDay.setUTCHours(23, 59, 59, 999); // Set to the end of the day (UTC)

  const handleDateChange = (date) => {
    setSelectedDate(normalizeToUTC(date)); // Store normalized UTC date
  };

  return (
    <div className={classes.container}>
      <h1 className={classes.title}>Dashboard</h1>

      <div className={classes.buttonGroup}>
        <Button variant="contained" onClick={() => fetchData("daily")}>
          Daily
        </Button>
        <Button variant="contained" onClick={() => fetchData("weekly")}>
          Weekly
        </Button>
        <Button variant="contained" onClick={() => fetchData("monthly")}>
          Monthly
        </Button>
      </div>

      {selectedPeriod === "daily" && (
        <div className={classes.calendarContainer}>
          <Calendar
            onChange={handleDateChange}
            value={selectedDate}
            maxDate={endOfDay}
          />
        </div>
      )}

      {dashboardData ? (
        <div>
          {/* Pie Chart */}
          {/*<div className={classes.chartContainer}>
            <h2 className={classes.chartTitle}>Appointments Summary</h2>
            <ResponsiveContainer width="100%" height={300}>
              <PieChart>
                <Pie
                  data={dataForPieChart}
                  dataKey="value"
                  nameKey="name"
                  cx="50%"
                  cy="50%"
                  outerRadius={80}
                  fill="#8884d8"
                  label
                >
                  {dataForPieChart.map((entry, index) => (
                    <Cell
                      key={`cell-${index}`}
                      fill={index % 2 === 0 ? "#00C49F" : "#FF8042"}
                    />
                  ))}
                </Pie>
              </PieChart>
            </ResponsiveContainer>
          </div>
          */}
          {/* Bar Chart */}
          <div className={classes.chartContainer}>
            <h2 className={classes.chartTitle}>
              Appointments and Tests Comparison
            </h2>
            <ResponsiveContainer width="100%" height={400}>
              <BarChart data={dataForBarChart}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Bar dataKey="value" fill="#8884d8" />
              </BarChart>
            </ResponsiveContainer>
          </div>

          {/* Tabular Data */}
          <div className={classes.tableContainer}>
            <h2 className={classes.subTitle}>
              Detailed Stats:{" "}
              {dashboardData.startDate
                ? new Date(dashboardData.startDate).toLocaleDateString(
                    "en-GB",
                    { day: "2-digit", month: "2-digit", year: "numeric" },
                  )
                : "N/A"}{" "}
              -
              {dashboardData.endDate
                ? new Date(dashboardData.endDate).toLocaleDateString("en-GB", {
                    day: "2-digit",
                    month: "2-digit",
                    year: "numeric",
                  })
                : "N/A"}
            </h2>
            <TableContainer component={Paper}>
              <Table>
                <TableHead>
                  <TableRow className={classes.tableHeader}>
                    <TableCell>Metric</TableCell>
                    <TableCell align="right">Value</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  <TableRow>
                    <TableCell>Total Appointments</TableCell>
                    <TableCell align="right" className={classes.valueCell}>
                      {dashboardData.totalAppointments}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Scheduled Appointments</TableCell>
                    <TableCell align="right" className={classes.valueCell}>
                      {dashboardData.completedAppointments}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Total Tests</TableCell>
                    <TableCell align="right" className={classes.valueCell}>
                      {dashboardData.totalTests}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Completed Tests</TableCell>
                    <TableCell align="right" className={classes.valueCell}>
                      {dashboardData.completedTests}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Total Revenue</TableCell>
                    <TableCell align="right" className={classes.valueCell}>
                      {dashboardData.totalRevenue !== null
                        ? `₹${parseFloat(dashboardData.totalRevenue).toFixed(
                            2,
                          )}`
                        : "₹0.00"}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Total Discount</TableCell>
                    <TableCell align="right" className={classes.valueCell}>
                      {dashboardData.totalDiscount !== null
                        ? `₹${parseFloat(dashboardData.totalDiscount).toFixed(
                            2,
                          )}`
                        : "₹0.00"}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell>Total Bills (Paid and Unpaid)</TableCell>
                    <TableCell align="right" className={classes.valueCell}>
                      {dashboardData.totalBills}
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </div>
          <br />
          <div>
            {/* Expandable Sections */}
            {renderExpandableSection(
              "Referrer Stats",
              dashboardData.referrerStats,
            )}
            {renderExpandableSection("Doctor Stats", dashboardData.doctorStats)}
            {renderExpandableSection(
              "Test-Wise Stats",
              dashboardData.testWiseStats,
            )}
          </div>
        </div>
      ) : (
        <p>Loading...</p>
      )}
    </div>
  );
}

export default Dashboard;
