import React, { Component } from "react";
import {
  Grid,
  Card,
  CardContent,
  Typography,
  CardActions,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  DialogActions,
  TextField,
  MenuItem,
  Alert,
} from "@mui/material";
import Identity from "../services/Identity"; // Replace with your actual service

class DoctorList extends Component {
  state = {
    doctors: [],
    selectedDoctor: {},
    open: false,
    pricedialogopen: false,
    gender: ["MALE", "FEMALE", "TRANS"],
    consultationfee: 0,
    updatedErrors: {
      firstName: "",
      lastName: "",
      gender: "",
      licenseNumber: "",
      specialization: "",
      aadhaar: "",
      price: "",
      phone: "",
    },
  };

  handleconsultationfee = (value) => {
    this.setState({ consultationfee: value });
  };

  handleFieldChange = (name, value) => {
    this.setState((prevState) => ({
      selectedDoctor: {
        ...prevState.selectedDoctor,
        doctorDetails: {
          ...prevState.selectedDoctor.doctorDetails,
          [name]: value,
        },
      },
    }));
  };

  handleConsultationFeeChange = (name, value) => {
    this.setState((prevState) => ({
      selectedDoctor: {
        ...prevState.selectedDoctor,
        [name]: value,
      },
    }));
  };

  componentDidMount() {
    this.fetchDoctors();
  }

  handleSave = () => {
    var identityService = new Identity();
    let isValid = true; // To track overall form validity

    // First Name validation
    if (
      !this.state.selectedDoctor.doctorDetails.firstName ||
      !this.state.selectedDoctor.doctorDetails.firstName.match(/^[A-Za-z]+$/)
    ) {
      isValid = false;
      this.state.updatedErrors["firstName"] =
        "First name must contain only letters.";
    } else {
      this.state.updatedErrors["firstName"] = "";
    }

    if (!this.state.selectedDoctor.doctorDetails.gender) {
      isValid = false;
      this.state.updatedErrors["gender"] = "Gender Selection is Mandatory";
    } else {
      isValid = true;
      this.state.updatedErrors["gender"] = "";
    }

    if (
      !this.state.selectedDoctor.doctorDetails.lastName ||
      !this.state.selectedDoctor.doctorDetails.lastName.match(/^[A-Za-z]+$/)
    ) {
      isValid = false;
      this.state.updatedErrors["lastName"] =
        "Last name must contain only letters.";
    } else {
      this.state.updatedErrors["lastName"] = "";
    }

    if (
      !this.state.selectedDoctor.doctorDetails.specialization ||
      !this.state.selectedDoctor.doctorDetails.specialization.match(
        /^[A-Za-z ]+$/,
      )
    ) {
      isValid = false;
      this.state.updatedErrors["specialization"] =
        "Specialization must contain only letters.";
    } else {
      this.state.updatedErrors["specialization"] = "";
    }

    if (!this.state.selectedDoctor.doctorDetails.licenseNumber) {
      isValid = false;
      this.state.updatedErrors["licenseNumber"] =
        "License Number is mandatory.";
    } else {
      this.state.updatedErrors["licenseNumber"] = "";
    }

    var regex = /^\d*\.?\d*$/;
    var value = this.state.selectedDoctor.price;

    if (value === "" || regex.test(value)) {
      if (!value || value.trim() === "") {
        value = 0;
      } else if (value < 0) {
        value = 0;
      }

      this.setState((prevState) => ({
        selectedDoctor: {
          ...prevState.selectedDoctor,
          price: value,
        },
      }));
    }

    // Aadhaar validation
    if (
      this.state.selectedDoctor.doctorDetails.aadhaar &&
      !this.state.selectedDoctor.doctorDetails.aadhaar.match(/^\d{12}$/)
    ) {
      isValid = false;
      this.state.updatedErrors["aadhaar"] =
        "Aadhaar must be a 12-digit number.";
    } else {
      this.state.updatedErrors["aadhaar"] = "";
    }

    if (
      !this.state.selectedDoctor.doctorDetails.phone ||
      !this.state.selectedDoctor.doctorDetails.phone.match(/^[0-9]{10}$/)
    ) {
      isValid = false;
      this.state.updatedErrors["phone"] =
        "Contact number must be a 10-digit number.";
    } else {
      this.state.updatedErrors["phone"] = "";
    }

    this.setState({ updatedErrors: this.state.updatedErrors });

    if (isValid) {
      identityService
        .updateDoctorDetails(this.state.selectedDoctor)
        .then((res) => {
          if (res.data) {
            alert("Successfully Updated Doctor Details");
            this.setState({ editMode: false, loading: false });
            window.location.href = "/doctors";
          } else {
            alert("Failed Doctor Details Updation");
            this.setState({ loading: false });
          }
        })
        .catch((error) => {
          console.error("Error updating doctor data:", error);
          this.setState({ loading: false });
        });
    }
  };
  handleClose = () => {
    this.setState({ open: false, pricedialogopen: false, selectedDoctor: {} });
  };

  fetchDoctors = () => {
    var docService = new Identity();
    docService
      .fetchAllDoctors() // Replace with your actual API call
      .then((response) => {
        this.setState({ doctors: response.data });
      })
      .catch((error) => {
        console.error("Error fetching doctor data:", error);
      });
  };

  render() {
    const { doctors } = this.state;

    return (
      <div>
        {this.state.selectedDoctor.doctorDetails && (
          <Dialog open={this.state.open} onClose={this.handleClose}>
            <DialogTitle>Edit Doctor Details</DialogTitle>
            <DialogContent>
              <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                  <TextField
                    label="First Name"
                    name="First Name"
                    value={
                      this.state.selectedDoctor.doctorDetails.firstName ?? ""
                    }
                    onChange={(e) =>
                      this.handleFieldChange("firstName", e.target.value)
                    }
                    fullWidth
                    margin="dense"
                    error={!!this.state.updatedErrors.firstName}
                    helperText={this.state.updatedErrors.firstName}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    label="Last Name"
                    name="Last Name"
                    value={this.state.selectedDoctor.doctorDetails.lastName}
                    onChange={(e) =>
                      this.handleFieldChange("lastName", e.target.value)
                    }
                    fullWidth
                    margin="dense"
                    error={!!this.state.updatedErrors.lastName}
                    helperText={this.state.updatedErrors.lastName}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    label="Specialization"
                    name="Specialization"
                    value={
                      this.state.selectedDoctor.doctorDetails.specialization
                    }
                    onChange={(e) =>
                      this.handleFieldChange("specialization", e.target.value)
                    }
                    fullWidth
                    margin="dense"
                    error={!!this.state.updatedErrors.specialization}
                    helperText={this.state.updatedErrors.specialization}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    label="Login Phone No."
                    name="loginPhoneNo"
                    type="number"
                    value={this.state.selectedDoctor.doctorDetails.phone}
                    onChange={(e) => {
                      this.handleFieldChange("phone", e.target.value);
                      this.handleFieldChange("loginPhoneNo", e.target.value);
                    }}
                    required
                    fullWidth
                    error={!!this.state.updatedErrors.phone}
                    helperText={this.state.updatedErrors.phone}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    label="Aadhaar No."
                    name="aadhaar"
                    type="number"
                    value={this.state.selectedDoctor.doctorDetails.aadhaar}
                    onChange={(e) =>
                      this.handleFieldChange("aadhaar", e.target.value)
                    }
                    fullWidth
                    error={!!this.state.updatedErrors.aadhaar}
                    helperText={this.state.updatedErrors.aadhaar}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    label="Gender"
                    name="gender"
                    fullWidth
                    value={this.state.selectedDoctor.doctorDetails.gender}
                    onChange={(e) =>
                      this.handleFieldChange("gender", e.target.value)
                    }
                    select
                    variant="outlined"
                    error={!!this.state.updatedErrors.gender}
                    helperText={this.state.updatedErrors.gender}
                  >
                    {this.state.gender.map((status) => (
                      <MenuItem key={status} value={status}>
                        {status}
                      </MenuItem>
                    ))}
                  </TextField>
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    variant="outlined"
                    label="License Number"
                    name="licenseNumber"
                    value={
                      this.state.selectedDoctor.doctorDetails.licenseNumber
                    }
                    onChange={(e) =>
                      this.handleFieldChange("licenseNumber", e.target.value)
                    }
                    required
                    fullWidth
                    error={!!this.state.updatedErrors.licenseNumber}
                    helperText={this.state.updatedErrors.licenseNumber}
                  />
                </Grid>
                <Grid item xs={12} md={6}>
                  <TextField
                    variant="outlined"
                    label="Consultation Fee"
                    name="price"
                    type="number"
                    value={this.state.selectedDoctor.price}
                    onChange={(e) =>
                      this.handleConsultationFeeChange("price", e.target.value)
                    }
                    required
                    fullWidth
                    error={!!this.state.updatedErrors.price}
                    helperText={this.state.updatedErrors.price}
                  />
                </Grid>
              </Grid>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="secondary">
                Cancel
              </Button>
              <Button onClick={this.handleSave} color="primary">
                Save
              </Button>
            </DialogActions>
          </Dialog>
        )}
        <Grid container spacing={3} style={{ padding: 16 }}>
          {doctors.map((doctor) => (
            <Grid
              item
              xs={12}
              sm={6}
              md={4}
              lg={3}
              key={doctor.doctorDetails.id}
            >
              <Card
                style={{
                  backgroundColor: "#f9f9f9",
                  borderRadius: 8,
                  boxShadow: "0 4px 10px rgba(0, 0, 0, 0.1)",
                }}
              >
                <CardContent>
                  <Typography variant="h6" style={{ fontWeight: "bold" }}>
                    {doctor.doctorDetails.firstName}{" "}
                    {doctor.doctorDetails.lastName}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    Code: {doctor.doctorDetails.code || "Not Specified"}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    Specialization:{" "}
                    {doctor.doctorDetails.specialization || "Not Specified"}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    License: {doctor.doctorDetails.licenseNumber || "N/A"}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    Phone: {doctor.doctorDetails.phone || "N/A"}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    Gender: {doctor.doctorDetails.gender || "N/A"}
                  </Typography>
                  <Typography variant="body2" color="textSecondary">
                    Consultation Fee : {doctor.price || 0}
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button
                    size="small"
                    color="primary"
                    onClick={() =>
                      this.handleViewProfile(doctor.doctorDetails.id)
                    }
                  >
                    Edit Profile
                  </Button>
                </CardActions>
              </Card>
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }

  handleViewProfile = (id) => {
    const doctor = this.state.doctors.find(
      (doc) => doc.doctorDetails.id === id,
    );
    const doctorDetails = doctor.doctorDetails;
    this.setState({ selectedDoctor: doctor, open: true });
    console.log("Viewing profile for doctor ID:", id);
  };
}

export default DoctorList;
