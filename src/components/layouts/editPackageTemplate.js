import React, { Component } from "react";
import Paper from "@mui/material/Paper";
import { withStyles } from "@mui/styles";
import {
  TextField,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  Select,
  RadioGroup,
  FormControlLabel,
  Radio,
  Grid,
} from "@mui/material";
import RemoveCircleOutlineOutlinedIcon from "@mui/icons-material/RemoveCircleOutlineOutlined";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import Typography from "@mui/material/Typography";
import { Redirect } from "react-router-dom";

import MasterTestReport from "../services/MasterTestReport";

import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import { HashLoader } from "react-spinners";
import useStyles from "../constants/styles";

class EditPackageReport extends Component {
  state = {
    id: null,
    reportName: "",
    redirect: false,
    price: 0,
    errors: {},
    loading: false,
  };

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/daycare/templates" />;
    }
  };

  handlePriceDetailsChange = (event) => {
    var regex = /^\d*\.?\d*$/;
    var value = event.target.value;
    if (value === "" || regex.test(value)) {
      if (!value || value.trim() === "") {
        value = 0;
      }
    } else {
      value = 0;
    }
    if (value < 0) {
      value = 0;
    }
    this.setState({ price: parseInt(value) });
  };

  handleReportNameChange = (event) => {
    this.state.reportName = event.target.value;
    this.setState({ reportName: this.state.reportName });
  };

  isSubmitEnabled() {
    return this.state.reportName && this.state.price && this.state.price > 0;
  }

  submitForm() {
    let data = {
      id: this.state.id,
      reportName: this.state.reportName,
      price: this.state.price,
      type: "DAY_CARE",
    };
    if (
      this.state.reportName.trim() != "" &&
      this.state.price &&
      this.state.price > 0
    ) {
      let masterTestReportService = new MasterTestReport();
      let d = this.props.id
        ? masterTestReportService.putEntity(data)
        : masterTestReportService.postEntity(data);
      this.setState({ loading: true });
      d.then(function (res) {
        let redirectUri = "/daycare/templates";
        window.location.href = redirectUri;
      }).catch((err) => {
        console.error("Error saving report template:", err);
        this.setState({ loading: false });
      });
    } else {
      alert("Fill all the requried fields");
      return;
    }
  }

  componentDidMount() {
    let masterTestReportService = new MasterTestReport();
    let id = this.props.id;
    if (id) {
      masterTestReportService
        .getMasterReport(id)
        .then((res) => {
          this.setState({
            id: id,
            reportName: res.data.reportName,
            price: res.data.price,
          });
        })
        .catch((err) => {
          console.error("Error fetching report template:", err);
          this.setState({ loading: false });
        });
    }
  }

  render() {
    const { classes } = this.props;
    return this.state.loading ? (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <HashLoader
          color={"#00ffff"}
          loading={true}
          size={50}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
      </div>
    ) : (
      <main className={classes.content}>
        <Typography
          variant="h4"
          style={{
            fontWeight: "bold",
            padding: "8px 16px",
            borderBottom: "2px solid #f0f0f0",
            color: "#3f51b5",
            textAlign: "center",
          }}
        >
          {this.props.id ? "Edit Package Template" : "New Package Template"}
        </Typography>
        <br />
        <Paper className={classes.paper}>
          <TextField
            label="Package Name"
            variant="outlined"
            fullWidth
            className={classes.textField}
            value={this.state.reportName}
            onChange={this.handleReportNameChange}
          />
        </Paper>
        <br />
        <div align="right">
          <TextField
            required
            margin="dense"
            variant="outlined"
            label="Base Price"
            value={this.state.price}
            onChange={this.handlePriceDetailsChange}
          />
        </div>
        <br />
        <Grid
          container
          spacing={2}
          justifyContent="flex-end"
          className={classes.tableElements}
        >
          <Grid item>
            <Button
              className={classes.buttonSpacing}
              disabled={!this.isSubmitEnabled()}
              variant="contained"
              color="primary"
              component="span"
              onClick={() => this.submitForm()}
            >
              Submit
            </Button>
          </Grid>
          {this.renderRedirect()}
          <Grid item>
            <Button
              className={classes.buttonSpacing}
              variant="contained"
              component="span"
              onClick={this.setRedirect}
              sx={{ backgroundColor: "red", color: "white" }}
            >
              Cancel
            </Button>
          </Grid>
        </Grid>
      </main>
    );
  }
}
export default withStyles(useStyles, { withTheme: true })(EditPackageReport);
