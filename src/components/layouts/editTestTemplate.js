import React, { Component } from "react";
import Paper from "@mui/material/Paper";
import { withStyles } from "@mui/styles";
import {
  TextField,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  Select,
  RadioGroup,
  FormControlLabel,
  Radio,
  Grid,
} from "@mui/material";
import RemoveCircleOutlineOutlinedIcon from "@mui/icons-material/RemoveCircleOutlineOutlined";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import Typography from "@mui/material/Typography";
import { Redirect } from "react-router-dom";

import MasterTestReport from "../services/MasterTestReport";

import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import { HashLoader } from "react-spinners";
import useStyles from "../constants/styles";

class EditTestReport extends Component {
  state = {
    id: null,
    reportName: "",
    inputFields: [],
    derivedFields: [],
    newInputField: null,
    redirect: false,
    price: 0,
    derivedFieldDialogOpen: false,
    inputFieldDialogOpen: false,
    operators: ["(", ")", "+", "-", "/", "*"],
    formulae: null,
    dervidedFormulaeIdx: null,
    errors: {},
    loading: false,
  };

  handleClickOpen = (idx) => {
    this.setState({ dervidedFormulaeIdx: idx, derivedFieldDialogOpen: true });
  };

  handleInputFieldDialogOpen = () => {
    let reference = {
      minValue: "",
      maxValue: "",
      gender: "NOT_SPECIFIED",
      minAge: 1,
      maxAge: 100,
      description: "",
    };
    this.setState({
      newInputField: {
        id: "",
        name: "",
        references: [reference],
        referenceUnit: "",
        type: "INPUT",
        dataType: "RANGE",
      },
      inputFieldDialogOpen: true,
    });
  };

  handleInputDialogSave = () => {
    const errors = this.validateInputFields();

    if (Object.keys(errors).length > 0) {
      // Handle errors, e.g., show a Snackbar, console log, or update the state
      // Optionally, you can set the errors to state and render them in the UI
      this.setState({ errors: errors });
      return; // Prevent saving if there are validation errors
    }

    this.setState({
      inputFields: [...this.state.inputFields, this.state.newInputField],
      dervidedFormulaeIdx: null,
      inputFieldDialogOpen: false,
    });
  };

  handleInputDialogClose = () => {
    this.setState({ inputFieldDialogOpen: false });
  };

  handleAddNewRefernce = () => {
    let reference = {
      minValue: "",
      maxValue: "",
      gender: "NOT_SPECIFIED",
      minAge: 1,
      maxAge: 100,
      description: "",
    };
    this.state.newInputField.references = [
      ...this.state.newInputField.references,
      reference,
    ];
    this.setState({ newInputField: this.state.newInputField });
  };

  isOperator(str) {
    let res = false;
    this.state.operators.map((field, idx) => {
      if (field === str) {
        res = true;
        return;
      }
    });
    return res;
  }

  findFieldName(str) {
    if (this.isOperator(str)) return str;
    var f = "";
    this.state.inputFields.map((field, idx) => {
      if (field.id === str) {
        f = field.name;
      }
    });
    return f;
  }

  addSelectedField2Formulae = () => (event) => {
    if (
      event.target.value !== null &&
      event.target.value !== undefined &&
      event.target.value !== ""
    ) {
      this.state.derivedFields[this.state.dervidedFormulaeIdx].formulaeTokens =
        [
          ...this.state.derivedFields[this.state.dervidedFormulaeIdx]
            .formulaeTokens,
          event.target.value,
        ];
      this.state.derivedFields[this.state.dervidedFormulaeIdx].formulae =
        this.state.derivedFields[this.state.dervidedFormulaeIdx].formulae +
        " " +
        this.findFieldName(event.target.value);
      this.setState({ derivedFields: this.state.derivedFields });
    }
  };

  addSelectedOperator2Formulae = () => (event) => {
    if (
      event.target.value !== null &&
      event.target.value !== undefined &&
      event.target.value !== ""
    ) {
      this.state.derivedFields[this.state.dervidedFormulaeIdx].formulaeTokens =
        [
          ...this.state.derivedFields[this.state.dervidedFormulaeIdx]
            .formulaeTokens,
          event.target.value,
        ];
      this.state.derivedFields[this.state.dervidedFormulaeIdx].formulae =
        this.state.derivedFields[this.state.dervidedFormulaeIdx].formulae +
        " " +
        event.target.value;
      this.setState({ derivedFields: this.state.derivedFields });
    }
  };

  handleClose = () => {
    this.setState({ dervidedFormulaeIdx: null, derivedFieldDialogOpen: false });
  };

  handleClear = () => {
    this.state.derivedFields[this.state.dervidedFormulaeIdx].formulaeTokens =
      [];
    this.state.derivedFields[this.state.dervidedFormulaeIdx].formulae = "";
    this.setState({ derivedField: this.state.derivedFields });
  };

  handleDerviedFieldDialogSave = () => {
    this.setState({ dervidedFormulaeIdx: null, derivedFieldDialogOpen: false });
  };

  setRedirect = () => {
    this.setState({
      redirect: true,
    });
  };
  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/lab/templates" />;
    }
  };

  addDerivedField() {
    this.setState({
      derivedFields: [
        ...this.state.derivedFields,
        {
          id: "",
          name: "",
          formulae: "",
          referenceUnit: "",
          type: "DERIVED",
          formulaeTokens: [],
        },
      ],
    });
  }

  handleInputFieldDetailsChange = (index, attr) => (event) => {
    switch (attr) {
      case "name":
        this.state.newInputField.name = event.target.value;
        break;
      case "referenceUnit":
        this.state.newInputField.referenceUnit = event.target.value;
        break;
      case "dataType":
        this.state.newInputField.dataType = event.target.value;
        break;
      case "minValue":
        this.state.newInputField.references[index].minValue =
          event.target.value.trim();
        break;
      case "maxValue":
        this.state.newInputField.references[index].maxValue =
          event.target.value.trim();
        break;
      case "description":
        this.state.newInputField.references[index].description =
          event.target.value.trim();
        break;
      case "gender":
        this.state.newInputField.references[index].gender =
          event.target.value.trim();
        break;
      case "maxAge":
        this.state.newInputField.references[index].maxAge = event.target.value;
        break;
      case "minAge":
        this.state.newInputField.references[index].minAge = event.target.value;
        break;
      default:
        break;
    }
    this.setState({ newInputField: this.state.newInputField });
  };

  handleDerivedFieldDetailsChange = (index, attr) => (event) => {
    switch (attr) {
      case "referenceUnit":
        this.state.derivedFields[index].referenceUnit = event.target.value;
        this.setState({ derivedFields: this.state.derivedFields });
        break;
      case "name":
        this.state.derivedFields[index].name = event.target.value;
        this.setState({ derivedFields: this.state.derivedFields });
        break;
      default:
        break;
    }
  };

  handlePriceDetailsChange = (event) => {
    var regex = /^\d*\.?\d*$/;
    var value = event.target.value;
    if (value === "" || regex.test(value)) {
      if (!value || value.trim() === "") {
        value = 0;
      }
    } else {
      value = 0;
    }
    if (value < 0) {
      value = 0;
    }
    this.setState({ price: parseInt(value) });
  };

  handleReportNameChange = (event) => {
    this.state.reportName = event.target.value;
    this.setState({ reportName: this.state.reportName });
  };

  isSubmitEnabled() {
    return (
      this.state.reportName &&
      this.state.inputFields &&
      this.state.inputFields.count > 0 &&
      this.state.price &&
      this.state.price > 0
    );
  }

  submitForm() {
    let data = {
      id: this.state.id,
      reportName: this.state.reportName,
      inputFields: this.state.inputFields,
      derivedFields: this.state.derivedFields,
      price: this.state.price,
      type: "LAB",
    };
    if (
      this.state.reportName.trim() != "" &&
      this.state.inputFields &&
      this.state.inputFields.length > 0 &&
      this.state.price &&
      this.state.price > 0
    ) {
      let masterTestReportService = new MasterTestReport();
      let d = this.props.id
        ? masterTestReportService.putEntity(data)
        : masterTestReportService.postEntity(data);
      this.setState({ loading: true });
      d.then(function (res) {
        let redirectUri = "/lab/template/" + res.data.id;
        window.location.href = redirectUri;
      }).catch((err) => {
        console.error("Error saving report template:", err);
        this.setState({ loading: false });
      });
    } else {
      alert("Fill all the requried fields");
      return;
    }
  }

  validateInputFields = () => {
    const { newInputField } = this.state;
    const errors = {};

    // Validate Test Name and Reference Unit
    if (!newInputField.name) {
      errors.name = "Test Name is required.";
    }

    if (!newInputField.referenceUnit) {
      errors.referenceUnit = "Reference Unit is required.";
    }

    // Validate references
    if (this.state.newInputField.dataType != "POLARITY") {
      newInputField.references.forEach((reference, idx) => {
        var regex = /^\d*\.?\d*$/;
        if (!reference.gender) {
          errors[`gender-${idx}`] = "Gender is required.";
        }
        if (
          (this.state.newInputField.dataType === "GREATER_THAN" ||
            this.state.newInputField.dataType === "RANGE") &&
          (!reference.minValue ||
            reference.minValue === "" ||
            !regex.test(reference.minValue))
        ) {
          errors[`minValue-${idx}`] = "Invalid Min Value Input.";
        }
        if (
          (this.state.newInputField.dataType === "LESS_THAN" ||
            this.state.newInputField.dataType === "RANGE") &&
          (!reference.maxValue ||
            reference.maxValue === "" ||
            !regex.test(reference.maxValue))
        ) {
          errors[`maxValue-${idx}`] = "Invalid Max Value Input.";
        }
        if (!reference.description || reference.description === "") {
          errors[`description-${idx}`] = "Description is required.";
        }
        if (
          !reference.minAge ||
          reference.minAge === "" ||
          parseInt(reference.minAge, 10) <= 0 ||
          !regex.test(reference.minAge)
        ) {
          errors[`minAge-${idx}`] = "Invalid Input.";
        }
        if (
          !reference.maxAge ||
          reference.maxAge === "" ||
          parseInt(reference.maxAge, 10) > 120 ||
          !regex.test(reference.maxAge)
        ) {
          errors[`maxAge-${idx}`] = "Invalid Input.";
        }
        if (
          reference.minAge &&
          reference.maxAge &&
          parseInt(reference.minAge, 10) > parseInt(reference.maxAge, 10)
        ) {
          errors[`maxAge-${idx}`] = "Min Age cannot be greater than Max Age.";
        }
      });
    }
    return errors;
  };

  removeInputField(index) {
    this.state.inputFields.splice(index, 1);
    this.setState({ inputFields: this.state.inputFields });
  }

  removeDerivedField(index) {
    this.state.derivedFields.splice(index, 1);
    this.setState({ derivedFields: this.state.derivedFields });
  }

  componentDidMount() {
    let masterTestReportService = new MasterTestReport();
    let id = this.props.id;
    if (id) {
      masterTestReportService
        .getMasterReport(id)
        .then((res) => {
          this.setState({
            id: id,
            reportName: res.data.reportName,
            inputFields: res.data.inputFields,
            derivedFields: res.data.derivedFields,
            price: res.data.price,
            loading: false,
          });

          this.state.derivedFields.map((field, idx) => {
            let formulae = "";
            field.formulaeTokens.map((token, tidx) => {
              formulae = formulae + " " + this.findFieldName(token);
            });
            this.state.derivedFields[idx].formulae = formulae;
            this.setState({
              derivedFields: this.state.derivedFields,
              loading: false,
            });
          });
        })
        .catch((err) => {
          console.error("Error fetching report template:", err);
          this.setState({ loading: false });
        });
    }
  }

  render() {
    const { classes } = this.props;
    return this.state.loading ? (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <HashLoader
          color={"#00ffff"}
          loading={true}
          size={50}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
      </div>
    ) : (
      <main className={classes.content}>
        {this.state.newInputField ? (
          <Dialog
            disableBackdropClick
            style={{ marginTop: "80px" }}
            fullWidth
            maxWidth="lg"
            open={this.state.inputFieldDialogOpen}
            onClose={this.handleInputDialogClose}
            aria-labelledby="responsive-dialog-title"
          >
            <DialogContent>
              <DialogContentText>
                <Table>
                  <TableBody>
                    <TableRow>
                      <TableCell align="center" className={classes.tableHeader}>
                        <b>Test Name</b>
                      </TableCell>
                      <TableCell align="center">
                        <TextField
                          id="test-name"
                          margin="dense"
                          variant="outlined"
                          error={!!this.state.errors.name}
                          helperText={this.state.errors.name}
                          onChange={this.handleInputFieldDetailsChange(
                            -1,
                            "name",
                          )}
                          value={this.state.newInputField.name}
                          className={classes.textField}
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="center" className={classes.tableHeader}>
                        <b>Reference Unit</b>
                      </TableCell>
                      <TableCell align="center">
                        <TextField
                          id="reference-unit"
                          margin="dense"
                          variant="outlined"
                          onChange={this.handleInputFieldDetailsChange(
                            -1,
                            "referenceUnit",
                          )}
                          value={this.state.newInputField.referenceUnit}
                          className={classes.textField}
                          error={!!this.state.errors.referenceUnit}
                          helperText={this.state.errors.referenceUnit}
                        />
                      </TableCell>
                    </TableRow>
                    <TableRow>
                      <TableCell align="center" className={classes.tableHeader}>
                        <b>Data Type</b>
                      </TableCell>
                      <TableCell align="center">
                        <RadioGroup
                          value={this.state.newInputField.dataType}
                          aria-label="type"
                          onChange={this.handleInputFieldDetailsChange(
                            -1,
                            "dataType",
                          )}
                        >
                          <FormControlLabel
                            value="RANGE"
                            control={<Radio />}
                            label="RANGE"
                          />
                          <FormControlLabel
                            value="GREATER_THAN"
                            control={<Radio />}
                            label="GREATER_THAN"
                          />
                          <FormControlLabel
                            value="LESS_THAN"
                            control={<Radio />}
                            label="LESS_THAN"
                          />
                          <FormControlLabel
                            value="POLARITY"
                            control={<Radio />}
                            label="POSITIVE/NEGATIVE"
                          />
                        </RadioGroup>
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
                {this.state.newInputField.dataType != "POLARITY" ? (
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell
                          align="center"
                          colSpan={2}
                          className={classes.tableHeader}
                        >
                          <b>Range</b>
                        </TableCell>
                        <TableCell
                          align="center"
                          className={classes.tableHeader}
                        >
                          <b>Gender</b>
                        </TableCell>
                        <TableCell
                          align="center"
                          colSpan={2}
                          className={classes.tableHeader}
                        >
                          <b>Age Range</b>
                        </TableCell>
                        <TableCell
                          align="center"
                          className={classes.tableHeader}
                        >
                          <b>Description</b>
                        </TableCell>
                        <TableCell align="center">
                          <AddCircleIcon
                            onClick={() => this.handleAddNewRefernce()}
                          />
                        </TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {this.state.newInputField.references.map(
                        (reference, idx) => (
                          <TableRow key={idx} align="center">
                            <TableCell colSpan={2}>
                              <div className={classes.tableElements}>
                                {this.state.newInputField.dataType !=
                                "LESS_THAN" ? (
                                  <TextField
                                    id={`minValue-${idx}`}
                                    margin="dense"
                                    label="Min Value"
                                    variant="outlined"
                                    value={reference.minValue}
                                    onChange={this.handleInputFieldDetailsChange(
                                      idx,
                                      "minValue",
                                    )}
                                    error={
                                      !!this.state.errors[`minValue-${idx}`]
                                    }
                                    helperText={
                                      this.state.errors[`minValue-${idx}`]
                                    }
                                    className={classes.ageField}
                                  />
                                ) : (
                                  <div></div>
                                )}
                                {this.state.newInputField.dataType !=
                                "GREATER_THAN" ? (
                                  <TextField
                                    id={`maxValue-${idx}`}
                                    label="Max Value"
                                    margin="dense"
                                    variant="outlined"
                                    value={reference.maxValue}
                                    onChange={this.handleInputFieldDetailsChange(
                                      idx,
                                      "maxValue",
                                    )}
                                    error={
                                      !!this.state.errors[`maxValue-${idx}`]
                                    }
                                    helperText={
                                      this.state.errors[`maxValue-${idx}`]
                                    }
                                    className={classes.ageField}
                                  />
                                ) : (
                                  <div></div>
                                )}
                              </div>
                            </TableCell>
                            <TableCell align="center">
                              <RadioGroup
                                aria-label="gender"
                                value={
                                  this.state.newInputField.references[idx]
                                    .gender
                                }
                                name={`gender-${idx}`}
                                error={!!this.state.errors[`gender-${idx}`]}
                                helperText={this.state.errors[`gender-${idx}`]}
                                onChange={this.handleInputFieldDetailsChange(
                                  idx,
                                  "gender",
                                )}
                              >
                                <FormControlLabel
                                  value="MALE"
                                  control={<Radio />}
                                  label="MALE"
                                />
                                <FormControlLabel
                                  value="FEMALE"
                                  control={<Radio />}
                                  label="FEMALE"
                                />
                                <FormControlLabel
                                  value="NOT_SPECIFIED"
                                  control={<Radio />}
                                  label="COMMON"
                                />
                              </RadioGroup>
                              {this.state.errors[`gender-${idx}`] && (
                                <Typography variant="p" color="red">
                                  {this.state.errors[`gender-${idx}`]}
                                </Typography>
                              )}
                            </TableCell>
                            <TableCell colSpan={2} align="center">
                              <div className={classes.tableElements}>
                                <TextField
                                  id={`minAge-${idx}`}
                                  margin="dense"
                                  variant="outlined"
                                  value={reference.minAge}
                                  onChange={this.handleInputFieldDetailsChange(
                                    idx,
                                    "minAge",
                                  )}
                                  className={classes.ageField}
                                  error={!!this.state.errors[`minAge-${idx}`]}
                                  helperText={
                                    this.state.errors[`minAge-${idx}`]
                                  }
                                />
                                <TextField
                                  id={`maxAge-${idx}`}
                                  margin="dense"
                                  variant="outlined"
                                  value={reference.maxAge}
                                  onChange={this.handleInputFieldDetailsChange(
                                    idx,
                                    "maxAge",
                                  )}
                                  className={classes.ageField}
                                  error={!!this.state.errors[`maxAge-${idx}`]}
                                  helperText={
                                    this.state.errors[`maxAge-${idx}`]
                                  }
                                />
                              </div>
                            </TableCell>
                            <TableCell align="center">
                              <TextField
                                id={`description-${idx}`}
                                margin="dense"
                                variant="outlined"
                                value={reference.description}
                                onChange={this.handleInputFieldDetailsChange(
                                  idx,
                                  "description",
                                )}
                                className={classes.textField}
                                error={
                                  !!this.state.errors[`description-${idx}`]
                                }
                                helperText={
                                  this.state.errors[`description-${idx}`]
                                }
                              />
                            </TableCell>
                            <TableCell align="center"></TableCell>
                          </TableRow>
                        ),
                      )}
                    </TableBody>
                  </Table>
                ) : (
                  ""
                )}
              </DialogContentText>
            </DialogContent>
            <DialogActions>
              <Button
                className={classes.buttonSpacing}
                onClick={this.handleInputDialogSave}
                variant="contained"
                color="primary"
              >
                SAVE
              </Button>
              <Button
                className={classes.buttonSpacing}
                onClick={this.handleInputDialogClose}
                variant="outlined"
                color="secondary"
              >
                CLEAR
              </Button>
            </DialogActions>
          </Dialog>
        ) : (
          ""
        )}
        <Dialog
          disableBackdropClick
          fullWidth="true"
          open={this.state.derivedFieldDialogOpen}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogContent>
            <DialogContentText>
              <Table>
                <TableRow>
                  <TableCell colSpan={2} align="center">
                    <TextField
                      id="standard-basic"
                      fullWidth="true"
                      margin="dense"
                      variant="outlined"
                      multiline="true"
                      disabled
                      rows="3"
                      value={
                        this.state.dervidedFormulaeIdx !== null
                          ? this.state.derivedFields[
                              this.state.dervidedFormulaeIdx
                            ].formulae
                          : ""
                      }
                    />
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Select
                      native
                      align="center"
                      fullWidth="true"
                      labal="Test Field"
                      variant="outlined"
                      margin="dense"
                      onChange={this.addSelectedField2Formulae()}
                    >
                      <option align="center" value="">
                        Select TestField
                      </option>
                      {this.state.inputFields.map((op, index) => {
                        if (op.id)
                          return (
                            <option key={op.id} value={op.id} align="center">
                              {op.name}
                            </option>
                          );
                      })}
                    </Select>
                  </TableCell>
                  <TableCell>
                    <Select
                      native
                      align="center"
                      fullWidth="true"
                      labal="Operator"
                      variant="outlined"
                      margin="dense"
                      onChange={this.addSelectedOperator2Formulae()}
                    >
                      <option align="center" value="">
                        Select Operator
                      </option>
                      {this.state.operators.map((op, index) => {
                        return (
                          <option key={op} value={op} align="center">
                            {op}
                          </option>
                        );
                      })}
                    </Select>
                  </TableCell>
                </TableRow>
              </Table>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button
              className={classes.buttonSpacing}
              onClick={this.handleDerviedFieldDialogSave}
              variant="outlined"
              color="primary"
              autoFocus
            >
              SAVE
            </Button>
            <Button
              className={classes.buttonSpacing}
              autoFocus
              onClick={this.handleClear}
              variant="outlined"
              color="primary"
            >
              CLEAR
            </Button>
          </DialogActions>
        </Dialog>
        <Typography
          variant="h4"
          style={{
            fontWeight: "bold",
            padding: "8px 16px",
            borderBottom: "2px solid #f0f0f0",
            color: "#3f51b5",
            textAlign: "center",
          }}
        >
          {this.props.id ? "Edit Report Template" : "New Report Template"}
        </Typography>
        <br />
        <Paper className={classes.paper}>
          <TextField
            label="Report Name"
            variant="outlined"
            fullWidth
            className={classes.textField}
            value={this.state.reportName}
            onChange={this.handleReportNameChange}
          />
          <Typography
            variant="h6"
            style={{
              fontWeight: "bold",
              padding: "8px 16px",
              borderBottom: "2px solid #f0f0f0",
              color: "#3f51b5",
              textAlign: "center",
            }}
          >
            Input Fields
          </Typography>
          <TableContainer>
            <Table className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell align="center">
                    <b>Index</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Field ID</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Field Name</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Field Type</b>
                  </TableCell>
                  <TableCell align="left">
                    <b>Reference Range</b>
                  </TableCell>
                  <TableCell align="left">
                    <b>Reference Unit</b>
                  </TableCell>
                  <TableCell align="center">
                    <AddCircleIcon
                      className={classes.addIcon}
                      onClick={this.handleInputFieldDialogOpen}
                    />
                  </TableCell>
                </TableRow>
              </TableHead>
              {this.state.inputFields.map((field, index) => {
                return (
                  <TableRow hover role="checkbox" key={index} tabIndex={index}>
                    <TableCell align="center">{index + 1}</TableCell>
                    <TableCell align="center">
                      {this.state.inputFields[index].code}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.inputFields[index].name}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.inputFields[index].dataType}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.inputFields[index].references.map(
                        (ref, idx) => {
                          var gender = ref.gender;
                          if (ref.gender === "NOT_SPECIFIED") {
                            gender = "";
                          }
                          return (
                            <div key={idx} align="left">
                              {this.state.inputFields[index].dataType ==
                              "RANGE" ? (
                                <div>{`${ref.minValue} - ${ref.maxValue}`}</div>
                              ) : this.state.inputFields[index].dataType ==
                                "GREATER_THAN" ? (
                                <div>{`> ${ref.minValue}`}</div>
                              ) : this.state.inputFields[index].dataType ==
                                "LESS_THAN" ? (
                                <div>{`< ${ref.maxValue}`}</div>
                              ) : (
                                <div>{`POSITIVE/NEGATIVE`}</div>
                              )}
                              {this.state.inputFields[index].dataType !=
                                "POLARITY" &&
                              ref.minAge &&
                              ref.maxAge ? (
                                <div>{`${gender} (Age : ${ref.minAge} - ${ref.maxAge})`}</div>
                              ) : (
                                <div></div>
                              )}
                            </div>
                          );
                        },
                      )}
                    </TableCell>
                    <TableCell align="left">
                      {this.state.inputFields[index].referenceUnit}
                    </TableCell>
                    <TableCell align="center">
                      <RemoveCircleOutlineOutlinedIcon
                        onClick={() => this.removeInputField(index)}
                      />
                    </TableCell>
                  </TableRow>
                );
              })}
            </Table>
          </TableContainer>
        </Paper>
        <Paper className={classes.paper}>
          <Typography
            variant="h6"
            style={{
              fontWeight: "bold",
              padding: "8px 16px",
              borderBottom: "2px solid #f0f0f0",
              color: "#3f51b5",
              textAlign: "center",
            }}
          >
            Derived Fields
          </Typography>
          {this.props.id &&
          this.state.inputFields &&
          this.state.inputFields.length >= 2 ? (
            <TableContainer className={classes.root}>
              <Table
                stickyHeader
                aria-label="sticky table"
                className={classes.table}
              >
                <TableHead>
                  <TableCell align="center">
                    <b>Index</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Field Id</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Field Name</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Reference Formulae</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Reference Unit</b>
                  </TableCell>
                  <TableCell align="center">
                    <AddCircleIcon onClick={(e) => this.addDerivedField(e)} />
                  </TableCell>
                </TableHead>
                {this.state.derivedFields.map((field, idx) => {
                  return (
                    <TableRow hover role="checkbox" key={idx} tabIndex={idx}>
                      <TableCell align="center">{idx + 1}</TableCell>
                      <TableCell>
                        {this.state.derivedFields[idx].code}
                      </TableCell>
                      <TableCell align="center">
                        <TextField
                          required
                          label="Name"
                          align="center"
                          value={this.state.derivedFields[idx].name}
                          onChange={this.handleDerivedFieldDetailsChange(
                            idx,
                            "name",
                          )}
                        />
                      </TableCell>
                      <TableCell align="center">
                        <TextField
                          label="Reference Formulae"
                          align="center"
                          value={this.state.derivedFields[idx].formulae}
                          onClick={() => this.handleClickOpen(idx)}
                        />
                      </TableCell>

                      <TableCell align="center">
                        <TextField
                          required
                          label="Reference Units"
                          align="center"
                          value={this.state.derivedFields[idx].referenceUnit}
                          onChange={this.handleDerivedFieldDetailsChange(
                            idx,
                            "referenceUnit",
                          )}
                        />
                      </TableCell>
                      <TableCell align="center">
                        <RemoveCircleOutlineOutlinedIcon
                          onClick={() => this.removeDerivedField(idx)}
                        />
                      </TableCell>
                    </TableRow>
                  );
                })}
              </Table>
            </TableContainer>
          ) : null}
          <br />
          <div align="center">
            * Since derived fieds are based on input fields, Please save new
            template with input fields, further on edit template derived fields
            can be added
          </div>
        </Paper>
        <br />
        <div align="right">
          <TextField
            required
            margin="dense"
            variant="outlined"
            label="Base Price"
            value={this.state.price}
            onChange={this.handlePriceDetailsChange}
          />
        </div>
        <br />
        <Grid
          container
          spacing={2}
          justifyContent="flex-end"
          className={classes.tableElements}
        >
          <Grid item>
            <Button
              className={classes.buttonSpacing}
              disabled={this.isSubmitEnabled()}
              variant="contained"
              color="primary"
              component="span"
              onClick={() => this.submitForm()}
            >
              Submit
            </Button>
          </Grid>
          {this.renderRedirect()}
          <Grid item>
            <Button
              className={classes.buttonSpacing}
              variant="contained"
              component="span"
              onClick={this.setRedirect}
              sx={{ backgroundColor: "red", color: "white" }}
            >
              Cancel
            </Button>
          </Grid>
        </Grid>
      </main>
    );
  }
}
export default withStyles(useStyles, { withTheme: true })(EditTestReport);
