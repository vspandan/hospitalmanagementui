import React, { Component } from "react";
import Paper from "@mui/material/Paper";
import { withStyles } from "@mui/styles";
import {
  TextField,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableBody,
  TableCell,
  Button,
  Grid,
} from "@mui/material";
import Typography from "@mui/material/Typography";
import useStyles from "../constants/styles";
import LabTests from "../services/LabTests";
import { HashLoader } from "react-spinners";
import { PdfDialog } from "./pdfViewer";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";

class LabTestReport extends Component {
  invokeApi = (stepIndex) => {
    try {
      this.setState({ loading: true });
      console.log(`Invoking API for step: ${this.state.steps[stepIndex]}`);
      //console.log("API Response:", response.data);
    } catch (error) {
      console.error("API Error:", error);
    } finally {
      this.setState({ loading: false });
    }
  };

  handleNext = () => {
    if (this.state.activeStep < this.state.steps.length) {
      const nextStep = this.state.activeStep + 1;
      this.invokeApi(nextStep);
      this.setState({ activeStep: nextStep });
    }
  };

  handleBack = () => {
    if (this.state.activeStep > 0) {
      const prevStep = this.state.activeStep - 1;
      this.invokeApi(prevStep);
      this.setState({ activeStep: prevStep });
    }
  };

  state = {
    id: null,
    testFieldDetails: [],
    redirect: false,
    testCode: null,
    testName: null,
    billId: null,
    version: null,
    testStatus: null,
    patientObj: null,
    loading: true,
    open: false,
    isEditable: false,
    isEditable2: false,
    activeStep: 0,
    steps: [
      "REQUESTED",
      "SAMPLE_COLLECTION",
      "TEST_IN_PROGRESS",
      "UPDATE_REPORT",
      "VERIFICATION_PENDING",
      "COMPLETED",
    ],
    buttons: [
      "Start",
      "Sample Collected",
      "Test Complete",
      "Update Report",
      "Approve",
      "Submit",
    ],
  };

  closeForm = () => {
    window.location.href = "/tests";
  };

  handleInputFieldDetailsChange = (index, attr) => (event) => {
    switch (attr) {
      case "value":
        this.state.testFieldDetails[index].value = event.target.value;
        this.setState({ testFieldDetails: this.state.testFieldDetails });
        break;
      default:
        break;
    }
  };

  handleOpen = () => {
    this.setState({ open: true, header: false });
  };

  handleOpenWithHeader = () => {
    this.setState({ open: true, header: true });
  };

  submitForm() {
    const testValues = {};
    Object.keys(this.state.testFieldDetails).map((index) => {
      testValues[index] = this.state.testFieldDetails[index].value;
    });
    let data = {
      id: this.state.id,
      version: this.state.version,
      testValues: testValues,
      testStatus: "COMPLETED",
    };
    let labtTestService = new LabTests();
    labtTestService
      .putEntity(data)
      .then(function (res) {
        let redirectUri = "/test/" + res.data.id;
        window.location.href = redirectUri;
      })
      .catch((error) => {
        console.error(`Error updating test report`, error);
        this.setState({ loading: false });
      });
  }

  saveForm() {
    const testValues = {};
    var valid = true;
    Object.keys(this.state.testFieldDetails).map((index) => {
      testValues[index] = this.state.testFieldDetails[index].value;
      valid = valid && this.state.testFieldDetails[index].value !== "";
    });
    if (valid || this.state.activeStep < this.state.steps.length) {
      let data = {
        id: this.state.id,
        version: this.state.version,
        testValues: testValues,
        testStatus: this.state.steps[this.state.activeStep],
      };
      let labtTestService = new LabTests();
      this.setState({ loading: true });
      labtTestService
        .putEntity(data)
        .then((res) => {
          let redirectUri = "/test/" + res.data.id;
          window.location.href = redirectUri;
        })
        .catch((error) => {
          console.error(`Error updating test report`, error);
          this.setState({ loading: false });
        });
    } else {
      alert("Please Provide Input Values For All Tests");
    }
  }

  handleSave() {
    let data = {
      id: this.state.id,
      createdAt: new Date(this.state.createdAt).getTime(),
      modifiedAt: new Date(this.state.modifiedAt).getTime(),
    };
    let labtTestService = new LabTests();
    this.setState({
      loading: true,
      isEditable: !this.state.isEditable,
      isEditable2: !this.state.isEditable2,
    });
    labtTestService
      .putEntityDates(data)
      .then(function (res) {
        let redirectUri = "/test/" + res.data.id;
        window.location.href = redirectUri;
      })
      .catch((error) => {
        console.error(`Error updating test report`, error);
        this.setState({ loading: false });
      });
  }

  componentDidMount() {
    this.setState({ loading: true });
    let labtTestService = new LabTests();
    let id = this.props.id;
    labtTestService
      .getEntity(id)
      .then((res) => {
        this.setState({
          testFieldDetails: res.data.testFieldDetails,
          testCode: res.data.testCode,
          testName: res.data.testName,
          billId: res.data.billId,
          id: res.data.id,
          billStatus: res.data.billStatus,
          testStatus: res.data.status,
          patientObj: res.data.patient,
          createdAt: res.data.createdAt,
          modifiedAt: res.data.modifiedAt,
          loading: false,
          activeStep:
            this.state.steps.indexOf(res.data.status) <
            this.state.steps.length - 1
              ? this.state.steps.indexOf(res.data.status) + 1
              : this.state.steps.length,
        });
      })
      .catch((error) => {
        console.error(`Error fetching test report`, error);
        this.setState({ loading: false });
      });
  }
  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    return this.state.loading ? (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <HashLoader
          color={"#00ffff"}
          loading={true}
          size={50}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
      </div>
    ) : (
      <main className={classes.content}>
        <PdfDialog
          open={this.state.open}
          header={this.state.header}
          onClose={this.handleClose}
          type="report"
          id={this.state.id}
        />

        <Paper className={classes.paper} sx={{ padding: 2 }}>
          <Grid container spacing={2}>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Patient ID</Typography>
              <Typography>{this.state.patientObj.code}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Name</Typography>
              <Typography>
                {this.state.patientObj.firstName}{" "}
                {this.state.patientObj.surname || ""}
              </Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Date of Birth</Typography>
              <Typography>{this.state.patientObj.dob}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Gender</Typography>
              <Typography>{this.state.patientObj.gender}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Phone</Typography>
              <Typography>{this.state.patientObj.phone}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Aadhaar</Typography>
              <Typography>
                {this.state.patientObj.maskedAadhaar || "N/A"}
              </Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Marital Status</Typography>
              <Typography>{this.state.patientObj.maritialStatus}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Blood Group</Typography>
              <Typography>{this.state.patientObj.bloodGroup}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Test Id</Typography>
              <Typography>{this.state.testCode}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Test Name</Typography>
              <Typography>{this.state.testName}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Bill Id</Typography>
              <Typography>{this.state.billId}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Bill Status</Typography>
              <Typography>{this.state.billStatus}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Test Status</Typography>
              <Typography>{this.state.testStatus}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Doctor Name</Typography>
              <Typography>{this.state.doctorName || "Self"}</Typography>
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Requested Time</Typography>
              {this.state.isEditable ? (
                <TextField
                  onBlur={() => {
                    this.handleSave();
                  }} // Exits edit mode on blur
                  type="datetime-local"
                  value={this.state.createdAt}
                  onChange={(e) => {
                    if (!isNaN(new Date(e.target.value).getTime()))
                      this.setState({ createdAt: e.target.value });
                  }}
                  variant="outlined"
                  size="small"
                  onKeyDown={(e) => e.preventDefault()}
                  inputProps={{
                    min: "2020-01-01T00:00", // Replace this with your desired minimum date
                    max: new Date().toISOString().slice(0, 16), // Current date and time
                  }}
                />
              ) : (
                <Typography
                  onClick={() => {
                    this.setState({ isEditable: !this.state.isEditable });
                  }}
                  style={{ cursor: "pointer" }}
                >
                  {this.state.createdAt}
                </Typography>
              )}
            </Grid>
            <Grid
              item
              spacing={2}
              alignItems="center"
              justifyContent="center"
              xs={3}
            >
              <Typography fontWeight="bold">Report Generated Time</Typography>
              {this.state.isEditable2 ? (
                <TextField
                  onBlur={() => {
                    this.handleSave();
                  }} // Exits edit mode on blur
                  value={this.state.modifiedAt}
                  onChange={(e) => {
                    if (!isNaN(new Date(e.target.value).getTime()))
                      this.setState({ modifiedAt: e.target.value });
                  }}
                  type="datetime-local"
                  variant="outlined"
                  size="small"
                  onKeyDown={(e) => e.preventDefault()}
                  inputProps={{
                    min: "2020-01-01T00:00", // Replace this with your desired minimum date
                    max: new Date().toISOString().slice(0, 16), // Current date and time
                  }}
                />
              ) : (
                <Typography
                  onClick={() => {
                    this.setState({ isEditable2: !this.state.isEditable2 });
                  }}
                  style={{ cursor: "pointer" }}
                >
                  {this.state.modifiedAt}
                </Typography>
              )}
            </Grid>
          </Grid>
          <Typography
            variant="h6"
            style={{
              fontWeight: "bold",
              padding: "8px 16px",
              borderBottom: "2px solid #f0f0f0",
              color: "#3f51b5",
              textAlign: "center",
            }}
          >
            <div style={{ padding: "20px", margin: "0 auto" }}>
              <Stepper activeStep={this.state.activeStep} alternativeLabel>
                {this.state.steps.map((label, index) => (
                  <Step key={index}>
                    <StepLabel>{label}</StepLabel>
                  </Step>
                ))}
              </Stepper>
            </div>
            <b>
              {this.state.testStatus === "COMPLETED"
                ? `Final Report - ${this.state.testName}`
                : "Report Data Form"}
            </b>
          </Typography>
          <TableContainer>
            <Table
              stickyHeader
              aria-label="sticky table"
              className={classes.table}
              size="small"
            >
              <TableHead>
                <TableCell align="center">
                  <b>Field Id</b>
                </TableCell>
                <TableCell align="center">
                  <b>Field Name</b>
                </TableCell>
                <TableCell align="center">
                  <b>Field Type</b>
                </TableCell>
                <TableCell align="center">
                  <b>Data Type</b>
                </TableCell>
                <TableCell align="center">
                  <b>Input Value</b>
                </TableCell>
                <TableCell align="center">
                  <b>Gender</b>
                </TableCell>
                <TableCell align="center">
                  <b>Normal Values</b>
                </TableCell>
                <TableCell align="center">
                  <b>Unit</b>
                </TableCell>
              </TableHead>
              {Object.keys(this.state.testFieldDetails).map((index) => {
                return (
                  <TableRow hover role="checkbox" key={index} tabIndex={index}>
                    <TableCell align="center">
                      {this.state.testFieldDetails[index].code}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.testFieldDetails[index].name}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.testFieldDetails[index].labFieldType}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.testFieldDetails[index].dataType}
                    </TableCell>
                    <TableCell align="center">
                      {this.state.activeStep >
                        this.state.steps.indexOf("TEST_IN_PROGRESS") &&
                      this.state.activeStep <=
                        this.state.steps.indexOf("COMPLETED") - 1 &&
                      this.state.testFieldDetails[index].labFieldType ===
                        "INPUT" ? (
                        <TextField
                          required
                          type="text"
                          value={this.state.testFieldDetails[index].value || ""}
                          onChange={this.handleInputFieldDetailsChange(
                            index,
                            "value",
                          )}
                        />
                      ) : (
                        this.state.testFieldDetails[index].value
                      )}
                    </TableCell>
                    <TableCell align="center">
                      {Object.keys(
                        this.state.testFieldDetails[index].references,
                      ).map((ref) => {
                        return (
                          <div key={ref}>
                            {this.state.testFieldDetails[index].references[ref]
                              .gender
                              ? this.state.testFieldDetails[index].references[
                                  ref
                                ].gender
                              : "COMMON"}
                          </div>
                        );
                      })}
                    </TableCell>
                    <TableCell align="center">
                      {Object.keys(
                        this.state.testFieldDetails[index].references,
                      ).map((ref) => {
                        return (
                          <div key={ref}>
                            {
                              this.state.testFieldDetails[index].references[ref]
                                .minValue
                            }{" "}
                            -{" "}
                            {
                              this.state.testFieldDetails[index].references[ref]
                                .maxValue
                            }
                          </div>
                        );
                      })}
                    </TableCell>
                    <TableCell align="center">
                      {Object.keys(
                        this.state.testFieldDetails[index].references,
                      ).map((ref) => {
                        return (
                          <div key={ref}>
                            {this.state.testFieldDetails[index].referenceUnit}
                          </div>
                        );
                      })}
                    </TableCell>
                  </TableRow>
                );
              })}
            </Table>
          </TableContainer>
        </Paper>
        <br />
        {this.state.testStatus === "COMPLETED" ? (
          ""
        ) : (
          <div>
            * Derived fields will be evaluated based on input fields, will be
            part of report after submission
          </div>
        )}
        <Grid container spacing={2} justifyContent="flex-end">
          {this.state.testStatus === "COMPLETED" ||
          this.state.testStatus === "CANCELLED" ? (
            <Grid container spacing={2} justifyContent="flex-end">
              <Grid item>
                {this.state.testStatus !== "CANCELLED" ? (
                  <Button
                    variant="contained"
                    color="primary"
                    component="span"
                    className={classes.buttonSpacing}
                    onClick={() => this.handleOpen()}
                  >
                    Reports Without Header
                  </Button>
                ) : (
                  ""
                )}{" "}
              </Grid>
              <Grid item>
                {this.state.testStatus !== "CANCELLED" ? (
                  <Button
                    variant="contained"
                    color="primary"
                    component="span"
                    className={classes.buttonSpacing}
                    onClick={() => this.handleOpenWithHeader()}
                  >
                    Reports
                  </Button>
                ) : (
                  ""
                )}{" "}
              </Grid>
              <Grid item>
                <Button
                  className={classes.buttonSpacing}
                  variant="contained"
                  component="span"
                  onClick={this.closeForm}
                  sx={{ backgroundColor: "black", color: "white" }}
                >
                  Close
                </Button>
              </Grid>
            </Grid>
          ) : (
            <>
              <Grid item>
                <Button
                  className={classes.buttonSpacing}
                  variant="contained"
                  color="primary"
                  component="span"
                  disabled={this.state.steps.length == this.state.activeStep}
                  onClick={() => this.saveForm()}
                >
                  {this.state.buttons[this.state.activeStep]}
                </Button>
              </Grid>
              <Grid item>
                <Button
                  className={classes.buttonSpacing}
                  variant="contained"
                  component="span"
                  onClick={this.closeForm}
                  sx={{ backgroundColor: "black", color: "white" }}
                >
                  Close
                </Button>
              </Grid>
            </>
          )}
        </Grid>
      </main>
    );
  }
}
export default withStyles(useStyles, { withTheme: true })(LabTestReport);
