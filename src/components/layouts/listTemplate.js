import React, { Component } from "react";
import Paper from "@mui/material/Paper";
import { withStyles } from "@mui/styles";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Button,
  Typography,
  Grid,
  TextField,
} from "@mui/material";
import { Redirect } from "react-router-dom";
import useStyles from "../constants/styles";
import MasterTestReport from "../services/MasterTestReport";
import { HashLoader } from "react-spinners";

class ListReports extends Component {
  state = {
    reportLists: [],
    viewTemplateRedirect: false,
    selectedTemplateId: null,
    redirect: false,
    loading: true,
    pageSize: 20,
    currentPage: 0,
    totalElements: 0,
    totalPages: 0,
    filterCond: null,
  };

  componentDidMount() {
    this.fetchMasterTestDetails();
  }

  handlePageChange(page) {
    this.fetchMasterTestDetails(page); // Fetch data for the new page
  }

  filterMasterTestDetails = (event) => {
    this.setState({ filterCond: event.target.value }, () => {
      this.fetchMasterTestDetails();
    });
  };

  fetchMasterTestDetails(page = 0, size = this.state.pageSize) {
    const masterTestReportService = new MasterTestReport();
    let params = {
      page: page, // Current page number
      size: size,
      type: "LAB", // Number of records per page
      input: this.state.filterCond,
    };
    masterTestReportService
      .getMasterReportsList(params)
      .then((res) =>
        this.setState({
          loading: false,
          reportLists: res.data.content,
          totalElements: res.data.totalElements, // Store total number of elements
          totalPages: res.data.totalPages, // Store total number of pages
          currentPage: res.data.number,
        }),
      )
      .catch((error) => {
        console.error(`Error fetching report templates: `, error);
        this.setState({ loading: false });
      });
  }

  setViewTemplateRedirect = (id) => () => {
    this.setState({
      viewTemplateRedirect: true,
      selectedTemplateId: id,
    });
  };

  renderViewTemplateRedirect = () => {
    if (this.state.viewTemplateRedirect) {
      const redirectUrl = `/lab/template/${this.state.selectedTemplateId}`;
      return <Redirect to={redirectUrl} />;
    }
  };

  isAdmin = () => {
    return Storage.get("isAdmin") === true;
  };
  removeField = (index, id) => {
    const { reportLists } = this.state;
    reportLists.splice(index, 1);
    this.setState({ reportLists, loading: true });

    const masterTestReportService = new MasterTestReport();
    masterTestReportService
      .deleteEntity(id)
      .then(() => this.setState({ redirect: true, loading: false }))
      .catch((error) => {
        console.error(`Error deleting report templates: `, error);
        this.setState({ loading: false });
      });
  };

  renderPaginationControls() {
    const { totalPages, currentPage } = this.state;

    return (
      <div style={{ marginTop: "20px", textAlign: "center" }}>
        <Button
          disabled={currentPage === 0}
          onClick={() => this.handlePageChange(currentPage - 1)}
        >
          Previous
        </Button>
        <span style={{ margin: "0 10px" }}>
          Page {totalPages === 0 ? 0 : currentPage + 1} of {totalPages}
        </span>
        <Button
          disabled={currentPage + 1 === totalPages}
          onClick={() => this.handlePageChange(currentPage + 1)}
        >
          Next
        </Button>
      </div>
    );
  }

  render() {
    const { classes } = this.props;
    const { reportLists } = this.state;

    return this.state.loading ? (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <HashLoader
          color={"#00ffff"}
          loading={true}
          size={50}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
      </div>
    ) : (
      <main className={classes.content}>
        <Typography
          variant="h4"
          style={{
            fontWeight: "bold",
            padding: "8px 16px",
            borderBottom: "2px solid #f0f0f0",
            color: "#3f51b5",
            textAlign: "center",
          }}
        >
          Lab Report Templates
        </Typography>

        <Grid
          container
          spacing={2}
          style={{
            alignItems: "center",
            gap: "10px",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <Grid item>
            <TextField
              label="Search Lab Template"
              variant="outlined"
              margin="dense"
              onChange={this.filterMasterTestDetails}
            />
          </Grid>
          <Grid item>
            <Button
              label="Search Template"
              variant="outlined"
              sx={{
                backgroundColor: "#0056b3",
                color: "white",
                "&:hover": { backgroundColor: "#A056b3" },
              }}
              margin="dense"
              onClick={() => {
                window.location.href = "/lab/template";
              }}
            >
              {" "}
              Add Report Template{" "}
            </Button>
          </Grid>
        </Grid>

        <Paper className={classes.paper}>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">
                    <b>Index</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Id</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Type</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Name</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Created By</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Created At</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Actions</b>
                  </TableCell>
                </TableRow>
              </TableHead>
              <tbody>
                {reportLists.length > 0 ? (
                  reportLists.map((field, index) => (
                    <TableRow hover role="checkbox" key={index}>
                      <TableCell align="center">{index + 1}</TableCell>
                      {this.renderViewTemplateRedirect()}
                      <TableCell
                        align="center"
                        onClick={this.setViewTemplateRedirect(field.id)}
                      >
                        {field.code}
                      </TableCell>
                      <TableCell align="center">{field.type}</TableCell>
                      {this.renderViewTemplateRedirect()}
                      <TableCell
                        align="center"
                        onClick={this.setViewTemplateRedirect(field.id)}
                      >
                        {field.reportName}
                      </TableCell>
                      <TableCell align="center">{field.createdBy}</TableCell>
                      <TableCell align="center">{field.createdAt}</TableCell>
                      <TableCell align="center">
                        <Grid
                          container
                          spacing={2}
                          className={classes.tableElements}
                          justifyContent="center"
                        >
                          {this.renderViewTemplateRedirect()}

                          <Grid item>
                            <Button
                              className={classes.buttonSpacing}
                              variant="contained"
                              color="primary" // You can keep this or use sx for a custom color
                              onClick={this.setViewTemplateRedirect(field.id)}
                            >
                              View
                            </Button>
                          </Grid>

                          <Grid item>
                            <Button
                              className={classes.buttonSpacing}
                              variant="contained"
                              color="secondary" // Changed to secondary for distinction; adjust as needed
                              disabled={!this.isAdmin}
                              onClick={() => this.removeField(index, field.id)}
                            >
                              Delete
                            </Button>
                          </Grid>
                        </Grid>
                      </TableCell>
                    </TableRow>
                  ))
                ) : (
                  <TableRow>
                    <TableCell colSpan={6} align="center">
                      No Reports Available
                    </TableCell>
                  </TableRow>
                )}
              </tbody>
            </Table>
          </TableContainer>
          {/* Pagination Controls */}
          {this.renderPaginationControls()}
        </Paper>
      </main>
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(ListReports);
