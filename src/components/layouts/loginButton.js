import React, { useEffect, useState } from "react";
import { GoogleOAuthProvider, GoogleLogin } from "@react-oauth/google";
import { Button } from "@mui/material";

const LoginButton = ({ onTokenReceived }) => {
  const [tokenClient, setTokenClient] = useState(null);

  useEffect(() => {
    const initializeTokenClient = () => {
      if (
        window.google &&
        window.google.accounts &&
        window.google.accounts.oauth2
      ) {
        // Initialize token client with desired scope
        const client = window.google.accounts.oauth2.initTokenClient({
          client_id:
            "852829753982-8dsjo59mbumqqmp4pnjc41orc43084nb.apps.googleusercontent.com",
          scope:
            "https://www.googleapis.com/auth/drive.file https://www.googleapis.com/auth/drive.metadata.readonly",
          callback: (tokenResponse) => {
            if (tokenResponse && tokenResponse.access_token) {
              onTokenReceived(tokenResponse.access_token); // Return the token
            }
          },
        });
        setTokenClient(client);
      }
    };

    // Ensure the google.accounts API is ready
    if (window.google && window.google.accounts) {
      initializeTokenClient();
    } else {
      // If not, wait until it loads
      window.addEventListener("load", initializeTokenClient);
    }

    // Cleanup event listener
    return () => {
      window.removeEventListener("load", initializeTokenClient);
    };
  }, [onTokenReceived]);

  const handleLogin = () => {
    if (tokenClient) {
      // Trigger the login flow and get the token
      tokenClient.requestAccessToken();
    } else {
      console.error("Google API client not initialized.");
    }
  };

  return (
    <GoogleOAuthProvider clientId="852829753982-8dsjo59mbumqqmp4pnjc41orc43084nb.apps.googleusercontent.com">
      <div>
        <Button
          variant="contained"
          color="primary"
          sx={{ backgroundColor: "black", color: "white" }}
          onClick={handleLogin}
        >
          Login with Google
        </Button>
      </div>
    </GoogleOAuthProvider>
  );
};

export default LoginButton;
