import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import {
  TextField,
  MenuItem,
  Button,
  Typography,
  FormControl,
  InputLabel,
  NativeSelect,
  Grid,
  Paper,
} from "@mui/material";
import Identity from "../services/Identity";
import Autocomplete from "@mui/material/Autocomplete";
import DateTimePicker from "@mui/lab/DateTimePicker";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import Appointment from "../services/Appointment";

const useStyles = (theme) => ({
  container: {
    padding: "20px",
    margin: "20px auto",
    maxWidth: "1000px",
    background: "#fff",
    borderRadius: "8px",
    boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
  },
  formField: {
    margin: "10px 0",
  },
  button: {
    marginTop: "20px",
  },
});

class NewAppointment extends Component {
  state = {
    loading: false,
    id: null,
    patientAutoSearchData: [],
    patientObj: {
      id: null,
      firstName: null,
      lastName: null,
      phone: null,
      dob: null,
    },
    doctorObj: null,
    referrerObj: null,
    doctorAutoSearchData: [],
    referrerAutoSearchData: [],
    editMode: true,
    errors: {},
    loading: false,
    appointmentSource: [
      "WALK_IN",
      "CONSULTATION",
      "SCHEDULED",
      "FOLLOW_UP",
      "DIAGNOSTIC",
      "THERAPY",
      "IMMUNIZATION",
      "EMERGENCY",
      "CHECK_UP",
      "SPECIALIST_REFERRAL",
      "SECOND_OPINION",
    ],
    appointmentDetails: {
      appointmentType: "CONSULTATION",
      appointmentDate: new Date().toISOString().split("T")[0],
      clinicalInformation: "",
      doctorId: "",
    },
    errors: {
      firstName: "",
      lastName: "",
      phone: "",
      dob: "",
      gender: "",
      appointmentType: "",
      appointmentDate: "",
      clinicalInformation: "",
      doctorId: "",
    },
  };

  patientAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .patientAutoSearch(event.target.value)
      .then((res) =>
        this.setState({ patientAutoSearchData: res.data, loading: false }),
      )
      .catch((error) => {
        console.error("Error searching patient: ", error);
        this.setState({ loading: false });
      });
  };

  referrerAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .referrerAutoSearch(event.target.value)
      .then((res) =>
        this.setState({ referrerAutoSearchData: res.data, loading: false }),
      )
      .catch((error) => {
        console.error("Error searching patient: ", error);
        this.setState({ loading: false });
      });
  };

  doctorAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .doctorAutoSearch(event.target.value)
      .then((res) =>
        this.setState({ doctorAutoSearchData: res.data, loading: false }),
      )
      .catch((error) => {
        console.error("Error searching doctor: ", error);
        this.setState({ loading: false });
      });
  };

  isISODateWithoutTime(dateStr) {
    const regex = /^\d{4}-\d{2}-\d{2}$/;
    return regex.test(dateStr);
  }

  formatToISO = (dateStr) => {
    if (this.isISODateWithoutTime(dateStr)) return dateStr;
    const [day, month, year] = dateStr.split("/");
    console.log(day);
    console.log(month);
    console.log(year);
    const date = new Date(Date.UTC(year, month - 1, day)); // Create a UTC date without timezone shift

    // Format to ISO (YYYY-MM-DD)
    return date.toISOString().split("T")[0];
  };

  handleInputChange = (field, value, section = "patientObj") => {
    // Update the field value in the corresponding section
    this.setState(
      (prevState) => ({
        [section]: {
          ...prevState[section],
          [field]: value,
        },
      }),
      //() => this.validateField(field, value, section)
    );
  };

  validateForm = () => {
    const { patientObj, appointmentDetails, doctorObj, referrerObj } =
      this.state;
    alert(JSON.stringify(appointmentDetails));
    let errors = {};
    let isValid = true;

    // Validation rules for each section
    const validationRules = {
      patientObj: {
        firstName: (value) =>
          !value || value.trim() === "" ? "First name is required" : "",
        lastName: (value) =>
          !value || value.trim() === "" ? "Last name is required" : "",
        phone: (value) =>
          !value || /^[0-9]{10}$/.test(value)
            ? ""
            : "Phone number must be 10 digits",
        dob: (value) => (!value || value ? "" : "Date of Birth is required"),
      },
      appointmentDetails: {
        appointmentType: (value) =>
          value.trim() === "" ? "Appointment type is required" : "",
        appointmentDate: (value) =>
          value ? "" : "Appointment date is required",
        clinicalInformation: (value) =>
          value == null || value.trim() === ""
            ? "Clinical information is required"
            : "",
      },
    };

    // Validate patientObj fields
    Object.keys(patientObj || {}).forEach((field) => {
      const error = validationRules.patientObj[field]
        ? validationRules.patientObj[field](patientObj[field])
        : "";
      if (error) {
        errors[field] = error;
        isValid = false;
      }
    });

    // Validate appointmentDetails fields
    Object.keys(appointmentDetails || {}).forEach((field) => {
      const error = validationRules.appointmentDetails[field]
        ? validationRules.appointmentDetails[field](appointmentDetails[field])
        : "";
      if (error) {
        errors[field] = error;
        isValid = false;
      }
    });

    // Additional validations for doctor and referrer
    if (!doctorObj || !doctorObj.id) {
      errors.doctorId = "Doctor must be selected";
      isValid = false;
    }

    if (!patientObj) {
      errors.patientInfo = "Patient Information must be filled";
      isValid = false;
    }

    // Set errors in state
    this.setState({ errors });
    console.log(JSON.stringify(errors));
    // Return whether the form is valid
    return isValid;
  };

  handleSubmit = () => {
    const { patientObj, appointmentDetails, doctorObj, referrerObj } =
      this.state;
    let errors = {};
    let isValid = this.validateForm();

    this.setState({ errors }, () => {
      if (isValid) {
        let appointmentData = {
          patientDetails: {
            id: patientObj?.id ?? null,
            firstName: patientObj?.firstName ?? "",
            lastName: patientObj?.lastName ?? "",
            phone: patientObj?.phone ?? "",
            dob: patientObj?.dob
              ? new Date(patientObj.dob).getTime()
              : new Date().getTime(),
            gender: patientObj?.gender ?? "MALE",
          },
          appointmentDetails: {
            doctorId: doctorObj.id, // Ensure doctor is selected
            referrerId: referrerObj ? referrerObj.id : null,
            appointmentType: appointmentDetails?.appointmentType ?? "",
            appointmentDate: appointmentDetails?.appointmentDate
              ? new Date(appointmentDetails.appointmentDate).getTime()
              : new Date().getTime(),
            clinicalInformation: appointmentDetails?.clinicalInformation ?? "",
          },
        };

        let appointmentService = new Appointment();
        this.setState({ editMode: true });
        appointmentService
          .scheduleAppointment(appointmentData)
          .then((res) => {
            if (res.data) {
              alert("Appointment booked successfully!");
              window.location.href = "/Billing/" + res.data;
            }
          })
          .catch(() => {
            alert("Appointment Schedule Failed");
          });
      } else {
        alert("Please fill in all required fields correctly.");
        this.setState({ editMode: true });
      }
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <Paper className={classes.container}>
        <Typography variant="h5" gutterBottom>
          Book an Appointment
        </Typography>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            {this.state.id === null ? (
              <Autocomplete
                options={this.state.patientAutoSearchData}
                getOptionLabel={(option) =>
                  `${option.code} - ${option.firstName} - ${option.lastName} - ${option.phone}`
                }
                style={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Patient"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={this.patientAutoSearch}
                  />
                )}
                onChange={(_event, option) => {
                  if (option)
                    this.setState({
                      patientObj: option,
                      editMode: option === null,
                    });
                  else {
                    this.setState({
                      patientObj: {
                        id: null,
                        firstName: null,
                        lastName: null,
                        phone: null,
                        dob: null,
                      },
                      editMode: true,
                    });
                  }
                }}
                onClear={() => {
                  this.setState({
                    patientObj: {
                      id: null,
                      firstName: null,
                      lastName: null,
                      phone: null,
                      dob: null,
                    },
                    editMode: true,
                  });
                }}
              />
            ) : null}
          </Grid>
          <Grid container spacing={3} padding={"15px"}>
            {/* Row 1 */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="First Name"
                variant="outlined"
                disabled={!this.state.editMode}
                required
                fullWidth
                value={
                  this.state.patientObj != null &&
                  this.state.patientObj.firstName
                    ? this.state.patientObj.firstName
                    : ""
                }
                onChange={(e) =>
                  this.handleInputChange("firstName", e.target.value)
                }
                error={!!this.state.errors.firstName}
                helperText={this.state.errors.firstName}
                className={classes.formField}
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                label="Sur Name"
                required
                fullWidth
                variant="outlined"
                disabled={!this.state.editMode}
                value={
                  this.state.patientObj != null &&
                  this.state.patientObj.lastName
                    ? this.state.patientObj.lastName
                    : ""
                }
                onChange={(e) =>
                  this.handleInputChange("lastName", e.target.value)
                }
                error={!!this.state.errors.lastName}
                helperText={this.state.errors.lastName}
                className={classes.formField}
              />
            </Grid>
            <Grid item xs={12} sm={2}>
              <TextField
                label="Phone Number"
                required
                variant="outlined"
                fullWidth
                disabled={!this.state.editMode}
                value={
                  this.state.patientObj != null && this.state.patientObj.phone
                    ? this.state.patientObj.phone
                    : ""
                }
                onChange={(e) =>
                  this.handleInputChange("phone", e.target.value)
                }
                className={classes.formField}
              />
            </Grid>
            <Grid item xs={12} sm={2}>
              <TextField
                label="Date of Birth"
                required
                fullWidth
                disabled={!this.state.editMode}
                variant="outlined"
                type="date"
                inputProps={{
                  max: new Date().toISOString().split("T")[0], // Current date and time
                }}
                value={
                  this.state.patientObj && this.state.patientObj.dob
                    ? this.formatToISO(this.state.patientObj.dob)
                    : new Date().toISOString().split("T")[0]
                }
                InputLabelProps={{ shrink: true }}
                onKeyDown={(e) => e.preventDefault()}
                onChange={(e) => this.handleInputChange("dob", e.target.value)}
                error={!!this.state.errors.dob}
                helperText={this.state.errors.dob}
                className={classes.formField}
              />
            </Grid>
            <Grid item xs={12} sm={2}>
              <FormControl
                fullWidth
                disabled={!this.state.editMode}
                variant="outlined"
                className={classes.formField}
              >
                <InputLabel shrink required>
                  Gender
                </InputLabel>
                <NativeSelect
                  required
                  variant="outlined"
                  value={
                    this.state.patientObj != null
                      ? this.state.patientObj.gender
                      : "MALE"
                  }
                  onChange={(e) =>
                    this.handleInputChange("gender", e.target.value)
                  }
                  error={!!this.state.errors.gender}
                  inputProps={{ "aria-label": "gender" }}
                >
                  <option value="" disabled>
                    Select Gender
                  </option>
                  <option value="MALE">Male</option>
                  <option value="FEMALE">Female</option>
                  <option value="TRANS">Others</option>
                </NativeSelect>
              </FormControl>
            </Grid>
          </Grid>
          <line />
          {/* Row 2 */}
          <Grid container spacing={3} padding={"15px"}>
            {/* Appointment Form */}
            <Grid item xs={12} sm={6}>
              <TextField
                select
                value={
                  this.state.appointmentDetails?.appointmentType ??
                  this.state.appointmentSource[0]
                }
                defaultValue={this.state.appointmentSource[0]}
                label="Appointment Type"
                variant="outlined"
                fullWidth
                onChange={(e) =>
                  this.handleInputChange(
                    "appointmentType",
                    e.target.value,
                    "appointmentDetails",
                  )
                }
                error={!!this.state.errors.appointmentType}
                helperText={this.state.errors.appointmentType}
                className={classes.formField}
              >
                {this.state.appointmentSource.map((type) => (
                  <MenuItem key={type} value={type}>
                    {type}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                label="Appointment Date"
                required
                fullWidth
                variant="outlined"
                type="datetime-local"
                onKeyDown={(e) => e.preventDefault()}
                inputProps={{
                  min: "2020-01-01T00:00", // Replace this with your desired minimum date
                }}
                value={
                  this.state.appointmentDetails.appointmentDate
                    ? this.state.appointmentDetails.appointmentDate
                    : new Date().toISOString()
                }
                onChange={(e) =>
                  this.handleInputChange(
                    "appointmentDate",
                    e.target.value,
                    "appointmentDetails",
                  )
                }
                error={!!this.state.errors.appointmentDate}
                helperText={this.state.errors.appointmentDate}
                className={classes.formField}
              />
            </Grid>
          </Grid>
          <Grid container spacing={3} padding={"15px"}>
            <Grid item xs={12} sm={6}>
              <Autocomplete
                options={this.state.doctorAutoSearchData}
                getOptionLabel={(option) =>
                  `${option.firstName} ${option.lastName}  - ${option.specialization}`
                }
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Doctor"
                    variant="outlined"
                    margin="dense"
                    required
                    fullWidth
                    onChange={this.doctorAutoSearch}
                  />
                )}
                onChange={(_event, option) => {
                  if (option) this.setState({ doctorObj: option });
                  else this.setState({ doctorObj: null });
                }}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Autocomplete
                options={this.state.referrerAutoSearchData}
                getOptionLabel={(option) =>
                  `${option.firstName} ${option.lastName}`
                }
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Referrer (Optional)"
                    variant="outlined"
                    margin="dense"
                    fullWidth
                    onChange={this.referrerAutoSearch}
                  />
                )}
                onChange={(_event, option) => {
                  if (option) this.setState({ referrerObj: option });
                }}
              />
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <TextField
              label="Clinical Information"
              required
              fullWidth
              variant="outlined"
              onChange={(e) =>
                this.handleInputChange(
                  "clinicalInformation",
                  e.target.value,
                  "appointmentDetails",
                )
              }
              error={!!this.state.errors.clinicalInformation}
              helperText={this.state.errors.clinicalInformation}
              className={classes.formField}
            />
          </Grid>
          {/* Submit Button */}
          <Grid item xs={12}>
            <Button
              variant="contained"
              color="primary"
              fullWidth
              onClick={this.handleSubmit}
              className={classes.button}
            >
              Book Appointment
            </Button>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(NewAppointment);
