import React, { useState } from "react";
import Identity from "../services/Identity"; // Import your Identity service
import { Button, TextField, Typography, Paper, Grid } from "@mui/material";
import useStyles from "../constants/styles"; // Adjust the path as necessary
import { HashLoader } from "react-spinners";

const PasswordReset = () => {
  const classes = useStyles();
  const [phone, setPhone] = useState("");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [loading, setLoading] = useState(false);
  const handleSubmit = (e) => {
    e.preventDefault();
    const identityService = new Identity();

    // Reset error and success messages
    setError("");
    setSuccess("");
    setLoading(true);
    // Validate phone number (assuming it should be 10 digits)
    if (phone.length !== 10 || isNaN(phone)) {
      setError("Phone number must be a 10-digit number.");
      return;
    }

    identityService
      .resetPassword(phone)
      .then((res) => {
        setLoading(false);
        if (res.data === true) {
          setPhone("");
          setSuccess(`Password Successfully reset for ${phone}`);
        } else {
          setError("Password Reset Failed");
        }
      })
      .catch((error) => {
        setError(`Failed Resetting Password: `, error);
        setLoading(false);
      });
  };

  return loading ? (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
      }}
    >
      <HashLoader
        color={"#00ffff"}
        loading={true}
        size={50}
        aria-label="Loading Spinner"
        data-testid="loader"
      />
    </div>
  ) : (
    <Paper elevation={3} className={classes.paper}>
      <Typography
        variant="h4"
        style={{
          fontWeight: "bold",
          padding: "8px 16px",
          borderBottom: "2px solid #f0f0f0",
          color: "#3f51b5",
          textAlign: "center",
        }}
      >
        Reset Password
      </Typography>

      <form onSubmit={handleSubmit} className={classes.formAddUser}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <TextField
              label="Login Phone No."
              name="phone"
              type="number"
              value={phone}
              onChange={(e) => setPhone(e.target.value)}
              required
              fullWidth
              error={!!error} // Indicate error state
              helperText={error} // Show error message
              className={classes.textField}
            />
          </Grid>
          <Grid item xs={12}>
            <Button
              className={classes.buttonSpacing}
              variant="contained"
              color="primary"
              type="submit"
            >
              Reset
            </Button>
          </Grid>
          {success && (
            <Grid item xs={12}>
              <Typography color="green" align="center">
                {success}
              </Typography>
            </Grid>
          )}
        </Grid>
      </form>
    </Paper>
  );
};

export default PasswordReset;
