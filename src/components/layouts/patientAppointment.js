import Identity from "../services/Identity";
import Appointment from "../services/Appointment";
import React, { Component, useStyles } from "react";
import { withStyles } from "@mui/styles";
import { Alert } from "react-bootstrap";
import { PdfDialog } from "./pdfViewer";
import {
  MenuItem,
  TextField,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
} from "@mui/material";

const styles = {
  container: {
    padding: "20px",
    fontFamily: "Arial, sans-serif",
  },
  header: {
    marginBottom: "20px",
    fontSize: "24px",
    fontWeight: "bold",
  },
  form: {
    display: "grid",
    gap: "5px",
    marginBottom: "10px",
  },
  formGroup: {
    display: "flex",
    flexDirection: "column",
  },
  label: {
    fontWeight: "bold",
    marginBottom: "5px",
  },
  input: {
    padding: "8px",
    fontSize: "14px",
    borderRadius: "4px",
    border: "1px solid #ccc",
  },
  buttonGroup: {
    display: "flex",
    gap: "10px",
  },
  button: {
    padding: "10px 15px",
    fontSize: "14px",
    borderRadius: "4px",
    cursor: "pointer",
    border: "none",
  },
  saveButton: {
    backgroundColor: "#007bff",
    color: "#fff",
  },
  retrieveButton: {
    backgroundColor: "#28a745",
    color: "#fff",
  },
  errorMessage: {
    color: "red",
  },
  successMessage: {
    color: "green",
  },
  retrievedVitals: {
    marginTop: "20px",
  },
};

class VitalsPage extends Component {
  state = {
    patientId: "",
    vitalDetails: {
      patientId: "",
      temperature: "",
      temperatureScale: "Fahrenheit",
      temperatureMethod: "Skin",
      spo2: "",
      systolic: "",
      diastolic: "",
      pulseRate: "",
      respirationRate: "",
      weight: "",
      height: "",
      parentId: "",
    },
    billId: "",
    open: false,
    appointmentDetails: null,
    appointmentId: null,
    retrievedVitals: [],
    loading: false,
    message: "",
    temperatureScale: ["Fahrenheit", "Celsius"],
    temperatureMethod: ["Orally", "Rectally", "Axillary", "Ear", "Skin"],
  };

  fetchAppointmentDetails = (billId) => {
    let appointmentService = new Appointment();
    appointmentService
      .fetchAppointmentDetails(billId)
      .then((res) => {
        const appointmentId = res.data.appointmentDetails.appointmentId;
        this.setState(
          {
            appointmentDetails: res.data,
            appointmentId: appointmentId,
          },
          () => {
            // Callback ensures state is updated before proceeding
            this.retrieveVitals(appointmentId);
          },
        );
      })
      .catch((error) => {
        Alert("Failed fetching appointment details");
      });
  };

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState((prevState) => ({
      vitalDetails: {
        ...prevState.vitalDetails,
        [name]: value,
      },
    }));
  };

  // Save patient vitals
  saveVitals = () => {
    try {
      this.setState({ loading: true });
      var vitalDetails = this.state.vitalDetails;
      vitalDetails.parentId = this.state.appointmentId;
      vitalDetails.patientId = this.state.appointmentDetails.patientDetails.id;
      let identityService = new Identity();
      identityService.saveVitals(this.state.vitalDetails).then((res) => {
        this.retrieveVitals();
      });
      this.setState({
        loading: false,
        message: "Vitals saved successfully",
        vitalDetails: {
          patientId: "",
          temperature: "",
          temperatureScale: "Fahrenheit",
          temperatureMethod: "Skin",
          spo2: "",
          systolic: "",
          diastolic: "",
          pulseRate: "",
          respirationRate: "",
          weight: "",
          height: "",
          parentId: "",
        },
      });
    } catch (error) {
      this.setState({ loading: false, message: "Failed to save vitals." });
    }
  };

  // Retrieve patient vitals
  retrieveVitals() {
    try {
      this.setState({ loading: true });
      var identityService = new Identity();
      identityService.fetchVitals(this.state.appointmentId).then((res) => {
        this.setState({
          retrievedVitals: res.data,
          loading: false,
        });
      });
    } catch (error) {
      this.setState({ loading: false, message: "Failed to retrieve vitals." });
    }
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  componentDidMount() {
    if (this.props && this.props.inputId) {
      const id = this.props.inputId;
      this.setState({ loading: true, billId: id });
      this.fetchAppointmentDetails(id);
      this.setState({ loading: false });
    }
  }
  render() {
    const { classes } = this.props;
    const { vitalDetails, loading, message, isError, retrievedVitals } =
      this.state;

    return (
      <div className={classes.container}>
        <PdfDialog
          open={this.state.open}
          onClose={this.handleClose}
          header={true}
          type="prescription"
          id={this.state.billId}
        />
        <h1>Appointment</h1>
        {this.state.appointmentDetails &&
        this.state.appointmentDetails.patientDetails ? (
          <h6 className={classes.header}>
            Patient Vitals (
            {this.state.appointmentDetails.patientDetails.firstName}{" "}
            {this.state.appointmentDetails.patientDetails.lastName})
          </h6>
        ) : (
          <h6 className={classes.header}>Patient Vitals</h6>
        )}
        {message && (
          <p
            className={isError ? classes.errorMessage : classes.successMessage}
          >
            {message}
          </p>
        )}
        <form className={classes.form}>
          <Grid container spacing={2}>
            {/* First Row */}
            <Grid item xs={12}>
              <Grid container spacing={2}>
                {Object.keys(vitalDetails).map((key) => {
                  if (key == "patientId" || key == "parentId") {
                    return;
                  } else if (key === "temperatureScale") {
                    return (
                      <Grid item xs={2}>
                        <TextField
                          label="Temperature Scale"
                          name="temperatureScale"
                          fullWidth
                          value={this.state.vitalDetails.temperatureScale}
                          onChange={this.handleChange}
                          select
                          variant="outlined"
                          margin="dense"
                        >
                          {this.state.temperatureScale.map((status) => (
                            <MenuItem key={status} value={status}>
                              {status}
                            </MenuItem>
                          ))}
                        </TextField>
                      </Grid>
                    );
                  } else if (key === "temperatureMethod") {
                    return (
                      <Grid item xs={2}>
                        <TextField
                          label="Temperature Method"
                          name="temperatureMethod"
                          defaultValue={
                            this.state.vitalDetails.temperatureMethod[0]
                          }
                          fullWidth
                          value={this.state.vitalDetails.temperatureMethod}
                          onChange={this.handleChange}
                          select
                          variant="outlined"
                          margin="dense"
                        >
                          {this.state.temperatureMethod.map((status) => (
                            <MenuItem key={status} value={status}>
                              {status}
                            </MenuItem>
                          ))}
                        </TextField>
                      </Grid>
                    );
                  } else {
                    return (
                      <Grid item xs={2} key={key}>
                        <TextField
                          label={
                            key.charAt(0).toUpperCase() +
                            key.slice(1).replace(/([A-Z])/g, " $1")
                          }
                          name={key}
                          fullWidth
                          value={vitalDetails[key]}
                          onChange={this.handleChange}
                          variant="outlined"
                          margin="dense"
                          type={key === "patientId" ? "text" : "number"}
                        />
                      </Grid>
                    );
                  }
                })}
              </Grid>
            </Grid>
          </Grid>
        </form>
        <div className={classes.buttonGroup}>
          <button
            onClick={this.saveVitals}
            disabled={loading}
            className={`${classes.button} ${classes.saveButton}`}
          >
            Save Vitals
          </button>
        </div>
        <TableContainer component={Paper}>
          <Table>
            {/* Table Header */}
            <TableHead>
              <TableRow>
                <TableCell>
                  <b>Patient ID</b>
                </TableCell>
                <TableCell>
                  <b>Temperature</b>
                </TableCell>
                <TableCell>
                  <b>Temperature Scale</b>
                </TableCell>
                <TableCell>
                  <b>Temperature Method</b>
                </TableCell>
                <TableCell>
                  <b>SpO2</b>
                </TableCell>
                <TableCell>
                  <b>Systolic</b>
                </TableCell>
                <TableCell>
                  <b>Diastolic</b>
                </TableCell>
                <TableCell>
                  <b>Pulse Rate</b>
                </TableCell>
                <TableCell>
                  <b>Respiration Rate</b>
                </TableCell>
                <TableCell>
                  <b>Weight (In kgs)</b>
                </TableCell>
                <TableCell>
                  <b>Height (In cms)</b>
                </TableCell>
                <TableCell>
                  <b>Updated At</b>
                </TableCell>
              </TableRow>
            </TableHead>

            {/* Table Body */}
            <TableBody>
              {this.state.retrievedVitals.map((row) => (
                <TableRow key={row.id}>
                  <TableCell>{row.patientId}</TableCell>
                  <TableCell>{row.temperature}</TableCell>
                  <TableCell>{row.temperatureScale}</TableCell>
                  <TableCell>{row.temperatureMethod}</TableCell>
                  <TableCell>{row.spo2}</TableCell>
                  <TableCell>{row.systolic}</TableCell>
                  <TableCell>{row.diastolic}</TableCell>
                  <TableCell>{row.pulseRate}</TableCell>
                  <TableCell>{row.respirationRate}</TableCell>
                  <TableCell>{row.weight}</TableCell>
                  <TableCell>{row.height}</TableCell>
                  <TableCell>
                    {new Date(row.createdAt).toLocaleString()}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <br />
        <div className={classes.buttonGroup}>
          <button
            onClick={this.handleOpen}
            disabled={loading}
            className={`${classes.button} ${classes.saveButton}`}
          >
            Generate Prescription
          </button>
        </div>
      </div>
    );
  }
}
export default withStyles(styles)(VitalsPage);
