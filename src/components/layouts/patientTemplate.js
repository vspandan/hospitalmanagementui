import React, { Component } from "react";
import Paper from "@mui/material/Paper";
import { withStyles } from "@mui/styles";
import { State, City, Country } from "country-state-city";
import {
  TextField,
  Button,
  TableCell,
  TableRow,
  NativeSelect,
  FormControlLabel,
  RadioGroup,
  Radio,
  Grid,
  Box,
  FormControl,
  InputLabel,
  FormHelperText,
} from "@mui/material";
import { Autocomplete } from "@mui/lab";
import Typography from "@mui/material/Typography";
import Identity from "../services/Identity";
import { Table } from "react-bootstrap";
import useStyles from "../constants/styles";
import { HashLoader } from "react-spinners";

class PatientTemplate extends Component {
  state = {
    id: null,
    code: null,
    redirect: false,
    version: null,
    patientObj: null,
    userAutoSearchData: [],
    editMode: false,
    errors: {},
    loading: false,
    primaryAddress: {},
    selectedCountry: "India",
    selectedState: "Telangana",
    states: [],
  };

  handleEditClick = () => {
    this.setState({ editMode: true });
  };

  handleCancelClick = () => {
    this.setState({ editMode: false });
  };

  savePatientDetails = () => {
    let identityService = new Identity();
    let updatedErrors = { ...this.state.errors }; // Copy current error state
    let isValid = true; // To track overall form validity

    // First Name validation
    if (
      !this.state.patientObj.firstName ||
      !this.state.patientObj.firstName.match(/^[A-Za-z]+$/)
    ) {
      isValid = false;
      updatedErrors["firstName"] = "First name must contain only letters.";
    } else {
      updatedErrors["firstName"] = "";
    }

    // Last Name validation
    if (
      !this.state.patientObj.lastName ||
      !this.state.patientObj.lastName.match(/^[A-Za-z]+$/)
    ) {
      isValid = false;
      updatedErrors["lastName"] = "Last name must contain only letters.";
    } else {
      updatedErrors["lastName"] = "";
    }

    // Gender validation
    if (!this.state.patientObj.gender) {
      this.state.patientObj.gender = "NOT_SPECIFIED";
    }

    // Date of Birth validation
    if (
      !this.state.patientObj.dob ||
      isNaN(new Date(this.state.patientObj.dob).getTime())
    ) {
      isValid = false;
      updatedErrors["dob"] = "Valid date of birth is required.";
    } else {
      updatedErrors["dob"] = "";
    }

    // PAN Card validation
    const panRegex = /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
    if (
      this.state.patientObj.panCard &&
      !this.state.patientObj.panCard.match(panRegex)
    ) {
      isValid = false;
      updatedErrors["panCard"] =
        "Invalid PAN format. Please enter a valid PAN number.";
    } else {
      updatedErrors["panCard"] = "";
    }

    // Nationality and Citizenship validation (defaulting if not set)
    if (!this.state.patientObj.citizenShip) {
      this.state.patientObj.citizenShip = "India";
    }
    if (!this.state.patientObj.nationality) {
      this.state.patientObj.nationality = "India";
    }

    // Blood group validation
    if (!this.state.patientObj.bloodGroup || !this.state.patientObj.rhFactor) {
      isValid = false;
      updatedErrors["bloodGroup"] = "Blood group and Rh factor are required.";
    } else {
      updatedErrors["bloodGroup"] = "";
    }

    // Address validations
    if (!this.state.primaryAddress) {
      isValid = false;
      updatedErrors["address"] = "Primary address is required.";
    } else {
      // PIN code validation
      if (
        !this.state.primaryAddress.pinCode ||
        !this.state.primaryAddress.pinCode.match(/^\d{6}$/)
      ) {
        isValid = false;
        updatedErrors["pinCode"] = "PIN code must be a 6-digit number.";
      } else {
        updatedErrors["pinCode"] = "";
      }

      // City validation
      if (
        !this.state.primaryAddress.city ||
        this.state.primaryAddress.city.trim().length === 0
      ) {
        isValid = false;
        updatedErrors["city"] = "City is required.";
      } else {
        updatedErrors["city"] = "";
      }

      this.state.primaryAddress.state = this.state.selectedState ?? "";
      // State validation
      if (
        !this.state.primaryAddress.state ||
        this.state.primaryAddress.state.trim().length === 0
      ) {
        isValid = false;
        updatedErrors["state"] = "State is required.";
      } else {
        updatedErrors["state"] = "";
      }

      this.state.primaryAddress.country = this.state.selectedCountry;
      // Country validation
      if (
        !this.state.primaryAddress.country ||
        this.state.primaryAddress.country.trim().length === 0
      ) {
        isValid = false;
        updatedErrors["country"] = "Country is required.";
      } else {
        updatedErrors["country"] = "";
      }

      // Contact number validation
      /*if (
        !this.state.primaryAddress.phoneNo ||
        !this.state.primaryAddress.phoneNo.match(/^[0-9]{10}$/)
      ) {
        isValid = false;
        updatedErrors["phoneNo"] = "Contact number must be a 10-digit number.";
      } else {
        updatedErrors["phoneNo"] = "";
      }*/

      // Email validation
      if (
        this.state.primaryAddress.emailAddress &&
        (!this.state.primaryAddress.emailAddress.match(
          /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
        ) ||
          this.state.primaryAddress.emailAddress.length > 100)
      ) {
        isValid = false;
        updatedErrors["emailAddress"] =
          "Invalid email format or email address exceeds 100 characters.";
      } else {
        updatedErrors["emailAddress"] = "";
      }
    }

    // Update errors state and check if the form is valid
    this.setState({ errors: updatedErrors });

    // If any validation failed, don't proceed with registration
    if (!isValid) {
      alert("Please fill all the mandatory information in the form.");
      return;
    }
    this.state.patientObj.address = [this.state.primaryAddress];
    this.setState({ loading: true });
    identityService
      .updatePatientDetails(this.state.patientObj)
      .then((res) => {
        if (res.data) {
          alert("Successfully Updated Patient Details");
          this.setState({ editMode: false, loading: false });
        } else {
          alert("Failed Patient Details Updation");
          this.setState({ loading: false });
        }
      })
      .catch((error) => {
        console.error("Error updating patient data:", error);
        this.setState({ loading: false });
      });
  };

  closePatientDetails = () => {
    this.setState({
      patientObj: null,
    });
  };

  submitForm = () => {};

  updateStates() {
    if (this.state.selectedCountry) {
      const countryIsoCode = Country.getAllCountries().find(
        (c) => c.name === this.state.selectedCountry,
      )?.isoCode;

      if (countryIsoCode) {
        this.setState({ states: State.getStatesOfCountry(countryIsoCode) });
      } else {
        this.setState({ states: [] });
      }
    }
  }
  componentDidMount() {
    this.updateStates();
  }

  userAutoSearch = (event) => {
    let identityService = new Identity();
    identityService
      .userAutoSearch(event.target.value)
      .then((res) =>
        this.setState({ userAutoSearchData: res.data, loading: false }),
      )
      .catch((error) => {
        console.error("Error searching patient: ", error);
        this.setState({ loading: false });
      });
  };

  unSetDoctor() {
    this.setState({ docterReferenceId: null });
  }

  fetchPatientDetails(id) {
    let identityService = new Identity();
    this.setState({ loading: true });
    identityService
      .getDetailedUserDetails(id)
      .then((res) => {
        var address =
          res.data.address != null && res.data.address.length >= 1
            ? res.data.address[0]
            : {};
        this.setState({
          patientObj: res.data,
          primaryAddress: address,
          loading: false,
        });
      })
      .catch((error) => {
        console.error("Error fetching patient information: ", error);
        this.setState({ loading: false });
      });
  }

  handleEdits = (type, attr) => (event) => {
    const value = event.target.value;
    let isValid = true;
    let errorMessage = "";
    let updatedErrors = { ...this.state.errors }; // Copy current error state

    switch (type) {
      case "demo":
        switch (attr) {
          case "fname":
            if (!value.match(/^[A-Za-z]+$/)) {
              isValid = false;
              errorMessage = "First name must contain only letters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.firstName = value;
            break;
          case "lname":
            if (!value.match(/^[A-Za-z]+$/)) {
              isValid = false;
              errorMessage = "Last name must contain only letters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.lastName = value;
            break;
          case "gender":
            if (!value) {
              isValid = false;
              errorMessage = "Gender is required.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.gender = value;
            break;
          case "fatherName":
            if (!this.state.patientObj.father)
              this.state.patientObj.father = {};
            if (!value.match(/^[A-Za-z ]+$/)) {
              isValid = false;
              errorMessage = "Father's name must contain only letters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.father.firstName = value;
            break;
          case "motherName":
            if (!this.state.patientObj.mother)
              this.state.patientObj.mother = {};
            if (!value.match(/^[A-Za-z ]+$/)) {
              isValid = false;
              errorMessage = "Mother's name must contain only letters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.mother.firstName = value;
            break;
          case "aadhaar":
            this.state.patientObj.aadhaar = value;
            break;
          case "panCard":
            var val = value.toUpperCase();
            const panRegex = /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
            if (!val.match(panRegex)) {
              isValid = false;
              errorMessage =
                "Invalid PAN format. Please enter a valid PAN number.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.panCard = val;
            break;
          case "referredBy":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "Referred by is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "Referred by must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.referredBy = value;
            break;
          case "spouseName":
            if (!this.state.patientObj.spouse)
              this.state.patientObj.spouse = {};
            if (!value.match(/^[A-Za-z ]+$/)) {
              isValid = false;
              errorMessage = "Spouse's name must contain only letters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.spouse.firstName = value;
            break;
          case "nationality":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "Nationality is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "Nationality must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.nationality = value;
            break;
          case "maritialStatus":
            if (!value) {
              isValid = false;
              errorMessage = "Marital status is required.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.maritialStatus = value;
            break;
          case "birth":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "Place of birth is required.";
            } else if (value.length > 100) {
              isValid = false;
              errorMessage = "Place of birth must not exceed 100 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.birthPlace = value;
            break;
          case "dob":
            let dateofBirth = new Date(value);
            if (!value || isNaN(dateofBirth.getTime())) {
              isValid = false;
              errorMessage = "Valid date of birth is required.";
            } else if (dateofBirth > new Date()) {
              isValid = false;
              errorMessage = "Date of birth cannot be in the future.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.dob = dateofBirth.getTime();
            break;
          case "bloodGroup":
            let bloodGroup = value.split(":");
            isValid = true;
            errorMessage = "";
            this.state.patientObj.bloodGroup = bloodGroup[0];
            this.state.patientObj.rhFactor = bloodGroup[1];
            break;
          default:
            break;
        }
        break;

      case "paddr":
        if (!this.state.primaryAddress) this.state.primaryAddress = {};
        switch (attr) {
          case "cname":
            if (value && !value.trim()) {
              isValid = false;
              errorMessage = "Emergency Contact name is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage =
                "Emergency Contact name must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.eContactName = value;
            break;
          case "cnum":
            if (!value.match(/^[0-9]{10}$/)) {
              isValid = false;
              errorMessage = "Contact number must be a 10-digit number.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.eContactNumber = value;

            break;
          case "crelation":
            if (value && !value.trim()) {
              isValid = false;
              errorMessage = "Emergency Contact Relation is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage =
                "Emergency Contact Relation must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.patientObj.eRelation = value;
            break;
          case "pinCode":
            if (!value.match(/^\d{6}$/)) {
              isValid = false;
              errorMessage = "PIN code must be a 6-digit number.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.primaryAddress.pinCode = value;
            break;
          case "city":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "City is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "City must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.primaryAddress.city = value;
            break;
          case "country":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "Country is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "Country must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.primaryAddress.country = value;
            break;
          case "district":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "District is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "District must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.primaryAddress.district = value;
            break;
          case "state":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "State is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "State must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.setState({ selectedState: value });
            this.state.primaryAddress.state = value;
            break;
          case "doorNo":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "Door number is required.";
            } else if (value.length > 20) {
              isValid = false;
              errorMessage = "Door number must not exceed 20 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.primaryAddress.doorNo = value;
            break;
          case "streetName":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "Street name is required.";
            } else if (value.length > 100) {
              isValid = false;
              errorMessage = "Street name must not exceed 100 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.primaryAddress.streetName = value;
            break;
          case "locality":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "Locality is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "Locality must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.primaryAddress.locality = value;
            break;
          case "viaName":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "Landmark is required.";
            } else if (value.length > 100) {
              isValid = false;
              errorMessage = "Landmark must not exceed 100 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.primaryAddress.viaName = value;
            break;
          case "contactName":
            if (!value.match(/^[A-Za-z]+$/)) {
              isValid = false;
              errorMessage = "Contact name must contain only letters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.primaryAddress.contactName = value;
            break;
          case "phoneNo":
            if (!value.match(/^[0-9]{10}$/)) {
              isValid = false;
              errorMessage = "Contact number must be a 10-digit number.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            this.state.primaryAddress.phoneNo = value;
            this.state.patientObj.loginPhoneNo = value;
            break;
          case "emailAddress":
            if (!value.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)) {
              isValid = false;
              errorMessage = "Invalid email address.";
            } else if (value.length > 100) {
              isValid = false;
              errorMessage = "Email address must not exceed 100 characters.";
            }
            this.state.primaryAddress.emailAddress = value;
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }
    this.setState({ patientObj: this.state.patientObj });
    // Update errors state
    if (!isValid) {
      updatedErrors[attr] = errorMessage; // Set the error message for the invalid field
    } else {
      updatedErrors[attr] = ""; // Clear the error message if valid
    }

    // Update state with new data and errors
    this.setState({ errors: updatedErrors });
  };

  render() {
    const { classes } = this.props;
    const { inputId } = this.props;
    const { patientObj } = this.state;
    if (inputId != null && patientObj === null) {
      this.fetchPatientDetails(inputId);
    }
    return this.state.loading ? (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <HashLoader
          color={"#00ffff"}
          loading={true}
          size={50}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
      </div>
    ) : (
      <main className={classes.content}>
        <Typography
          variant="h4"
          style={{
            fontWeight: "bold",
            padding: "8px 16px",
            borderBottom: "2px solid #f0f0f0",
            color: "#3f51b5",
            textAlign: "center",
          }}
        >
          User Details
        </Typography>
        <Autocomplete
          options={this.state.userAutoSearchData}
          getOptionLabel={(option) =>
            option.code +
            "-" +
            (option.firstName ? option.firstName + "-" : "") +
            (option.lastName ? option.lastName + "-" : "") +
            option.phone
          }
          style={{ width: 500 }}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Search User"
              variant="outlined"
              margin="dense"
              fullWidth
              onChange={this.userAutoSearch}
            />
          )}
          onChange={(event, option) => {
            if (option !== null && option !== undefined)
              this.fetchPatientDetails(option.id);
          }}
        />
        {this.state.patientObj ? (
          <Paper className={classes.paper} elevation={5} variant="outlined">
            <div className={classes.tableElements}>
              <br />
              <Grid container spacing={2} className={classes.mintable}>
                <Grid item xs={6} sm={2} className={classes.table_td1}>
                  <Typography variant="body1">
                    <b>Code</b>
                  </Typography>
                </Grid>
                <Grid item xs={6} sm={2} className={classes.table_td1}>
                  <Typography variant="body1">
                    {this.state.patientObj.code}
                  </Typography>
                </Grid>

                <Grid item xs={6} sm={2} className={classes.table_td1}>
                  <Typography variant="body1">
                    <b>Phone</b>
                  </Typography>
                </Grid>
                <Grid item xs={6} sm={2} className={classes.table_td1}>
                  <Typography variant="body1">
                    {this.state.patientObj.loginPhoneNo}
                  </Typography>
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Email Address</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      helperText={
                        !!this.state.errors.emailAddress &&
                        this.state.errors.emailAddress
                      }
                      error={!!this.state.errors.emailAddress}
                      variant="outlined"
                      margin="dense"
                      value={
                        this.state.primaryAddress
                          ? this.state.primaryAddress.emailAddress
                          : ""
                      }
                      onChange={this.handleEdits("paddr", "emailAddress")}
                    ></TextField>
                  ) : this.state.primaryAddress ? (
                    this.state.primaryAddress.emailAddress
                  ) : (
                    ""
                  )}
                </Grid>
                <Grid item xs={6} sm={2} className={classes.table_td1}>
                  <Typography variant="body1">
                    <b>User Type</b>
                  </Typography>
                </Grid>
                <Grid item xs={6} sm={2} className={classes.table_td1}>
                  <Typography variant="body1">
                    {this.state.patientObj.type}
                  </Typography>
                </Grid>

                <Grid item xs={6} sm={2} className={classes.table_td1}>
                  <Typography variant="body1">
                    <b>Login Id</b>
                  </Typography>
                </Grid>
                <Grid item xs={6} sm={2} className={classes.table_td1}>
                  <Typography variant="body1">
                    {this.state.patientObj.loginPhoneNo}
                  </Typography>
                </Grid>

                <Grid item xs={6} sm={2} className={classes.table_td1}>
                  <Typography variant="body1">
                    <b>Referred By</b>
                  </Typography>
                </Grid>
                <Grid item xs={6} sm={2} className={classes.table_td1}>
                  <Typography variant="body1">
                    {this.state.patientObj.referredBy}
                  </Typography>
                </Grid>
              </Grid>
              <Typography
                variant="h6"
                style={{
                  fontWeight: "bold",
                  padding: "8px 16px",
                  borderBottom: "2px solid #f0f0f0",
                  color: "#3f51b5",
                  textAlign: "center",
                }}
              >
                <b>Demographics Information</b>
              </Typography>
              <br />
              <Grid container spacing={2}>
                <Grid item xs={6} sm={2}>
                  {" "}
                  <b>First Name *</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      helperText={
                        !!this.state.errors.fname && this.state.errors.fname
                      }
                      error={!!this.state.errors.fname}
                      variant="outlined"
                      margin="dense"
                      value={this.state.patientObj.firstName}
                      onChange={this.handleEdits("demo", "fname")}
                    ></TextField>
                  ) : (
                    this.state.patientObj.firstName
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Sur Name *</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      helperText={
                        !!this.state.errors.lname && this.state.errors.lname
                      }
                      error={!!this.state.errors.lname}
                      variant="outlined"
                      margin="dense"
                      value={this.state.patientObj.lastName}
                      onChange={this.handleEdits("demo", "lname")}
                    ></TextField>
                  ) : (
                    this.state.patientObj.lastName
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Date Of Birth*</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      value={
                        new Date(this.state.patientObj.dob)
                          .toISOString()
                          .split("T")[0]
                      }
                      margin="dense"
                      color="black"
                      variant="outlined"
                      type="date"
                      inputProps={{
                        max: new Date().toISOString().split("T")[0], // Current date and time
                      }}
                      onKeyDown={(e) => e.preventDefault()}
                      onChange={this.handleEdits("demo", "dob")}
                    />
                  ) : (
                    `${new Date(this.state.patientObj.dob)
                      .getDate()
                      .toString()
                      .padStart(2, "0")}/${(
                      new Date(this.state.patientObj.dob).getMonth() + 1
                    )
                      .toString()
                      .padStart(2, "0")}/${new Date(
                      this.state.patientObj.dob,
                    ).getFullYear()}`
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Place Of Birth</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      helperText={
                        !!this.state.errors.birth && this.state.errors.birth
                      }
                      error={!!this.state.errors.birth}
                      variant="outlined"
                      margin="dense"
                      value={this.state.patientObj.birthPlace}
                      onChange={this.handleEdits("demo", "birth")}
                    ></TextField>
                  ) : (
                    this.state.patientObj.birthPlace
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Gender*</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <RadioGroup
                      aria-label="gender"
                      name="gender1"
                      row
                      value={this.state.patientObj.gender}
                      onChange={this.handleEdits("demo", "gender")}
                    >
                      <FormControlLabel
                        value="MALE"
                        control={<Radio />}
                        label="MALE"
                        labelPlacement="start"
                      />
                      <FormControlLabel
                        value="FEMALE"
                        control={<Radio />}
                        label="FEMALE"
                        labelPlacement="start"
                      />
                      <FormControlLabel
                        value="TRANS"
                        control={<Radio />}
                        label="OTHERS"
                        labelPlacement="start"
                      />
                    </RadioGroup>
                  ) : (
                    this.state.patientObj.gender
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Blood Group *</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <div className={classes.tableElements}>
                      <NativeSelect
                        fullWidth
                        onChange={this.handleEdits("demo", "bloodGroup")}
                        defaultValue={
                          this.state.patientObj.bloodGroup +
                          ":" +
                          this.state.patientObj.rhFactor
                        }
                      >
                        <option value={null}>NOT SPECIFIED</option>
                        <option value={"O:POSITIVE"}>O+</option>
                        <option value={"O:NEGATIVE"}>O-</option>
                        <option value={"A:POSITIVE"}>A+</option>
                        <option value={"A:NEGATIVE"}>A-</option>
                        <option value={"B:POSITIVE"}>B+</option>
                        <option value={"B:NEGATIVE"}>B-</option>
                        <option value={"AB:POSITIVE"}>AB+</option>
                        <option value={"AB:NEGATIVE"}>AB-</option>
                      </NativeSelect>
                    </div>
                  ) : (
                    `${this.state.patientObj.bloodGroup}${
                      this.state.patientObj.rhFactor === "POSITIVE" ? "+" : "-"
                    }`
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Marital Status*</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <NativeSelect
                      fullWidth
                      onChange={this.handleEdits("demo", "maritialStatus")}
                      defaultValue={
                        this.state.patientObj.maritialStatus
                          ? this.state.patientObj.maritialStatus
                          : null
                      }
                    >
                      <option value={null}>NOT SPECIFIED</option>
                      <option value={"SINGLE"}>SINGLE</option>
                      <option value={"MARRIED"}>MARRIED</option>
                      <option value={"DIVORCED"}>DIVORCED</option>
                      <option value={"WIDOWED"}>WIDOWED</option>
                    </NativeSelect>
                  ) : (
                    this.state.patientObj.maritialStatus
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Nationality</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.patientObj.nationality}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>CitizenShip</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.patientObj.citizenShip}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Aadhaar</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.patientObj.aadhaar}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>PAN ID</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.patientObj.panCard}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>ABHA ID</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.patientObj.abhaId}
                </Grid>
              </Grid>

              <Typography
                variant="h6"
                style={{
                  fontWeight: "bold",
                  padding: "8px 16px",
                  borderBottom: "2px solid #f0f0f0",
                  color: "#3f51b5",
                  textAlign: "center",
                }}
              >
                {" "}
                <b>Corresponding Address</b>
              </Typography>
              <br />
              <Grid container spacing={2}>
                <Grid item xs={6} sm={2}>
                  <b>Door No</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      helperText={
                        !!this.state.errors.doorNo && this.state.errors.doorNo
                      }
                      error={!!this.state.errors.doorNo}
                      variant="outlined"
                      margin="dense"
                      value={
                        this.state.primaryAddress
                          ? this.state.primaryAddress.doorNo
                          : ""
                      }
                      onChange={this.handleEdits("paddr", "doorNo")}
                    ></TextField>
                  ) : this.state.primaryAddress ? (
                    this.state.primaryAddress.doorNo
                  ) : (
                    ""
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Street Name</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      helperText={
                        !!this.state.errors.streetName &&
                        this.state.errors.streetName
                      }
                      error={!!this.state.errors.streetName}
                      variant="outlined"
                      margin="dense"
                      value={
                        this.state.primaryAddress
                          ? this.state.primaryAddress.streetName
                          : ""
                      }
                      onChange={this.handleEdits("paddr", "streetName")}
                    ></TextField>
                  ) : this.state.primaryAddress ? (
                    this.state.primaryAddress.streetName
                  ) : (
                    ""
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Locality</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      helperText={
                        !!this.state.errors.locality &&
                        this.state.errors.locality
                      }
                      error={!!this.state.errors.locality}
                      variant="outlined"
                      margin="dense"
                      value={
                        this.state.primaryAddress
                          ? this.state.primaryAddress.locality
                          : ""
                      }
                      onChange={this.handleEdits("paddr", "locality")}
                    ></TextField>
                  ) : this.state.primaryAddress ? (
                    this.state.primaryAddress.locality
                  ) : (
                    ""
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>LandMark</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      helperText={
                        !!this.state.errors.viaName && this.state.errors.viaName
                      }
                      error={!!this.state.errors.viaName}
                      variant="outlined"
                      margin="dense"
                      value={
                        this.state.primaryAddress
                          ? this.state.primaryAddress.viaName
                          : ""
                      }
                      onChange={this.handleEdits("paddr", "viaName")}
                    ></TextField>
                  ) : this.state.primaryAddress ? (
                    this.state.primaryAddress.viaName
                  ) : (
                    ""
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Country *</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    /*<TextField
                      helperText={
                        !!this.state.errors.country && this.state.errors.country
                      }
                      error={!!this.state.errors.country}
                      variant="outlined"
                      margin="dense"
                      value={
                        this.state.primaryAddress
                          ? this.state.primaryAddress.country
                          : ""
                      }
                      onChange={this.handleEdits("paddr", "country")}
                    ></TextField>
                    */
                    <FormControl
                      sx={{ paddingLeft: "10px" }}
                      fullWidth
                      variant="outlined"
                      error={!!this.state.errors.country}
                    >
                      <NativeSelect
                        value={this.state.selectedCountry}
                        onChange={(e) => {
                          const selectedValue = e.target.value;
                          this.setState(
                            { selectedCountry: selectedValue },
                            () => {
                              this.updateStates();
                            },
                          );
                          this.handleEdits("paddr", "country");
                          // Clear state when country changes
                          this.handleEdits(
                            "paddr",
                            "state",
                          )({ target: { value: "" } });
                        }}
                      >
                        <option value="">NOT SPECIFIED</option>
                        {Country.getAllCountries().map((country) => (
                          <option key={country.isoCode} value={country.name}>
                            {country.name}
                          </option>
                        ))}
                      </NativeSelect>
                      {this.state.errors.country && (
                        <FormHelperText>
                          {this.state.errors.country}
                        </FormHelperText>
                      )}
                    </FormControl>
                  ) : this.state.primaryAddress ? (
                    this.state.primaryAddress.country
                  ) : (
                    ""
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>State *</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    /*<TextField
                      helperText={
                        !!this.state.errors.state && this.state.errors.state
                      }
                      error={!!this.state.errors.state}
                      variant="outlined"
                      margin="dense"
                      value={
                        this.state.primaryAddress
                          ? this.state.primaryAddress.state
                          : ""
                      }
                      onChange={this.handleEdits("paddr", "state")}
                    ></TextField>*/ <FormControl
                      sx={{ paddingLeft: "10px" }}
                      fullWidth
                      variant="outlined"
                      error={!!this.state.errors.state}
                    >
                      <NativeSelect
                        value={this.state.selectedState}
                        onChange={this.handleEdits("paddr", "state")}
                        disabled={this.state.states.length === 0}
                      >
                        <option value="">NOT SPECIFIED</option>
                        {this.state.states.map((state) => (
                          <option key={state.isoCode} value={state.name}>
                            {state.name}
                          </option>
                        ))}
                      </NativeSelect>
                      {this.state.errors.state && (
                        <FormHelperText>
                          {this.state.errors.state}
                        </FormHelperText>
                      )}
                    </FormControl>
                  ) : this.state.primaryAddress ? (
                    this.state.primaryAddress.state
                  ) : (
                    ""
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>City *</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      helperText={
                        !!this.state.errors.city && this.state.errors.city
                      }
                      error={!!this.state.errors.city}
                      variant="outlined"
                      margin="dense"
                      value={
                        this.state.primaryAddress
                          ? this.state.primaryAddress.city
                          : ""
                      }
                      onChange={this.handleEdits("paddr", "city")}
                    ></TextField>
                  ) : this.state.primaryAddress ? (
                    this.state.primaryAddress.city
                  ) : (
                    ""
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>Pin Code *</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      helperText={
                        !!this.state.errors.pinCode && this.state.errors.pinCode
                      }
                      error={!!this.state.errors.pinCode}
                      variant="outlined"
                      margin="dense"
                      value={
                        this.state.primaryAddress
                          ? this.state.primaryAddress.pinCode
                          : ""
                      }
                      onChange={this.handleEdits("paddr", "pinCode")}
                    ></TextField>
                  ) : this.state.primaryAddress ? (
                    this.state.primaryAddress.pinCode
                  ) : (
                    ""
                  )}
                </Grid>
              </Grid>
              <Typography
                variant="h6"
                style={{
                  fontWeight: "bold",
                  padding: "8px 16px",
                  borderBottom: "2px solid #f0f0f0",
                  color: "#3f51b5",
                  textAlign: "center",
                }}
              >
                Emergency Contact Information
              </Typography>
              <br />
              <Grid container spacing={5}>
                {/* Contact Name */}
                <Grid item xs={6} sm={2}>
                  <b>Contact Name</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      value={this.state.patientObj.eContactName}
                      fullWidth
                      variant="outlined"
                      onChange={this.handleEdits("paddr", "cname")}
                      //error={!!errors.cname}
                      //helperText={errors.cname}
                    />
                  ) : (
                    this.state.patientObj.eContactName
                  )}
                </Grid>

                <Grid item xs={6} sm={2}>
                  <b>Contact Number</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <TextField
                      value={this.state.patientObj.eContactNumber}
                      fullWidth
                      variant="outlined"
                      onChange={this.handleEdits("paddr", "cnum")}
                      //error={!!errors.cnum}
                      //helperText={errors.cnum}
                    />
                  ) : (
                    this.state.patientObj.eContactNumber
                  )}
                </Grid>
                <Grid item xs={6} sm={2}>
                  <b>RelationShip</b>
                </Grid>
                <Grid item xs={6} sm={2}>
                  {this.state.editMode ? (
                    <div>
                      <NativeSelect
                        value={this.state.patientObj.eRelation || ""}
                        onChange={this.handleEdits("paddr", "crelation")}
                      >
                        <option value="">Select RelationShip</option>
                        <option value={"Parent"}>Parent</option>
                        <option value={"Spouse"}>Spouse</option>
                        <option value={"Sibling"}>Sibling</option>
                        <option value={"Friend"}>Friend</option>
                        <option value={"Other"}>Other</option>
                      </NativeSelect>
                      {
                        //errors.maritialStatus && (
                        //<FormHelperText>{errors.crelation}</FormHelperText>
                        //)
                      }
                    </div>
                  ) : (
                    this.state.patientObj.eRelation
                  )}
                </Grid>
              </Grid>
              <div className={classes.tableElements} align="right">
                {this.state.editMode ? (
                  <div>
                    <Box sx={{ display: "flex", gap: 2 }}>
                      <Button
                        className={classes.buttonSpacing}
                        variant="contained"
                        color="primary"
                        component="span"
                        onClick={this.savePatientDetails}
                        sx={{ backgroundColor: "black", color: "white" }}
                      >
                        Save
                      </Button>
                      <Button
                        className={classes.buttonSpacing}
                        variant="contained"
                        color="primary"
                        component="span"
                        onClick={this.handleCancelClick}
                      >
                        Cancel
                      </Button>
                    </Box>
                  </div>
                ) : (
                  <div>
                    <Box sx={{ display: "flex", gap: 2 }}>
                      <Button
                        className={classes.buttonSpacing}
                        variant="contained"
                        color="primary"
                        component="span"
                        onClick={this.handleEditClick}
                      >
                        Edit
                      </Button>
                      <Button
                        className={classes.buttonSpacing}
                        variant="contained"
                        color="primary"
                        component="span"
                        onClick={this.closePatientDetails}
                      >
                        Close
                      </Button>
                    </Box>
                  </div>
                )}
              </div>
            </div>
          </Paper>
        ) : null}
      </main>
    );
  }
}
export default withStyles(useStyles, { withTheme: true })(PatientTemplate);
