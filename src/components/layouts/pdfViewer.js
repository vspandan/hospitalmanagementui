import React, { useState, useEffect, useParams } from "react";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
} from "@mui/material";
import { Worker, Viewer } from "@react-pdf-viewer/core";
import "@react-pdf-viewer/core/lib/styles/index.css";
import { printPlugin } from "@react-pdf-viewer/print";
import "@react-pdf-viewer/print/lib/styles/index.css";
import DocumentService from "../services/Document";
import "../../print.css";
import printJS from "print-js";

export const PdfDialog = ({ open, onClose, type, id, header }) => {
  const [pdfData, setPdfData] = useState(null);
  const printPluginInstance = printPlugin();
  const viewerRef = React.useRef(null);
  useEffect(() => {
    if (open) {
      fetchPdfData();
    } else {
      setPdfData(null); // Reset PDF data when dialog is closed
    }
  }, [open]);

  const fetchPdfData = async () => {
    try {
      // Invoke the REST API to get the PDF
      const response = await new DocumentService().getReportPdf(
        id,
        type,
        header,
      );
      const blobUrl = URL.createObjectURL(response);
      setPdfData(blobUrl);
    } catch (error) {
      console.error("Error fetching PDF:", error);
    }
  };

  const handlePrint = () => {
    if (pdfData) {
      printJS({
        printable: pdfData,
        type: "pdf",
        showModal: true,
        style: `
                @page {
                    size: A4 portrait; /* Ensure portrait orientation */
                }
                body {
                    margin: 0; /* Remove body margins */
                }
                /* Additional styles for print */
                @media print {
                    * {
                        -webkit-print-color-adjust: exact; /* Preserve colors */
                        color-adjust: exact;
                    }
                }
            `,
      });
    } else {
      console.error("PDF data is not available.");
    }
  };

  return (
    <Dialog open={open} onClose={onClose} maxWidth="md" fullWidth>
      <DialogContent dividers style={{ height: "50", marginTop: "120px" }}>
        {pdfData ? (
          <Worker
            workerUrl={`https://unpkg.com/pdfjs-dist@2.16.105/build/pdf.worker.min.js`}
          >
            <div
              ref={viewerRef}
              className="pdf-print-container"
              style={{ height: "50%" }}
            >
              <Viewer fileUrl={pdfData} plugins={[printPluginInstance]} />
            </div>
          </Worker>
        ) : (
          <p>Loading PDF...</p>
        )}
      </DialogContent>
      <DialogActions>
        <Button onClick={handlePrint} color="primary">
          {" "}
          {/* Use custom print function */}
          Print
        </Button>
        <Button onClick={onClose} color="secondary">
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
};
