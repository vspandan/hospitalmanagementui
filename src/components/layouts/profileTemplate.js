import React from "react";
import Identity from "../services/Identity";
import {
  Paper,
  Typography,
  Grid,
  Divider,
  TableContainer,
  TableRow,
  Table,
  TableCell,
  TableBody,
  TableHead,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
} from "@mui/material";
import useStyles from "../constants/styles";
import { HashLoader } from "react-spinners";

const UserProfile = ({ user }) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const onClose = () => {
    setOldPassword("");
    setNewPassword("");
    setConfirmPassword("");
    setError("");
    setSuccess("");
    setOpen(false);
  };

  const openPasswordChangeDialog = () => {
    setOpen(true);
  };

  const [oldPassword, setOldPassword] = React.useState("");
  const [newPassword, setNewPassword] = React.useState("");
  const [confirmPassword, setConfirmPassword] = React.useState("");
  const [error, setError] = React.useState("");
  const [success, setSuccess] = React.useState("");

  const handleSubmit = () => {
    setLoading(true);
    setError("");
    setSuccess("");
    let data = {
      oldPassword: oldPassword,
      newPassword: newPassword,
      phone: user.loginPhoneNo,
    };
    let identityService = new Identity();
    identityService
      .changePassword(data)
      .then((res) => {
        if (res.data === true) {
          setOldPassword("");
          setNewPassword("");
          setConfirmPassword("");
          setError("");
          setSuccess("Password Changed Successfully!");
        } else {
          setError("Incorrect Old Password");
        }
        setLoading(false);
      })
      .catch((error) => {
        console.error(`Error updating test report`, error);
        setLoading(false);
      });
    // Validate password
    if (newPassword !== confirmPassword) {
      setError("New passwords don't match");
      setLoading(false);
      return;
    }
  };

  return (
    <div className={classes.content}>
      <Dialog open={open} onClose={onClose}>
        <DialogTitle>
          <Typography
            variant="h6"
            style={{
              fontWeight: "bold",
              padding: "8px 16px",
              borderBottom: "2px solid #f0f0f0",
              color: "#3f51b5",
              textAlign: "center",
            }}
          >
            Change Password
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Typography variant="body1" gutterBottom align="center">
            Please enter your old password and the new password below.
          </Typography>
          <TextField
            margin="normal"
            disabled={loading}
            fullWidth
            label="Old Password"
            type="password"
            variant="outlined"
            value={oldPassword}
            onChange={(e) => setOldPassword(e.target.value)}
          />
          <TextField
            margin="normal"
            fullWidth
            disabled={loading}
            label="New Password"
            type="password"
            variant="outlined"
            value={newPassword}
            onChange={(e) => setNewPassword(e.target.value)}
          />
          <TextField
            margin="normal"
            fullWidth
            disabled={loading}
            label="Confirm New Password"
            type="password"
            variant="outlined"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
          {error && (
            <Typography color="error" align="center">
              {error}
            </Typography>
          )}
          {success && (
            <Typography color="success" align="center">
              {success}
            </Typography>
          )}
          {loading && (
            <HashLoader
              align
              color={"#00ffff"}
              loading={true}
              size={50}
              aria-label="Loading Spinner"
              data-testid="loader"
            />
          )}
        </DialogContent>
        }
        <DialogActions>
          <Button onClick={onClose} color="secondary">
            Cancel
          </Button>
          <Button disabled={loading} onClick={handleSubmit} color="primary">
            Change Password
          </Button>
        </DialogActions>
      </Dialog>
      <Typography
        variant="h4"
        style={{
          fontWeight: "bold",
          padding: "8px 16px",
          borderBottom: "2px solid #f0f0f0",
          color: "#3f51b5",
          textAlign: "center",
        }}
      >
        User Profile
      </Typography>
      <Paper className={classes.paper}>
        <Grid container spacing={3} alignContent="center">
          <TableContainer>
            <Table className={classes.table}>
              <TableBody>
                <TableRow>
                  <TableCell className={classes.table_td}>
                    <b>First Name</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.firstName}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>Sur Name</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.lastName}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.table_td}>
                    <b>Gender</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.gender}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>Date of Birth</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>{user.dob}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.table_td}>
                    <b>Nationality</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.nationality}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>Citizenship</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.citizenShip}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.table_td}>
                    <b>Blood Group</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.bloodGroup}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>Rh Factor</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.rhFactor}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.table_td}>
                    <b>Aadhaar</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.aadhaar}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>PAN Card</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.panCard}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <Grid item xs={12}>
            <Typography
              variant="h6"
              style={{
                fontWeight: "bold",
                padding: "8px 16px",
                borderBottom: "2px solid #f0f0f0",
                color: "#3f51b5",
                textAlign: "center",
              }}
            >
              Address Details
            </Typography>
            <Table className={classes.mintable}>
              <TableBody>
                <TableRow>
                  <TableCell className={classes.table_td}>
                    <b>Contact Name *</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.contactName || ""}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>Contact Phone *</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.phoneNo || ""}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>Email Address</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.emailAddress || ""}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>Country *</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.country || ""}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.table_td}>
                    <b>Door No</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.doorNo || ""}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>Street Name</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.streetName || ""}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>Locality</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.locality || ""}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>LandMark</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.viaName || ""}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.table_td}>
                    <b>City *</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.city || ""}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>District</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.district || ""}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>State *</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.state || ""}
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    <b>Pin Code *</b>
                  </TableCell>
                  <TableCell className={classes.table_td}>
                    {user.primaryAddress?.pinCode || ""}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Grid>
          <Divider className={classes.divider} />
        </Grid>
        <div align="center">
          <br />
          <Button
            className={classes.buttonSpacing}
            variant="contained"
            color="primary"
            onClick={openPasswordChangeDialog}
          >
            Change Password
          </Button>
        </div>
      </Paper>
    </div>
  );
};

export default UserProfile;
