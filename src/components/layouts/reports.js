import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Paper,
  Typography,
  Button,
} from "@mui/material";
import { Redirect } from "react-router-dom";
import LabTests from "../services/LabTests";
import useStyles from "../constants/styles";
import { HashLoader } from "react-spinners";

class Reports extends Component {
  state = {
    reportLists: [],
    bid: null,
    loading: true,
    pageSize: 20,
    currentPage: 0,
    totalElements: 0,
    totalPages: 0,
  };

  setViewTemplateRedirect = (id) => () => {
    this.setState({
      viewTemplateRedirect: true,
      selectedTemplateId: id,
    });
  };

  renderViewTemplateRedirect = () => {
    if (this.state.viewTemplateRedirect) {
      let redirectUrl = "/test/" + this.state.selectedTemplateId;
      return <Redirect to={redirectUrl} />;
    }
  };

  componentDidMount() {
    let labtestService = new LabTests();
    this.setState({ loading: true });
    if (this.props && this.props.bid) {
      labtestService
        .getByBillId(this.props.bid)
        .then((res) =>
          this.setState({
            reportLists: res.data,
            bid: this.props.bid,
            loading: false,
          }),
        )
        .catch((error) => {
          console.error(
            `Error fetching lab tests for bill id ${this.props.bid}:`,
            error,
          );
          this.setState({ loading: false });
        });
    } else {
      this.fetchTestsData();
    }
  }

  fetchTestsData(page = 0, size = this.state.pageSize) {
    let labtestService = new LabTests();
    this.setState({ loading: true });

    let params = {
      page: page, // Current page number
      size: size, // Number of records per page
    };
    labtestService
      .getLabTestsList(params)
      .then((res) =>
        this.setState({
          reportLists: res.data.content,
          loading: false,
          totalElements: res.data.totalElements, // Store total number of elements
          totalPages: res.data.totalPages, // Store total number of pages
          currentPage: res.data.number,
        }),
      )
      .catch((error) => {
        console.error("Error fetching lab tests:", error);
        this.setState({ loading: false });
      });
  }

  renderPaginationControls() {
    const { totalPages, currentPage } = this.state;

    return (
      <div style={{ marginTop: "20px", textAlign: "center" }}>
        <Button
          disabled={currentPage === 0}
          onClick={() => this.handlePageChange(currentPage - 1)}
        >
          Previous
        </Button>
        <span style={{ margin: "0 10px" }}>
          Page {totalPages === 0 ? 0 : currentPage + 1} of {totalPages}
        </span>
        <Button
          disabled={currentPage + 1 === totalPages}
          onClick={() => this.handlePageChange(currentPage + 1)}
        >
          Next
        </Button>
      </div>
    );
  }

  handlePageChange(page) {
    this.fetchTestsData(page); // Fetch data for the new page
  }

  render() {
    const { classes } = this.props;
    return this.state.loading ? (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <HashLoader
          color={"#00ffff"}
          loading={true}
          size={50}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
      </div>
    ) : (
      <main className={classes.content}>
        <Typography
          variant="h4"
          style={{
            fontWeight: "bold",
            padding: "8px 16px",
            borderBottom: "2px solid #f0f0f0",
            color: "#3f51b5",
            textAlign: "center",
          }}
        >
          Lab Tests -{" "}
          {this.state.bid ? `(Bill Id - ${this.state.bid})` : "All Reports"}
        </Typography>
        <Paper className={classes.paper}>
          <TableContainer className={classes.container} size="small">
            <Table
              stickyHeader
              aria-label="sticky table"
              className={classes.table}
            >
              <TableHead>
                <TableRow>
                  <TableCell align="center" className={classes.tableHeadCell}>
                    <b>Sl.No</b>
                  </TableCell>
                  <TableCell align="center" className={classes.tableHeadCell}>
                    <b>Patient Name</b>
                  </TableCell>
                  <TableCell align="center" className={classes.tableHeadCell}>
                    <b>Test Id</b>
                  </TableCell>
                  <TableCell align="center" className={classes.tableHeadCell}>
                    <b>Test Name</b>
                  </TableCell>
                  <TableCell align="center" className={classes.tableHeadCell}>
                    <b> Bill Id</b>
                  </TableCell>
                  <TableCell align="center" className={classes.tableHeadCell}>
                    <b>Bill Status</b>
                  </TableCell>
                  <TableCell align="center" className={classes.tableHeadCell}>
                    <b>Test Status</b>
                  </TableCell>
                  <TableCell align="center" className={classes.tableHeadCell}>
                    <b> Requested Time</b>
                  </TableCell>
                  <TableCell align="center" className={classes.tableHeadCell}>
                    <b> Report Generated Time</b>
                  </TableCell>
                </TableRow>
              </TableHead>
              {this.state.reportLists &&
                this.state.reportLists.map((field, index) => (
                  <TableRow
                    hover
                    role="checkbox"
                    key={index}
                    tabIndex={index}
                    onClick={this.setViewTemplateRedirect(field.id)}
                  >
                    {this.renderViewTemplateRedirect()}
                    <TableCell align="center">{index + 1}</TableCell>
                    <TableCell align="center">
                      {field.patient.firstName}{" "}
                      {field.patient.lastName ? field.patient.lastName : ""}
                    </TableCell>
                    <TableCell align="center">{field.testCode}</TableCell>
                    <TableCell align="center">{field.testName}</TableCell>
                    <TableCell align="center">{field.billId}</TableCell>
                    <TableCell align="center">{field.billStatus}</TableCell>
                    <TableCell align="center">{field.status}</TableCell>
                    <TableCell align="center">{field.createdAt}</TableCell>
                    <TableCell align="center">{field.modifiedAt}</TableCell>
                  </TableRow>
                ))}
            </Table>
          </TableContainer>
          {/* Pagination Controls */}
          {this.renderPaginationControls()}
        </Paper>
      </main>
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(Reports);
