import React, { useState } from "react";
import Identity from "../services/Identity";
import {
  Button,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Typography,
  Paper,
  Grid,
} from "@mui/material";
import useStyles from "../constants/styles"; // Adjust the path as necessary
import { HashLoader } from "react-spinners";

const UserForm = () => {
  const classes = useStyles();
  const [data, setData] = useState({
    firstName: "",
    lastName: "",
    loginPhoneNo: "",
    type: "STAFF",
    specialization: "",
    licenseNumber: "",
    aadhaar: "",
    gender: "MALE",
  });
  const [gender, setGender] = useState(["MALE", "FEMALE", "TRANS"]);
  const [errors, setErrors] = useState({});
  const [loading, setLoading] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();
    const identityService = new Identity();

    // Check if any errors exist before submitting
    const hasErrors = Object.values(errors).some((error) => error);
    if (hasErrors) {
      return; // Prevent submission if there are errors
    }

    setLoading(true);
    identityService
      .registerUser(data)
      .then((res) => {
        setLoading(false);
        if (res.data === true) {
          alert("Successfully Registered User");
        } else {
          alert("Failed User Registration");
        }
      })
      .catch((error) => {
        console.error(`User Registration failed`, error);
        setLoading(false);
      });
    setData({
      firstName: "",
      lastName: "",
      loginPhoneNo: "",
      type: "STAFF",
      specialization: "",
      licenseNumber: "",
      aadhaar: "",
    });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setData({ ...data, [name]: value }); // Use setData to update state

    // Validate inputs on change
    validateInput(name, value);
  };

  const validateInput = (name, value) => {
    let errorMessage = "";

    switch (name) {
      case "firstName":
        if (!value.match(/^[A-Za-z]+$/)) {
          errorMessage = "First name must contain only letters.";
        }
        break;
      case "lastName":
        if (!value.match(/^[A-Za-z]+$/)) {
          errorMessage = "Surname must contain only letters.";
        }
        break;
      case "loginPhoneNo":
        if (value.length !== 10 || isNaN(value)) {
          errorMessage = "Phone number must be a 10-digit number.";
        }
        break;
      case "aadhaar":
        if (!value.match(/^\d{12}$/)) {
          errorMessage = "Aadhaar must be a 12-digit number.";
        }
        break;
      default:
        break;
    }

    setErrors({ ...errors, [name]: errorMessage });
  };

  return loading ? (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
      }}
    >
      <HashLoader
        color={"#00ffff"}
        loading={true}
        size={50}
        aria-label="Loading Spinner"
        data-testid="loader"
      />
    </div>
  ) : (
    <Paper elevation={3} className={classes.paper}>
      <Typography
        variant="h4"
        style={{
          fontWeight: "bold",
          padding: "8px 16px",
          borderBottom: "2px solid #f0f0f0",
          color: "#3f51b5",
          textAlign: "center",
        }}
      >
        Add New User
      </Typography>

      <form onSubmit={handleSubmit} className={classes.formAddUser}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <FormControl required fullWidth className={classes.textField}>
              <InputLabel>User Type</InputLabel>
              <Select name="type" value={data.type} onChange={handleChange}>
                <MenuItem value="ADMINISTRATOR">ADMINISTRATOR</MenuItem>
                <MenuItem value="STAFF">USER</MenuItem>
                <MenuItem value="DOCTOR">DOCTOR</MenuItem>
                <MenuItem value="REFERRER">REFERRER</MenuItem>
              </Select>
            </FormControl>
          </Grid>

          <Grid item xs={12}>
            <TextField
              label="First Name"
              name="firstName"
              value={data.firstName}
              onChange={handleChange}
              required
              fullWidth
              error={!!errors.firstName} // Indicate error state
              helperText={errors.firstName} // Show error message
              className={classes.firstName}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              label="SurName"
              name="lastName"
              value={data.lastName}
              onChange={handleChange}
              required
              fullWidth
              error={!!errors.lastName} // Indicate error state
              helperText={errors.lastName} // Show error message
              className={classes.textField}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              label="Login Phone No."
              name="loginPhoneNo"
              type="number"
              value={data.loginPhoneNo}
              onChange={handleChange}
              required
              fullWidth
              error={!!errors.loginPhoneNo} // Indicate error state
              helperText={errors.loginPhoneNo} // Show error message
              className={classes.textField}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              label="Aadhaar No."
              name="aadhaar"
              type="number"
              value={data.aadhaar}
              onChange={handleChange}
              required
              fullWidth
              error={!!errors.aadhaar} // Indicate error state
              helperText={errors.aadhaar} // Show error message
              className={classes.textField}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              label="Gender"
              name="gender"
              fullWidth
              value={data.gender}
              onChange={handleChange}
              select
              variant="outlined"
              margin="dense"
            >
              {gender.map((status) => (
                <MenuItem key={status} value={status}>
                  {status}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          {data.type === "DOCTOR" && (
            <>
              <Grid item xs={12}>
                <TextField
                  label="Specialization"
                  name="specialization"
                  value={data.specialization}
                  onChange={handleChange}
                  className={classes.textField}
                  required
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  label="License Number"
                  name="licenseNumber"
                  value={data.licenseNumber}
                  onChange={handleChange}
                  className={classes.textField}
                  required
                  fullWidth
                />
              </Grid>
            </>
          )}
          <Grid item xs={12} className={classes.buttonWrapper}>
            <Button
              className={classes.buttonSpacing}
              variant="contained"
              color="primary"
              type="submit"
            >
              Add User
            </Button>
          </Grid>
        </Grid>
      </form>
    </Paper>
  );
};

export default UserForm;
