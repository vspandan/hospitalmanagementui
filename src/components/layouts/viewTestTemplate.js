import React, { Component } from "react";
import Paper from "@mui/material/Paper";
import { withStyles } from "@mui/styles";
import {
  TextField,
  TableContainer,
  Table,
  TableHead,
  TableBody, // Import TableBody
  TableRow,
  TableCell,
  Button,
  Grid,
} from "@mui/material";
import Typography from "@mui/material/Typography";
import { Redirect } from "react-router-dom";
import useStyles from "../constants/styles";
import MasterTestReport from "../services/MasterTestReport";
import { HashLoader } from "react-spinners";

class ViewTestReport extends Component {
  state = {
    id: null,
    reportName: null,
    inputFields: [],
    derivedFields: [],
    redirect: false,
    price: 0,
    loading: true,
  };

  componentDidMount() {
    if (this.props.id) {
      const id = this.props.id;
      let masterTestReportService = new MasterTestReport();
      masterTestReportService
        .getMasterReport(id)
        .then((res) =>
          this.setState({
            id: id,
            reportName: res.data.reportName,
            inputFields: res.data.inputFields,
            derivedFields: res.data.derivedFields,
            price: res.data.price,
            loading: false,
          }),
        )
        .catch((err) => {
          console.error("Error fetching report template:", err);
          this.setState({ loading: false });
        });
    }
  }

  setRedirect = () => {
    this.setState({ redirect: true });
  };

  onClose = () => {
    window.location.href = "/lab/templates";
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to={`/lab/template/${this.state.id}/edit`} />;
    }
  };

  findFieldName(str) {
    var f = "";
    this.state.inputFields.map((field, idx) => {
      if (field.id === str) {
        f = field.name;
      }
    });
    return f === "" ? str : f;
  }

  renderReferences = (type, references) =>
    references.map((ref, idx) => {
      if (ref.gender === "NOT_SPECIFIED") {
        ref.gender = "";
      }
      return (
        <div key={idx} align="left">
          {type === "RANGE" ? (
            <div>{`${ref.minValue} - ${ref.maxValue}`}</div>
          ) : type === "GREATER_THAN" ? (
            <div>{`> ${ref.minValue}`}</div>
          ) : type === "LESS_THAN" ? (
            <div>{`< ${ref.maxValue}`}</div>
          ) : (
            <div>{`POSITIVE/NEGATIVE`}</div>
          )}
          {type != "POLARITY" && ref.minAge && ref.maxAge ? (
            <div>{`${ref.gender} (Age : ${ref.minAge} - ${ref.maxAge})`}</div>
          ) : (
            <div></div>
          )}
        </div>
      );
    });

  render() {
    const { classes } = this.props;
    const { inputFields, derivedFields, price, reportName } = this.state;

    return this.state.loading ? (
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100vh",
        }}
      >
        <HashLoader
          color={"#00ffff"}
          loading={true}
          size={50}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
      </div>
    ) : (
      <main className={classes.content}>
        <Typography
          variant="h4"
          style={{
            fontWeight: "bold",
            padding: "8px 16px",
            borderBottom: "2px solid #f0f0f0",
            color: "#3f51b5",
            textAlign: "center",
          }}
        >
          Report Template : {reportName}
        </Typography>
        <br />
        <Paper className={classes.paper}>
          <TableContainer>
            <Table stickyHeader className={classes.table}>
              <TableHead>
                <TableRow>
                  <TableCell align="center">
                    <b>Field Id</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Field Name</b>
                  </TableCell>
                  <TableCell align="center">
                    <b>Field Type</b>
                  </TableCell>
                  <TableCell align="left">
                    <b>Reference Range</b>
                  </TableCell>
                  <TableCell align="left">
                    <b>Reference Unit</b>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {" "}
                {/* Use TableBody instead of tbody */}
                {inputFields.map((field, index) => (
                  <TableRow key={index} hover role="checkbox">
                    <TableCell>{field.code}</TableCell>
                    <TableCell align="center">{field.name}</TableCell>
                    <TableCell align="center">{field.dataType}</TableCell>
                    <TableCell align="center">
                      {this.renderReferences(field.dataType, field.references)}
                    </TableCell>
                    <TableCell align="left">{field.referenceUnit}</TableCell>
                  </TableRow>
                ))}
                {derivedFields.map((field, index) => (
                  <TableRow key={index} hover role="checkbox">
                    <TableCell>{field.code}</TableCell>
                    <TableCell align="center">{field.name}</TableCell>
                    <TableCell align="center">{field.type}</TableCell>
                    <TableCell align="left">
                      <b>Formulae:</b>{" "}
                      {field.formulaeTokens
                        .map((token) => this.findFieldName(token))
                        .join(" ")}
                    </TableCell>
                    <TableCell align="left">{field.referenceUnit}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
        <br />
        <div align="right">
          <TextField
            disabled
            margin="dense"
            variant="outlined"
            label="Base Price"
            value={price}
          />
        </div>
        <Grid
          container
          justifyContent="flex-end"
          className={classes.tableElements}
          spacing={2}
        >
          {this.renderRedirect()}
          <Grid item>
            <Button
              className={classes.buttonSpacing}
              variant="contained"
              color="primary"
              onClick={this.setRedirect}
            >
              Edit
            </Button>
          </Grid>
          <Grid item>
            <Button
              className={classes.buttonSpacing}
              variant="contained"
              sx={{ backgroundColor: "black", color: "white" }}
              onClick={this.onClose}
            >
              Close
            </Button>
          </Grid>
        </Grid>
      </main>
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(ViewTestReport);
