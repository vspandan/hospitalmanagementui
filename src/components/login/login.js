import React, { Component } from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import PowerIcon from "@mui/icons-material/Power";
import Typography from "@mui/material/Typography";
import { withStyles } from "@mui/styles";
import Copyright from "../copyright/copyright";
import Storage from "../storage/storage";
import { Redirect } from "react-router-dom";
import Identity from "../services/Identity";
import logo from "../assets/logo.png";
import { HashLoader } from "react-spinners";
import { Table, TableCell, TableRow } from "@mui/material";
import Config from "../services/Config";

const override = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  height: "100vh",
};

const styles = (theme) => ({
  root: {
    height: "100vh",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  containers: {
    height: "50vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundImage: `url(${logo})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "contain",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: theme.spacing(4),
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[3],
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: "100%",
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    backgroundColor: theme.palette.primary.main,
    "&:hover": {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  forgotPassword: {
    marginTop: theme.spacing(2),
    textAlign: "center",
  },
  checkbox: {
    margin: theme.spacing(1, 0),
  },
  tagLine: {
    margin: "30px auto",
    width: "80%",
    textAlign: "center",
    fontSize: "1.5rem",
    lineHeight: "1.8",
    padding: "10px",
    color: "#2c3e50",
    backgroundColor: "#ecf0f1",
    borderRadius: "10px",
    boxShadow: "0 4px 8px rgba(0, 0, 0, 0.1)",
    border: "1px solid #bdc3c7",
  },
});

class Login extends Component {
  state = {
    redirect: false,
    phone: "",
    password: "",
    error: false,
    loading: true,
    isActivated: false,
    activationCode: "",
    cpuId: "",
    mac: "",
    message: "",
  };

  activate = () => {
    this.setState({
      loading: true,
      message: "Preloading data post activation",
    });
    const identityService = new Identity();
    identityService
      .activate(this.state.activationCode)
      .then((res) => {
        if (res && res.data === true) {
          alert("Successfully Activated");
          this.setState({ isActivated: true, loading: false, message: "" });
        } else {
          this.setState({ loading: false, message: "" });
        }
      })
      .catch((error) => {
        console.error(`Error Activation :`, error);
        this.setState({ loading: false });
      });
  };
  setRedirect = () => {
    this.setState({ loading: true });
    const identityService = new Identity();
    const data = { phoneNo: this.state.phone, password: this.state.password };

    identityService
      .signIn(data)
      .then(async (res) => {
        if (res && res.data) {
          Storage.set("loggedIn", true);
          Storage.set("userId", res.data.userId);
          Storage.set("userName", res.data.userName);
          Storage.set("accessToken", res.data.accessToken);
          Storage.set("expires", res.data.expires);
          Storage.set("validFrom", res.data.validFrom);
          Storage.set("isAdmin", res.data.admin);
          Storage.set("isSuperAdmin", res.data.superAdmin);
          var identityService1 = new Identity();
          await identityService1
            .getDetailedUserDetails(res.data.userId)
            .then(async (res1) => {
              if (res1 && res1.data) {
                Storage.set("user", JSON.stringify(res1.data));
                let configService = new Config();
                await configService.getConfigSettings().then((res) => {
                  if (res && res.data) {
                    Storage.set("logo", res.data.logoPath);
                    Storage.set("appName", res.data.appName);
                  }
                });
                this.setState({ redirect: true, loading: false });
              } else {
                this.setState({ redirect: false, loading: false });
              }
            })
            .catch((error) => {
              console.error(`Error fetching Profile information:`, error);
              this.setState({ redirect: false, loading: false });
            });
        } else if (res && res.status == "UNAUTHORIZED") {
          alert("Login Failure: " + res.message);
          this.setState({ redirect: false, loading: false });
        } else {
          this.setState({ redirect: false, loading: false });
        }
      })
      .catch(() => {
        this.setState({ redirect: false, loading: false });
      });
  };

  handleTextFieldChanges = (attr) => (event) => {
    let err = false;
    if (
      attr === "phone" &&
      (event.target.value.length !== 10 || isNaN(event.target.value))
    ) {
      err = true;
    }
    this.setState({ [attr]: event.target.value, error: err });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/dashboard" />;
    }
  };

  componentDidMount() {
    const isLoggedIn =
      Storage.get("loggedIn") === "true" &&
      Storage.get("accessToken") !== "null" &&
      Date.now() < Storage.get("expires") + Storage.get("validFrom");
    if (isLoggedIn) {
      window.location.replace("/dashboard");
    } else {
      const identityService = new Identity();
      identityService
        .isActivated()
        .then((res) => {
          if (res && res.data !== true) {
            var d = res.data;
            this.setState({
              isActivated: d["isActivated"],
              cpuId: d["CPU"],
              mac: d["MAC"],
              loading: false,
            });
          }
        })
        .catch((error) => {
          console.error(`Error fetching activation information: `, error);
          this.setState({ loading: false });
        });
    }
  }

  render() {
    const { classes } = this.props;
    const { phone, error, loading } = this.state;

    return loading ? (
      <div style={override}>
        <HashLoader
          color={"#00ffff"}
          loading={true}
          size={50}
          aria-label="Loading Spinner"
          data-testid="loader"
        />
        <div>{this.state.message}</div>
      </div>
    ) : (
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={12} sm={6} md={7} component={Paper} elevation={0} square>
          <div className={classes.containers} />
          <div className={classes.tagLine}>
            <strong style={{ color: "#2980b9" }}>
              Simplifying hospital operations and elevating patient care with{" "}
              <span style={{ color: "#e74c3c" }}>
                seamless, technology-powered solutions
              </span>{" "}
              for modern healthcare.
            </strong>
          </div>
        </Grid>
        {this.state.isActivated ? (
          <Grid
            item
            xs={12}
            sm={6}
            md={4}
            component={Paper}
            elevation={0}
            square
          >
            <div className={classes.paper}>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Sign in
              </Typography>
              <form className={classes.form} noValidate>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  id="phone"
                  label="Phone"
                  name="phone"
                  value={phone}
                  autoComplete="phone"
                  autoFocus
                  type="number"
                  inputProps={{ maxLength: 10 }}
                  error={error}
                  helperText={
                    error ? "Phone number must be exactly 10 digits." : ""
                  }
                  onChange={(e) => {
                    const { value } = e.target;
                    // Enforcing min/max length manually (optional)
                    if (
                      value.length <= 10 &&
                      !isNaN(value) &&
                      Number.isInteger(Number(value))
                    ) {
                      this.handleTextFieldChanges("phone")(e);
                    }
                  }}
                  onKeyPress={(ev) => {
                    if (ev.key === "Enter") {
                      this.setRedirect();
                      ev.preventDefault();
                    }
                  }}
                  onKeyDown={(ev) => {
                    if (ev.key === "ArrowUp" || ev.key === "ArrowDown") {
                      ev.preventDefault(); // Prevent browser default increment/decrement
                      let numValue = Number(phone) || "";

                      if (ev.key === "ArrowUp" && phone.length === 10) {
                        numValue = Math.min(numValue + 1, 9999999999); // Prevent exceeding max 10-digit number
                      } else if (
                        ev.key === "ArrowDown" &&
                        phone.length === 10
                      ) {
                        numValue = Math.max(numValue - 1, 1000000000); // Prevent dropping below 10-digit range
                      }

                      this.handleTextFieldChanges("phone")({
                        target: { value: numValue.toString() },
                      });
                    }
                  }}
                />
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  autoComplete="current-password"
                  onChange={this.handleTextFieldChanges("password")}
                  onKeyPress={(ev) => {
                    if (ev.key === "Enter") {
                      this.setRedirect();
                      ev.preventDefault();
                    }
                  }}
                />
                <FormControlLabel
                  control={
                    <Checkbox color="primary" className={classes.checkbox} />
                  }
                  label="Remember me"
                />
                {this.renderRedirect()}
                <Button
                  fullWidth
                  variant="contained"
                  className={classes.submit}
                  onClick={this.setRedirect}
                >
                  Sign In
                </Button>
                <Box className={classes.forgotPassword}>
                  <Typography variant="body2" color="textSecondary">
                    <a
                      href="#"
                      style={{ textDecoration: "none", color: "inherit" }}
                    >
                      Forgot password?
                    </a>
                  </Typography>
                </Box>
                <Box mt={5}>
                  <Copyright />
                </Box>
              </form>
            </div>
          </Grid>
        ) : (
          <Grid
            item
            xs={12}
            sm={6}
            md={4}
            component={Paper}
            elevation={0}
            square
          >
            <div className={classes.paper}>
              <Avatar className={classes.avatar}>
                <PowerIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Activation
              </Typography>
              <Table>
                <TableRow>
                  <TableCell align="center" className={classes.tableHeader}>
                    <b>Hardware Id</b>
                  </TableCell>
                  <TableCell align="center" className={classes.tableHeader}>
                    <b>{this.state.mac}</b>
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="center" className={classes.tableHeader}>
                    <b>CPU ID</b>
                  </TableCell>
                  <TableCell align="center" className={classes.tableHeader}>
                    <b>{this.state.cpuId}</b>
                  </TableCell>
                </TableRow>
              </Table>
              <form className={classes.form} noValidate>
                <TextField
                  variant="outlined"
                  margin="normal"
                  required
                  fullWidth
                  name="Activation Code"
                  label="Activation Code"
                  id="activationCode"
                  autoComplete="current-password"
                  onChange={this.handleTextFieldChanges("activationCode")}
                  onKeyPress={(ev) => {
                    if (ev.key === "Enter") {
                      this.setRedirect();
                      ev.preventDefault();
                    }
                  }}
                />
                {this.renderRedirect()}
                <Button
                  fullWidth
                  variant="contained"
                  className={classes.submit}
                  onClick={this.activate}
                >
                  Activate
                </Button>
                <Box mt={5}>
                  <Copyright />
                </Box>
              </form>
            </div>
          </Grid>
        )}
      </Grid>
    );
  }
}

export default withStyles(styles)(Login);
