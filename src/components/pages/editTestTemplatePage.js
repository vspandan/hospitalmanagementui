import React from "react";
import { makeStyles } from "@mui/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import EditTestReport from "../layouts/editTestTemplate";
import Copyright from "../copyright/copyright";
import Template from "../template/template";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    height: "100vh", // Full viewport height for the layout
    overflow: "hidden", // Prevent overflow on the main container
  },
  contentWrapper: {
    marginTop: "150px",
    overflow: "hidden", // Prevent overflowing the page
    flexGrow: 1,
    display: "flex",
    flexDirection: "column",
    overflowY: "auto", // This will make the Paper content scrollable
    padding: theme.spacing(2),
  },
  paper: {
    padding: theme.spacing(3),
    display: "flex",
    flexDirection: "column",
    borderRadius: theme.shape.borderRadius,
    width: "100%",
    flexGrow: 1,
  },
  templateWrapper: {
    position: "fixed", // Fix the Template toolbar at the top
    top: 0,
    left: 0,
    right: 0,
    zIndex: 2000, // Ensure it stays above other content
  },
}));

export default function EditTestTemplate() {
  const classes = useStyles();
  const { id } = useParams();

  return (
    <div className={classes.root}>
      {/* Fix the Template Toolbar */}
      <div className={classes.templateWrapper}>
        <Template /> {/* Includes AppBar and SideMenu */}
      </div>

      {/* Main content area with scrollable Paper */}
      <div className={classes.contentWrapper}>
        <div className={classes.paperWrapper}>
          <Paper className={classes.paper} elevation={5}>
            <EditTestReport id={id} />
          </Paper>
        </div>
      </div>

      {/* Copyright fixed at the bottom */}
      <Box pt={4}>
        <Copyright />
      </Box>
    </div>
  );
}
