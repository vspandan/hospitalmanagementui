export const getApiBaseUrl = () => {
  return new Promise((resolve, reject) => {
    const RTCPeerConnection =
      window.RTCPeerConnection ||
      window.webkitRTCPeerConnection ||
      window.mozRTCPeerConnection;
    const pc = new RTCPeerConnection({ iceServers: [] });

    pc.createDataChannel(""); // Create a fake data channel to trigger ICE gathering
    pc.createOffer()
      .then((offer) => pc.setLocalDescription(offer))
      .catch(reject);

    pc.onicecandidate = (event) => {
      if (event.candidate) {
        const candidate = event.candidate.candidate;
        const ipMatch = candidate.match(/([0-9]{1,3}\.){3}[0-9]{1,3}/);
        if (ipMatch) {
          resolve(ipMatch[0]);
          pc.close();
        }
      }
    };

    pc.onerror = reject;
  });
};
