import q from "q";
import Server from "./Server";
import Storage from "../storage/storage";

export default class Appointment {
  constructor() {
    this.api = new Server();
  }

  scheduleAppointment(body) {
    let deferred = q.defer();
    this.api
      .post(`/api/appointments/schedule`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  fetchAppointmentDetails(billId) {
    let deferred = q.defer();
    this.api
      .get(`/api/appointments/bill/` + billId)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  getAppointmentList(params) {
    let deferred = q.defer();
    this.api
      .get(`/api/appointments/list`, params)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  isActivated() {
    let deferred = q.defer();
    this.api
      .get(`/api/license/status`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  activate(data) {
    let deferred = q.defer();
    let encodedValue = encodeURIComponent(data);
    this.api
      .post(`/api/license/activate?license=${encodedValue}`, {})
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  signOut(id, body) {
    let deferred = q.defer();
    this.api
      .post(`/api/identity/sign_out/${id}`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  register(body) {
    let deferred = q.defer();
    this.api
      .post(`/api/identity/register`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  registerUser(body) {
    let deferred = q.defer();
    this.api
      .post(`/api/identity/register/user`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  resetPassword(body) {
    let deferred = q.defer();
    this.api
      .put(`/api/identity/password/reset?loginPhoneNo=${body}`, {})
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  handleError(res, promise) {
    if (!res.response) {
      return 0;
    }
    console.log("Error - ", res.response.data.message);
    if (res.response.status === 419) {
      Storage.set("loggedIn", false);
      Storage.set("userId", null);
      Storage.set("userName", null);
      Storage.set("accessToken", null);
    } else if (res.response.status === 401) {
      alert("Authorization Failed: " + res.response.data.message);
    } else if (res.response.status >= 400 && res.response.status <= 499) {
      alert("Error: " + res.response.data.message);
    } else if (res.response.status === 503) {
      alert("Error Server is down. Please try again later.");
    } else if (res.response.status >= 500) {
      alert("Internal Server Error. Please try again later.");
    }
    promise.reject(res);
  }
}
