import q from "q";
import Server from "./Server";
import Storage from "../storage/storage";

export default class Billing {
  constructor() {
    this.api = new Server();
  }

  postEntity(body) {
    let deferred = q.defer();
    this.api
      .post(`/api/billing`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  autoSearch(input) {
    let deferred = q.defer();
    this.api
      .get(`api/billing/autosearch?input=${input}`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  putEntity(body) {
    let deferred = q.defer();
    this.api
      .put(`/api/billing`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  putDateEntity(body) {
    let deferred = q.defer();
    this.api
      .put(`/api/billing/date`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  deleteEntity(id) {
    let deferred = q.defer();
    this.api
      .delete(`/api/billing?id=${id}`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  getBillingList(params) {
    let deferred = q.defer();
    this.api
      .get(`/api/billing/list`, params)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));

    return deferred.promise;
  }

  getEntity(id) {
    let deferred = q.defer();
    this.api
      .get(`/api/billing?id=${id}`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  handleError(res, promise) {
    if (!res.response) {
      return 0;
    }
    console.log("Error - ", res.response.data.message);
    promise.reject(res);
    if (res.response.status === 419) {
      Storage.set("loggedIn", false);
      Storage.set("userId", null);
      Storage.set("userName", null);
      Storage.set("accessToken", null);
    } else if (res.response.status === 401) {
      alert("Authorization Failed: " + res.response.data.message);
    } else if (res.response.status >= 400 && res.response.status <= 499) {
      alert("Error: " + res.response.data.message);
    } else if (res.response.status === 503) {
      alert("Error Server is down. Please try again later.");
    } else if (res.response.status >= 500) {
      alert("Internal Server Error. Please try again later.");
    }
  }
}
