import q from "q";
import Server from "./Server";
import Storage from "../storage/storage";

export default class Config {
  constructor() {
    this.api = new Server();
  }

  getConfigs() {
    let deferred = q.defer();
    this.api
      .get(`config`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  postToken(token) {
    let deferred = q.defer();
    this.api
      .post(`config/googleauth`, token)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  getConfigSettings() {
    let deferred = q.defer();
    this.api
      .get(`config/settings`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  saveConfigProperties(data) {
    var options = { headers: { "Content-Type": "multipart/form-data" } };
    let deferred = q.defer();
    this.api
      .post(`config/settings`, data, options)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  handleError(res, promise) {
    if (!res.response) {
      return 0;
    }
    console.log("Error - ", res.response.data.message);
    if (res.response.status === 419) {
      Storage.set("loggedIn", false);
      Storage.set("userId", null);
      Storage.set("userName", null);
      Storage.set("accessToken", null);
    } else if (res.response.status === 401) {
      alert("Authorization Failed: " + res.response.data.message);
    } else if (res.response.status >= 400 && res.response.status <= 499) {
      alert("Error: " + res.response.data.message);
    } else if (res.response.status === 503) {
      alert("Error Server is down. Please try again later.");
    } else if (res.response.status >= 500) {
      alert("Internal Server Error. Please try again later.");
    }
    promise.reject(res);
  }
}
