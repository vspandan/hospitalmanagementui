import q from "q";
import Server from "./Server";
import Storage from "../storage/storage";

export default class Billing {
  constructor() {
    this.api = new Server();
  }

  fetchAllDoctors() {
    let deferred = q.defer();
    this.api
      .get(`/api/identity/doctor/list`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  fetchVitals(appointmentId) {
    let deferred = q.defer();
    this.api
      .get(`/api/identity/user/vitals/` + appointmentId)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  saveVitals(body) {
    let deferred = q.defer();
    this.api
      .post(`/api/identity/user/vitals`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  patientAutoSearch(input) {
    let deferred = q.defer();
    this.api
      .get(`/api/identity/patient/autosearch?input=${input}`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  userAutoSearch(input) {
    let deferred = q.defer();
    this.api
      .get(`/api/identity/user/autosearch?input=${input}`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  getPatientDetails(id) {
    let deferred = q.defer();
    this.api
      .get(`/api/identity/patient?userId=${id}`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  updatePatientDetails(body) {
    let deferred = q.defer();
    this.api
      .put(`/api/identity/user/detailed`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  updateDoctorDetails(body) {
    let deferred = q.defer();
    this.api
      .put(`/api/identity/doctor`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  changePassword(body) {
    let deferred = q.defer();
    this.api
      .put(`/api/identity/password`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  getDetailedUserDetails(id) {
    let deferred = q.defer();
    this.api
      .get(`/api/identity/user/detailed?userId=${id}`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  doctorAutoSearch(input) {
    let deferred = q.defer();
    this.api
      .get(`/api/identity/doctor/autosearch?input=${input}`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  referrerAutoSearch(input) {
    let deferred = q.defer();
    this.api
      .get(`/api/identity/referrer/autosearch?input=${input}`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  signIn(body) {
    let deferred = q.defer();
    this.api
      .post(`/api/identity/sign_in`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  isActivated() {
    let deferred = q.defer();
    this.api
      .get(`/api/license/status`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  activate(data) {
    let deferred = q.defer();
    let encodedValue = encodeURIComponent(data);
    this.api
      .post(`/api/license/activate?license=${encodedValue}`, {})
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  signOut(id, body) {
    let deferred = q.defer();
    this.api
      .post(`/api/identity/sign_out/${id}`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  register(body) {
    let deferred = q.defer();
    this.api
      .post(`/api/identity/register`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  registerUser(body) {
    let deferred = q.defer();
    this.api
      .post(`/api/identity/register/user`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  resetPassword(body) {
    let deferred = q.defer();
    this.api
      .put(`/api/identity/password/reset?loginPhoneNo=${body}`, {})
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  handleError(res, promise) {
    if (!res.response) {
      return 0;
    }
    console.log("Error - ", res.response.data.message);
    if (res.response.status === 419) {
      Storage.set("loggedIn", false);
      Storage.set("userId", null);
      Storage.set("userName", null);
      Storage.set("accessToken", null);
    } else if (res.response.status === 401) {
      alert("Authorization Failed: " + res.response.data.message);
    } else if (res.response.status >= 400 && res.response.status <= 499) {
      alert("Error: " + res.response.data.message);
    } else if (res.response.status === 503) {
      alert("Error Server is down. Please try again later.");
    } else if (res.response.status >= 500) {
      alert("Internal Server Error. Please try again later.");
    }
    promise.reject(res);
  }
}
