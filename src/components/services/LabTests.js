import q from "q";
import Server from "./Server";
import Storage from "../storage/storage";

export default class Billing {
  constructor() {
    this.api = new Server();
  }

  getLabTestsList(params) {
    let deferred = q.defer();
    this.api
      .get(`/api/labmanagement/tests/list`, params)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  putEntity(body) {
    let deferred = q.defer();
    this.api
      .put(`/api/labmanagement/tests`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  putEntityDates(body) {
    let deferred = q.defer();
    this.api
      .put(`/api/labmanagement/tests/date`, body)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  getEntity(id) {
    let deferred = q.defer();
    this.api
      .get(`/api/labmanagement/tests?id=${id}`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  getByBillId(id) {
    let deferred = q.defer();
    this.api
      .get(`/api/labmanagement/tests/bill?id=${id}`)
      .then((res) => deferred.resolve(res))
      .catch((res) => this.handleError(res, deferred));
    return deferred.promise;
  }

  handleError(res, promise) {
    if (!res.response) {
      return 0;
    }
    console.log("Error - ", res.response.data.message);
    if (res.response.status === 419) {
      Storage.set("loggedIn", false);
      Storage.set("userId", null);
      Storage.set("userName", null);
      Storage.set("accessToken", null);
    } else if (res.response.status === 401) {
      alert("Authorization Failed: " + res.response.data.message);
    } else if (res.response.status >= 400 && res.response.status <= 499) {
      alert("Error: " + res.response.data.message);
    } else if (res.response.status === 503) {
      alert("Error Server is down. Please try again later.");
    } else if (res.response.status >= 500) {
      alert("Internal Server Error. Please try again later.");
    }
    promise.reject(res);
  }
}
