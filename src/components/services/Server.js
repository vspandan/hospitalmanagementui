import Qs from "qs";
import axios from "axios";
import q from "q";

import Storage from "../storage/storage";
import { getApiBaseUrl } from "./ApiBaseUrl";

export default class Server {
  constructor() {
    const url = `http://${window.location.hostname}:8080`;
    this.instance = axios.create({
      baseURL: url,
      headers: {
        "Content-Type": "application/json",
        "access-token": Storage.get("accessToken"),
      },
      paramsSerializer: function (params) {
        return Qs.stringify(params, { arrayFormat: "brackets" });
      },
    });
    this.updateToken = this.updateToken.bind(this);
  }

  get(url, params) {
    let deferred = q.defer();
    this.instance
      .get(url, {
        params,
      })
      .then((res) => deferred.resolve(res.data))
      .catch((res) => this.handleError(res, deferred));

    return deferred.promise;
  }

  getByConfig(url, params) {
    let deferred = q.defer();
    this.instance
      .get(url, params)
      .then((res) => deferred.resolve(res.data))
      .catch((res) => this.handleError(res, deferred));

    return deferred.promise;
  }

  post(url, data, options) {
    let deferred = q.defer();
    if (!data) {
      data = JSON.parse("{}");
    }
    this.instance
      .post(url, data, options)
      .then((res) => {
        deferred.resolve(res.data);
      })
      .catch((res) => {
        this.handleError(res, deferred);
      });
    return deferred.promise;
  }

  put(url, data, options) {
    let deferred = q.defer();

    this.instance
      .put(url, data, options)
      .then((res) => deferred.resolve(res.data))
      .catch((res) => this.handleError(res, deferred));

    return deferred.promise;
  }

  delete(url, options) {
    let deferred = q.defer();

    this.instance
      .delete(url, options)
      .then((res) => deferred.resolve(res.data))
      .catch((res) => this.handleError(res, deferred));

    return deferred.promise;
  }

  updateToken(token, userId) {
    this.instance.defaults.headers.common["ACCESS-TOKEN"] = token;
    if (userId) {
      this.instance.defaults.headers.common["USER-TOKEN"] = userId;
    } else {
      delete this.instance.defaults.headers.common.USER_TOKEN;
    }
  }

  updateLoginHeaders(password, email) {
    this.instance.defaults.headers.common["Authorization"] = btoa(
      [
        window.config.tokens.APP_KEY,
        window.config.tokens.APP_SECRET,
        password,
        email,
        "Employee",
      ].join(":"),
    );
    this.instance.defaults.headers.common["access_token"] = btoa(
      [window.config.tokens.APP_KEY, window.config.tokens.APP_SECRET].join(":"),
    );
    this.instance.defaults.headers.common["cache-control"] = "no-cache";
  }

  handleError(res, promise) {
    if (!res.response) {
      if (res.code === "ERR_NETWORK") {
        alert(
          "Please contact administrator/support, backend application is not reachable.",
        );
        console.log("Network Error - ", res.message);
      } else if (res.code === "ECONNABORTED") {
        alert("Request timed out. Please try again.");
        console.log("Timeout Error - ", res.message);
      } else {
        console.log("Unknown Error - ", res.message);
      }
      return;
    }

    const status = res.status || res.response.status;
    const message = res.response.data?.message;

    switch (status) {
      case 401:
        Storage.set("loggedIn", false);
        Storage.set("userId", null);
        Storage.set("accessToken", null);
        alert(message || "Unauthorized access. Please log in again.");
        window.location.href = "/login";
        break;

      case 403:
        Storage.set("loggedIn", false);
        Storage.set("userId", null);
        Storage.set("accessToken", null);
        alert(
          message ||
            "You don't have permission to access this page. Contact support if you think this is an error.",
        );
        break;

      case 400:
        alert(
          message ||
            "The server couldn't process your request. Please check your input and try again.",
        );
        break;

      case 404:
        alert(
          message ||
            "The requested page could not be found. Please check the URL or try again later.",
        );
        break;

      case 408:
        alert(
          message ||
            "The server took too long to respond. Please try again in a moment.",
        );
        break;

      case 429:
        alert(
          message ||
            "You’ve made too many requests in a short time. Please wait a moment and try again.",
        );
        break;

      case 500:
        alert(
          message ||
            "Oops! Something went wrong on our end. Please try again later.",
        );
        break;

      case 502:
        alert(
          message ||
            "The server is temporarily unavailable. Please try again later.",
        );
        break;

      case 503:
        alert(
          message ||
            "The service is currently unavailable. Please try again later.",
        );
        break;

      case 504:
        alert(
          message ||
            "The server took too long to respond. Please check your internet connection and try again.",
        );
        break;

      default:
        console.log(
          "Unhandled Error - Status:",
          status,
          "Message:",
          message || res.message,
        );
        alert(
          message || "An unexpected error occurred. Please try again later.",
        );
    }
  }
}
