import Cookies from "js-cookie";

export default class Storage {
  static get(key) {
    return Cookies.get(key);
  }
  static set(key, value) {
    Cookies.set(key, value, 0);
  }
  static delete(key) {
    Cookies.remove(key);
  }
}
