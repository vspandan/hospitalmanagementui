import "primeicons/primeicons.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.css";
import "primeflex/primeflex.css";
import "./index.css";
import Storage from "../storage/storage";
import Identity from "../services/Identity";
import logo from "../assets/logo.png";
import React, { Component } from "react";
import { Menubar } from "primereact/menubar";
import { InputText } from "primereact/inputtext";
import PatientRegistration from "./patentReg";

export class MenubarDemo extends Component {
  state = {
    patientRegDialogOpen: false,
  };

  handleClose = () => {
    this.setState({ patientRegDialogOpen: false });
  };

  constructor(props) {
    super(props);

    this.items = [
      {
        label: "Home",
        icon: "pi pi-fw pi-home",
        command: (event) => {
          window.location.href = "/dashboard";
        },
      },
      {
        label: "Billing",
        icon: "pi pi-fw pi-money-bill",

        command: (event) => {
          window.location.href = "/billing";
        },
      },
      {
        label: "Out Patient",
        icon: "pi pi-fw pi-users",
        items: [
          {
            label: "New Appointment",
            icon: "pi pi-fw pi-calendar-plus",
            command: (event) => {
              window.location.href = "/appointments";
            },
          },
          {
            label: "Appointments",
            icon: "pi pi-fw pi-chart-bar",
            command: (event) => {
              window.location.href = "/appointments/dashboard";
            },
          },
        ],
      },
      {
        label: "Laboratory",
        icon: "pi pi-fw pi-file",
        items: [
          {
            label: "Reports",
            icon: "pi pi-fw pi-file",
            command: (event) => {
              window.location.href = "/tests";
            },
          },
        ],
      },
      {
        label: "Patient Registration",
        icon: "pi pi-fw pi-user-plus",
        command: (event) => {
          this.setState({ patientRegDialogOpen: true });
        },
      },

      {
        label: "Doctors",
        icon: "pi pi-fw pi-prime",

        command: (event) => {
          window.location.href = "/doctors";
        },
        //{
        //  label: "Availability",
        //  icon: "pi pi-fw pi-calendar-check",
        //  command: (event) => {
        //    window.location.href = "/doctors";
        //  },
        //},
      },
      {
        label: "User Profile",
        icon: "pi pi-fw pi-user",

        command: (event) => {
          window.location.href = "/profile";
        },
      },
      {
        label: "User Search",
        icon: "pi pi-fw pi-info-circle",
        command: (event) => {
          window.location.href = "/patient";
        },
      },
      {
        label: "Logout",
        icon: "pi pi-fw pi-sign-out",
        command: (event) => {
          let identityService = new Identity();
          identityService
            .signOut(Storage.get("userId"), Storage.get("accessToken"))
            .then(() => {
              Storage.set("loggedIn", false);
              Storage.set("userId", null);
              Storage.set("accessToken", null);
              Storage.set("userName", null);
              Storage.set("logo", null);
              Storage.set("appName", null);
              window.location.href = "/login";
            });
        },
      },
      Storage.get("isAdmin") === "true"
        ? {
            label: "Admin",
            icon: "pi pi-fw pi-cog",
            items: [
              {
                label: "Configurations",
                icon: "pi pi-fw pi-building",
                command: (event) => {
                  window.location.href = "/config";
                },
              },
              {
                label: "Laboratory",
                icon: "pi pi-fw pi-file",

                command: (event) => {
                  window.location.href = "/lab/templates";
                },
              },
              {
                label: "Day Care",
                icon: "pi pi-fw pi-file",
                command: (event) => {
                  window.location.href = "/daycare/templates";
                },
              },
              {
                label: "Users",
                icon: "pi pi-fw pi-user",
                items: [
                  {
                    label: "New User",
                    icon: "pi pi-fw pi-user-plus",
                    command: (event) => {
                      window.location.href = "/admin/userregistration";
                    },
                  },
                  {
                    label: "Reset Password",
                    icon: "pi pi-fw pi-lock",
                    command: (event) => {
                      window.location.href = "/admin/passwordreset";
                    },
                  },
                ],
              },
              {
                label: "Modules",
                icon: "pi pi-fw pi-th-large",
                command: (event) => {},
              },
            ],
          }
        : {},
    ];
  }

  render() {
    const { patientRegDialogOpen } = this.state;
    const start = (
      <img
        alt="logo"
        src={logo}
        onError={(e) =>
          (e.target.src =
            "https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png")
        }
        height="40"
        className="mr-2"
      ></img>
    );
    const end = <InputText placeholder="Search" type="text" />;
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Menubar
          model={this.items}
          start={start}
          end={end}
          style={{
            fontSize: "14px",
            justifyContent: "space-between",
            width: "100%", // Ensure the menubar takes full width
            boxSizing: "border-box", // Prevents overflow due to padding
          }}
        />
        <PatientRegistration
          isOpen={patientRegDialogOpen}
          onClose={this.handleClose}
        />
      </div>
    );
  }
}
