import React, { useEffect } from "react";
import Typography from "@mui/material/Typography";
import Identity from "../services/Identity";
import {
  Button,
  TextField,
  NativeSelect,
  Grid,
  Divider,
  InputLabel,
  FormControl,
  FormHelperText,
} from "@mui/material";
import { State, City, Country } from "country-state-city";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useTheme } from "@mui/material/styles";

function PatientRegistration({ isOpen, onClose }) {
  const [patientRegDialogOpen, setPatientRegDialogOpen] =
    React.useState(isOpen);
  const [data, setData] = React.useState({});
  const [errors, setErrors] = React.useState({});
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("lg"));
  const [states, setStates] = React.useState([]);
  const [primaryAddress, setPrimaryAddress] = React.useState({});
  const [selectedCountry, setSelectedCountry] = React.useState(
    primaryAddress?.country || "India",
  );

  const [selectedState, setSelectedState] = React.useState(
    primaryAddress?.state || "Telangana",
  );
  useEffect(() => {
    if (selectedCountry) {
      const countryIsoCode = Country.getAllCountries().find(
        (c) => c.name === selectedCountry,
      )?.isoCode;

      if (countryIsoCode) {
        setStates(State.getStatesOfCountry(countryIsoCode));
      } else {
        setStates([]);
      }
    }
  }, [selectedCountry]);
  React.useEffect(() => {
    if (isOpen) {
      setPatientRegDialogOpen(true);
    }
  }, [isOpen]);

  const handleClose = () => {
    setPatientRegDialogOpen(false);
    onClose();
  };

  const handlePatientRegistration = () => {
    let identityService = new Identity();
    let updatedErrors = { ...errors }; // Copy current error state
    let isValid = true; // To track overall form validity

    // First Name validation
    if (!data.firstName || !data.firstName.match(/^[A-Za-z]+$/)) {
      isValid = false;
      updatedErrors["firstName"] = "First name must contain only letters.";
    } else {
      updatedErrors["firstName"] = "";
    }

    // Last Name validation
    if (!data.lastName || !data.lastName.match(/^[A-Za-z]+$/)) {
      isValid = false;
      updatedErrors["lastName"] = "Last name must contain only letters.";
    } else {
      updatedErrors["lastName"] = "";
    }

    if (!data.gender) {
      isValid = false;
      updatedErrors["gender"] = "Gender Selection is Mandatory";
    } else {
      isValid = true;
      updatedErrors["gender"] = "";
    }

    // Date of Birth validation
    if (!data.dob || isNaN(new Date(data.dob).getTime())) {
      isValid = false;
      updatedErrors["dob"] = "Valid date of birth is required.";
    } else {
      updatedErrors["dob"] = "";
    }

    // Aadhaar validation
    if (data.aadhaar && !data.aadhaar.match(/^\d{12}$/)) {
      isValid = false;
      updatedErrors["aadhaar"] = "Aadhaar must be a 12-digit number.";
    } else {
      updatedErrors["aadhaar"] = "";
    }

    // PAN Card validation
    const panRegex = /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
    if (data.panCard && !data.panCard.match(panRegex)) {
      isValid = false;
      updatedErrors["panCard"] =
        "Invalid PAN format. Please enter a valid PAN number.";
    } else {
      updatedErrors["panCard"] = "";
    }

    if (data.abhaId && !data.abhaId.match(/^\d{14}$/)) {
      isValid = false;
      updatedErrors["abhaId"] = "ABHA ID must be a 14-digit number.";
    } else {
      updatedErrors["abhaId"] = "";
    }

    // Nationality and Citizenship validation (defaulting if not set)
    if (!data.citizenShip) {
      data.citizenShip = "India";
    }
    if (!data.nationality) {
      data.nationality = "India";
    }

    // Blood group validation
    if (!data.bloodGroup || !data.rhFactor) {
      isValid = false;
      updatedErrors["bloodGroup"] = "Blood group and Rh factor are required.";
    } else {
      updatedErrors["bloodGroup"] = "";
    }

    // Address validations
    if (!primaryAddress) {
      isValid = false;
      updatedErrors["address"] = "Primary address is required.";
    } else {
      // PIN code validation
      if (!primaryAddress.pinCode || !primaryAddress.pinCode.match(/^\d{6}$/)) {
        isValid = false;
        updatedErrors["pinCode"] = "PIN code must be a 6-digit number.";
      } else {
        updatedErrors["pinCode"] = "";
      }

      // City validation
      if (!primaryAddress.city || primaryAddress.city.trim().length === 0) {
        isValid = false;
        updatedErrors["city"] = "City is required.";
      } else {
        updatedErrors["city"] = "";
      }

      primaryAddress.state = selectedState;

      // State validation
      if (!primaryAddress.state || primaryAddress.state.trim().length === 0) {
        isValid = false;
        updatedErrors["state"] = "State is required.";
      } else {
        updatedErrors["state"] = "";
      }

      primaryAddress.country = selectedCountry;
      // Country validation
      if (
        !primaryAddress.country ||
        primaryAddress.country.trim().length === 0
      ) {
        isValid = false;
        updatedErrors["country"] = "Country is required.";
      } else {
        updatedErrors["country"] = "";
      }

      // Contact number validation
      if (
        !primaryAddress.phoneNo ||
        !primaryAddress.phoneNo.match(/^[0-9]{10}$/)
      ) {
        isValid = false;
        updatedErrors["phoneNo"] = "Contact number must be a 10-digit number.";
      } else {
        updatedErrors["phoneNo"] = "";
      }

      // Email validation
      if (
        primaryAddress.emailAddress &&
        (!primaryAddress.emailAddress.match(
          /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/,
        ) ||
          primaryAddress.emailAddress.length > 100)
      ) {
        isValid = false;
        updatedErrors["emailAddress"] =
          "Invalid email format or email address exceeds 100 characters.";
      } else {
        updatedErrors["emailAddress"] = "";
      }
    }

    // Update errors state and check if the form is valid
    setErrors(updatedErrors);

    // If any validation failed, don't proceed with registration
    if (!isValid) {
      alert("Please fill all the mandatory information in the form.");
      return;
    }
    data.type = "PATIENT";
    data.address = [primaryAddress];
    // If the form is valid, proceed with registration
    identityService.register(data).then((res) => {
      if (res.data === true) {
        alert("Successfully Registered Patient");
        setPatientRegDialogOpen(false);
        setData({});
        onClose();
      } else {
        alert("Failed Patient Registration");
      }
    });
  };

  const handlePatientDetailsChange = (type, attr) => (event) => {
    const value = event.target.value;
    let isValid = true;
    let errorMessage = "";
    let updatedErrors = { ...errors }; // Copy current error state

    switch (type) {
      case "prof":
        switch (attr) {
          case "fname":
            if (!value.match(/^[A-Za-z]+$/)) {
              isValid = false;
              errorMessage = "First name must contain only letters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.firstName = value;
            break;
          case "lname":
            if (!value.match(/^[A-Za-z]+$/)) {
              isValid = false;
              errorMessage = "Last name must contain only letters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.lastName = value;
            break;
          case "gender":
            if (!value) {
              isValid = false;
              errorMessage = "Gender is required.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.gender = value;
            break;
          case "father":
            data.father = {};
            if (value && !value.match(/^[A-Za-z ]+$/)) {
              isValid = false;
              errorMessage = "Father's name must contain only letters.";
            } else {
              isValid = true;
              errorMessage = "";
              if (!data.father) data.father = {};
            }
            data.father.firstName = value;
            break;
          case "mother":
            data.mother = {};
            if (value && !value.match(/^[A-Za-z ]+$/)) {
              isValid = false;
              errorMessage = "Mother's name must contain only letters.";
            } else {
              isValid = true;
              errorMessage = "";
              if (!data.mother) data.mother = {};
            }
            data.mother.firstName = value;
            break;
          case "aadhaar":
            if (value && !value.match(/^\d{12}$/)) {
              isValid = false;
              errorMessage = "Aadhaar must be a 12-digit number.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.aadhaar = value;
            break;
          case "panCard":
            var val = value.toUpperCase();
            const panRegex = /^[A-Z]{5}[0-9]{4}[A-Z]{1}$/;
            if (val && !val.match(panRegex)) {
              isValid = false;
              errorMessage =
                "Invalid PAN format. Please enter a valid PAN number.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.panCard = val;
            break;
          case "abhaId":
            if (value && !value.match(/^\d{14}$/)) {
              isValid = false;
              errorMessage = "ABHA must be a 14-digit number.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.abhaId = value;
            break;
          case "spouse":
            data.spouse = {};
            if (value && !value.match(/^[A-Za-z ]+$/)) {
              isValid = false;
              errorMessage = "Spouse's name must contain only letters.";
            } else {
              isValid = true;
              errorMessage = "";
              if (!data.spouse) data.spouse = {};
            }
            data.spouse.firstName = value;
            break;
          case "nationality":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "Nationality is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "Nationality must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.nationality = value;
            break;
          case "maritialStatus":
            if (!value) {
              isValid = false;
              errorMessage = "Marital status is required.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.maritialStatus = value;
            break;
          case "birth":
            if (value && !value.trim()) {
              isValid = false;
              errorMessage = "Place of birth is required.";
            } else if (value.length > 100) {
              isValid = false;
              errorMessage = "Place of birth must not exceed 100 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.birthPlace = value;
            break;
          case "dob":
            let dateofBirth = new Date(value);
            if (!value || isNaN(dateofBirth.getTime())) {
              isValid = false;
              errorMessage = "Valid date of birth is required.";
            } else if (dateofBirth > new Date()) {
              isValid = false;
              errorMessage = "Date of birth cannot be in the future.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.dob = dateofBirth.getTime();
            break;
          case "blood":
            let bloodGroup = value.split(":");
            isValid = true;
            errorMessage = "";
            data.bloodGroup = bloodGroup[0];
            data.rhFactor = bloodGroup[1];
            break;
          default:
            break;
        }
        break;
      case "addr":
        if (!primaryAddress) primaryAddress = {};
        switch (attr) {
          case "pincode":
            if (!value.match(/^\d{6}$/)) {
              isValid = false;
              errorMessage = "PIN code must be a 6-digit number.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            primaryAddress.pinCode = value;
            break;
          case "city":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "City is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "City must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            primaryAddress.city = value;
            break;
          case "country":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "Country is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "Country must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            primaryAddress.country = value;
            break;
          case "district":
            if (value && !value.trim()) {
              isValid = false;
              errorMessage = "District is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "District must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            primaryAddress.district = value;
            break;
          case "state":
            if (!value.trim()) {
              isValid = false;
              errorMessage = "State is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "State must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            primaryAddress.state = value;
            break;
          case "door":
            if (value && !value.trim()) {
              isValid = false;
              errorMessage = "Door number is required.";
            } else if (value.length > 20) {
              isValid = false;
              errorMessage = "Door number must not exceed 20 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            primaryAddress.doorNo = value;
            break;
          case "street":
            if (value && !value.trim()) {
              isValid = false;
              errorMessage = "Street name is required.";
            } else if (value.length > 100) {
              isValid = false;
              errorMessage = "Street name must not exceed 100 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            primaryAddress.streetName = value;
            break;
          case "locality":
            if (value && !value.trim()) {
              isValid = false;
              errorMessage = "Locality is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage = "Locality must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            primaryAddress.locality = value;
            break;
          case "landmark":
            if (value && !value.trim()) {
              isValid = false;
              errorMessage = "Landmark is required.";
            } else if (value.length > 100) {
              isValid = false;
              errorMessage = "Landmark must not exceed 100 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            primaryAddress.viaName = value;
            break;
          case "cname":
            if (value && !value.trim()) {
              isValid = false;
              errorMessage = "Emergency Contact name is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage =
                "Emergency Contact name must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.eContactName = value;
            break;
          case "cnum":
            if (!value.match(/^[0-9]{10}$/)) {
              isValid = false;
              errorMessage = "Contact number must be a 10-digit number.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.eContactNumber = value;

            break;
          case "crelation":
            if (value && !value.trim()) {
              isValid = false;
              errorMessage = "Emergency Contact Relation is required.";
            } else if (value.length > 50) {
              isValid = false;
              errorMessage =
                "Emergency Contact Relation must not exceed 50 characters.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            data.eRelation = value;
            break;
          case "contact":
            if (!value.match(/^[0-9]{10}$/)) {
              isValid = false;
              errorMessage = "Contact number must be a 10-digit number.";
            } else {
              isValid = true;
              errorMessage = "";
            }
            primaryAddress.phoneNo = value;
            data.loginPhoneNo = value;
            break;
          case "email":
            if (value && !value.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/)) {
              isValid = false;
              errorMessage = "Invalid email address.";
            } else if (value.length > 100) {
              isValid = false;
              errorMessage = "Email address must not exceed 100 characters.";
            }
            primaryAddress.emailAddress = value;
            break;
          default:
            break;
        }
        break;
      default:
        break;
    }

    // Update errors state
    if (!isValid) {
      updatedErrors[attr] = errorMessage; // Set the error message for the invalid field
    } else {
      updatedErrors[attr] = ""; // Clear the error message if valid
    }

    // Update state with new data and errors
    setErrors(updatedErrors);
    setData({ ...data }); // Ensure the data state updates
  };

  if (!isOpen) {
    return null;
  }
  const countries = Country.getAllCountries();
  return (
    <Dialog
      disableBackdropClick
      fullWidth
      maxWidth="lg"
      style={{ marginTop: "120px" }}
      open={patientRegDialogOpen}
      fullScreen={fullScreen}
      onClose={handleClose}
      aria-labelledby="responsive-dialog-title"
    >
      <DialogTitle id="responsive-dialog-title">
        <Typography
          variant="h5"
          style={{
            fontWeight: "bold",
            padding: "8px 16px",
            borderBottom: "2px solid #f0f0f0",
            color: "#3f51b5",
            textAlign: "center",
          }}
        >
          New Patient Registration
        </Typography>
      </DialogTitle>

      <DialogContent>
        <DialogContentText>
          <Typography variant="h5" gutterBottom align="center">
            Personal Information
          </Typography>
          <br />
          <Grid container spacing={5}>
            <Grid item xs={12} sm={3}>
              <TextField
                label="First Name"
                required
                fullWidth
                variant="outlined"
                value={data ? data.firstName : ""}
                onChange={handlePatientDetailsChange("prof", "fname")}
                error={!!errors.fname}
                helperText={errors.fname}
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                label="Surname"
                required
                fullWidth
                variant="outlined"
                value={data ? data.lastName : ""}
                onChange={handlePatientDetailsChange("prof", "lname")}
                error={!!errors.lname}
                helperText={errors.lname}
              />
            </Grid>
            {/* Date of Birth */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="Date of Birth"
                required
                fullWidth
                variant="outlined"
                type="date"
                inputProps={{
                  max: new Date().toISOString().split("T")[0], // Current date and time
                }}
                InputLabelProps={{ shrink: true }}
                onChange={handlePatientDetailsChange("prof", "dob")}
                error={!!errors.dob}
                helperText={errors.dob}
                onKeyDown={(e) => e.preventDefault()}
              />
            </Grid>
            {/* Place of Birth */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="Place of Birth"
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("prof", "birth")}
                error={!!errors.birth}
                helperText={errors.birth}
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <FormControl
                sx={{ paddingLeft: "10px" }}
                fullWidth
                variant="outlined"
              >
                <InputLabel shrink required>
                  Gender
                </InputLabel>
                <NativeSelect
                  required
                  variant="outlined"
                  value={data.gender || ""}
                  onChange={handlePatientDetailsChange("prof", "gender")}
                  inputProps={{ "aria-label": "gender" }}
                >
                  <option value="" disabled>
                    Select Gender
                  </option>
                  <option value="MALE">Male</option>
                  <option value="FEMALE">Female</option>
                  <option value="TRANS">Others</option>
                </NativeSelect>
              </FormControl>
            </Grid>
            {/* Blood Group */}
            <Grid item xs={12} sm={3}>
              <FormControl
                sx={{ paddingLeft: "10px" }}
                fullWidth
                variant="outlined"
                error={!!errors.blood}
              >
                <InputLabel shrink required>
                  Blood Group
                </InputLabel>
                <NativeSelect
                  onChange={handlePatientDetailsChange("prof", "blood")}
                >
                  <option value="">Select Blood Group</option>
                  <option value={"O:POSITIVE"}>O+</option>
                  <option value={"O:NEGATIVE"}>O-</option>
                  <option value={"A:POSITIVE"}>A+</option>
                  <option value={"A:NEGATIVE"}>A-</option>
                  <option value={"B:POSITIVE"}>B+</option>
                  <option value={"B:NEGATIVE"}>B-</option>
                  <option value={"AB:POSITIVE"}>AB+</option>
                  <option value={"AB:NEGATIVE"}>AB-</option>
                </NativeSelect>
                {errors.blood && (
                  <FormHelperText>{errors.blood}</FormHelperText>
                )}
              </FormControl>
            </Grid>
            {/* Marital Status */}
            <Grid item xs={12} sm={3}>
              <FormControl
                sx={{ paddingLeft: "10px" }}
                fullWidth
                variant="outlined"
                error={!!errors.maritialStatus}
              >
                <InputLabel shrink>Marital Status *</InputLabel>
                <NativeSelect
                  value={data.maritialStatus || ""}
                  onChange={handlePatientDetailsChange(
                    "prof",
                    "maritialStatus",
                  )}
                >
                  <option value="">Select Marital Status</option>
                  <option value={"SINGLE"}>Single</option>
                  <option value={"MARRIED"}>Married</option>
                  <option value={"DIVORCED"}>Divorced</option>
                  <option value={"WIDOWED"}>Widowed</option>
                </NativeSelect>
                {errors.maritialStatus && (
                  <FormHelperText>{errors.maritialStatus}</FormHelperText>
                )}
              </FormControl>
            </Grid>
            {/* Nationality */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="Nationality"
                required
                fullWidth
                defaultValue={"INDIAN"}
                variant="outlined"
                onChange={handlePatientDetailsChange("prof", "nationality")}
                error={!!errors.nationality}
                helperText={errors.nationality}
              />
            </Grid>
            {/* Aadhaar Number */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="Aadhaar Number"
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("prof", "aadhaar")}
                error={!!errors.aadhaar}
                helperText={errors.aadhaar}
              />
            </Grid>
            <Grid item xs={12} sm={3}>
              <TextField
                label="PAN Number"
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("prof", "panCard")}
                error={!!errors.panCard}
                helperText={errors.panCard}
              />
            </Grid>

            {/* Referred By */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="ABHA ID"
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("prof", "abhaId")}
                error={!!errors.abhaId}
                helperText={errors.abhaId}
              />
            </Grid>
          </Grid>
          <br />
          <Divider style={{ margin: "20px 0" }} />
          <Typography variant="h5" gutterBottom align="center">
            Communication
          </Typography>
          <br />
          <Grid container spacing={5}>
            {/* Phone */}
            <Grid item xs={12} sm={6}>
              <TextField
                label="Phone"
                required
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("addr", "contact")}
                error={!!errors.contact}
                helperText={errors.contact}
              />
            </Grid>

            {/* Email */}
            <Grid item xs={12} sm={6}>
              <TextField
                label="Email"
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("addr", "email")}
                error={!!errors.email}
                helperText={errors.email}
              />
            </Grid>
          </Grid>
          <Divider style={{ margin: "20px 0" }} />
          <Typography variant="h5" gutterBottom align="center">
            Address
          </Typography>
          <br />
          <Grid container spacing={5}>
            {/* Door No */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="Door No"
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("addr", "door")}
                error={!!errors.door}
                helperText={errors.door}
              />
            </Grid>

            {/* Street */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="Street"
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("addr", "street")}
                error={!!errors.street}
                helperText={errors.street}
              />
            </Grid>

            {/* Locality */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="Locality"
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("addr", "locality")}
                error={!!errors.locality}
                helperText={errors.locality}
              />
            </Grid>

            {/* Landmark */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="Landmark"
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("addr", "landmark")}
                error={!!errors.landmark}
                helperText={errors.landmark}
              />
            </Grid>

            {/* Country */}
            <Grid item xs={12} sm={3}>
              <FormControl
                sx={{ paddingLeft: "10px" }}
                fullWidth
                variant="outlined"
                error={!!errors.country}
              >
                <InputLabel shrink>Country</InputLabel>
                <NativeSelect
                  value={selectedCountry}
                  onChange={(e) => {
                    const selectedValue = e.target.value;
                    setSelectedCountry(selectedValue);
                    handlePatientDetailsChange("addr", "country")(e);
                    // Clear state when country changes
                    handlePatientDetailsChange(
                      "addr",
                      "state",
                    )({ target: { value: "" } });
                  }}
                >
                  <option value="">NOT SPECIFIED</option>
                  {Country.getAllCountries().map((country) => (
                    <option key={country.isoCode} value={country.name}>
                      {country.name}
                    </option>
                  ))}
                </NativeSelect>
                {errors.country && (
                  <FormHelperText>{errors.country}</FormHelperText>
                )}
              </FormControl>
            </Grid>

            {/* State */}
            <Grid item xs={12} sm={3}>
              <FormControl
                sx={{ paddingLeft: "10px" }}
                fullWidth
                variant="outlined"
                error={!!errors.state}
              >
                <InputLabel shrink>State</InputLabel>
                <NativeSelect
                  value={selectedState}
                  onChange={handlePatientDetailsChange("addr", "state")}
                  disabled={states.length === 0}
                >
                  <option value="">NOT SPECIFIED</option>
                  {states.map((state) => (
                    <option key={state.isoCode} value={state.name}>
                      {state.name}
                    </option>
                  ))}
                </NativeSelect>
                {errors.state && (
                  <FormHelperText>{errors.state}</FormHelperText>
                )}
              </FormControl>
            </Grid>
            {/* City */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="City/Town"
                required
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("addr", "city")}
                error={!!errors.city}
                helperText={errors.city}
              />
            </Grid>
            {/* Pin Code */}
            <Grid item xs={12} sm={3}>
              <TextField
                label="Pin Code"
                required
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("addr", "pincode")}
                error={!!errors.pincode}
                helperText={errors.pincode}
              />
            </Grid>
          </Grid>
          <Divider style={{ margin: "20px 0" }} />
          <Typography variant="h5" gutterBottom align="center">
            Emergency Contact Information
          </Typography>
          <br />
          <Grid container spacing={5}>
            {/* Contact Name */}
            <Grid item xs={12} sm={4}>
              <TextField
                label="Emergency Contact Name"
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("addr", "cname")}
                error={!!errors.cname}
                helperText={errors.cname}
              />
            </Grid>

            {/* Contact Name */}
            <Grid item xs={12} sm={4}>
              <TextField
                label="Emergency Contact Number"
                fullWidth
                variant="outlined"
                onChange={handlePatientDetailsChange("addr", "cnum")}
                error={!!errors.cnum}
                helperText={errors.cnum}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <FormControl
                sx={{ paddingLeft: "10px" }}
                fullWidth
                variant="outlined"
                error={!!errors.maritialStatus}
              >
                <InputLabel shrink>RelationShip</InputLabel>
                <NativeSelect
                  value={data.eRelation || ""}
                  onChange={handlePatientDetailsChange("addr", "crelation")}
                >
                  <option value="">Select RelationShip</option>
                  <option value={"Parent"}>Parent</option>
                  <option value={"Spouse"}>Spouse</option>
                  <option value={"Sibling"}>Sibling</option>
                  <option value={"Friend"}>Friend</option>
                  <option value={"Other"}>Other</option>
                </NativeSelect>
                {errors.maritialStatus && (
                  <FormHelperText>{errors.crelation}</FormHelperText>
                )}
              </FormControl>
            </Grid>
          </Grid>
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={handlePatientRegistration}
          variant="outlined"
          sx={{ backgroundColor: "red", color: "white" }}
        >
          Save
        </Button>
        <Button
          onClick={handleClose}
          variant="outlined"
          sx={{ backgroundColor: "black", color: "white" }}
        >
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default PatientRegistration;
