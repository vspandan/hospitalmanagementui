import React from "react";
import clsx from "clsx";
import { makeStyles } from "@mui/styles";
import CssBaseline from "@mui/material/CssBaseline";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { withRouter } from "react-router-dom";
import Storage from "../storage/storage";
import { MenubarDemo } from "./menubar";
import { Box, Grid } from "@mui/material";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex", // Changed to 'flex' for proper layout
    flexDirection: "column",
  },
  toolbar: {
    paddingRight: 24,
    backgroundColor: theme.palette.primary.main, // Use primary color for toolbar
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    boxShadow: theme.shadows[5], // Lighter shadow for elegance
    backgroundColor: theme.palette.primary.main, // Consistent primary theme color
    borderRadius: "0 0 10px 10px", // Rounded corners for the bottom of the AppBar
  },
  title: {
    flexGrow: 1,
    fontWeight: 600, // Bold font for emphasis
    color: "#FFFFFF", // White text for contrast
  },
  menuBarContainer: {
    display: "flex",
    padding: theme.spacing(2),
    backgroundColor: "#F7F9FC", // Light background for the menu bar
    borderRadius: "10px", // Rounded corners for the menu bar
    boxShadow: theme.shadows[2], // Subtle shadow for elevation
  },
}));

function Template() {
  const classes = useStyles();
  const isLoggedIn =
    Storage.get("loggedIn") === "true" &&
    Storage.get("accessToken") !== "null" &&
    Date.now() < Storage.get("expires") + Storage.get("validFrom");

  if (isLoggedIn) {
    return (
      <div className={classes.root}>
        <CssBaseline />
        <Grid container>
          <Grid item xs={12}>
            <AppBar position="sticky" className={clsx(classes.appBar)}>
              <Toolbar className={classes.toolbar}>
                {Storage.get("logo") != "null" &&
                Storage.get("logo") !== null &&
                Storage.get("logo") !== undefined ? (
                  <img
                    src={Storage.get("logo")}
                    alt="Logo"
                    height="40"
                    className="mr-2"
                  />
                ) : (
                  <div></div>
                )}
                <Box ml={2}>
                  {" "}
                  {/* Adds spacing between the logo and the title */}
                  <Typography
                    component="h4"
                    variant="h3"
                    noWrap
                    className={classes.title}
                  >
                    {Storage.get("appName") != "null" &&
                    Storage.get("appName") !== null &&
                    Storage.get("appName") !== undefined
                      ? Storage.get("appName")
                      : "Hospital Management"}
                  </Typography>
                </Box>
              </Toolbar>
            </AppBar>
          </Grid>
          <Grid item xs={12} className={classes.menuBarContainer}>
            <MenubarDemo />
          </Grid>
        </Grid>
      </div>
    );
  } else {
    window.location.href = "/login";
    return null; // Return null to avoid rendering during redirect
  }
}

export default withRouter(Template);
