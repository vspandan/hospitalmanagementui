import config from "./config.json";

let hostname = window.location.hostname;

if (hostname === "localhost") {
  window.config = config.local;
} else {
  window.config = config.production;
}
