// theme.js
export const theme = {
  primaryDark: "#0D0C1D",
  primaryLight: "#FFFFFF",
  primaryHover: "#343078",
  mobile: "576px",
};
